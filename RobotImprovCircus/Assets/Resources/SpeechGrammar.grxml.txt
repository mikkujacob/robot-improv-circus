﻿<grammar version="1.0" xml:lang="en-US" root="rootRule" tag-format="semantics/1.0-literals" xmlns="http://www.w3.org/2001/06/grammar">
  <rule id="rootRule">
    <one-of>
      <item>
        <tag>START</tag>
        <one-of>
          <item> start </item>
          <item> begin </item>
          <item> commence </item>
          <item> go ahead </item>
        </one-of>
      </item>
      <item>
        <tag>RECORD</tag>
        <one-of>
          <item> record </item>
          <item> recording interface </item>
          <item> load recording interface </item>
          <item> recording </item>
        </one-of>
      </item>
      <item>
        <tag>STOP</tag>
        <one-of>
          <item> stop </item>
          <item> end </item>
          <item> finish </item>
          <item> end recording </item>
        </one-of>
      </item>
    </one-of>
  </rule>
</grammar>
