using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//using UnityEditor;
using System.Linq;
using System;
using System.IO;
using NewtonVR;

//using System.DateTime;
//using System.TimeSpan;
public class GestureSaver : MonoBehaviour/*, SpeechRecognitionInterface*/
{


	public SceneSaver sceneSaver;
	private string privateFilePath;
	public string filePath;
	public string recordingName;
	public bool useTimer = false;
	public int frameCap;



	private DateTime dateTimeOfRecordingStart;
	private Dictionary<GameObject, TrackedChange> objectToLastChange;
	private bool isRecordingRunning = false;
	private List<GameObject> trackedObjectsList;
	private int frameCounter = 0;
	private Dictionary<string, string> idToChangeLogPath;
	private string kinectGestureFilePath;


	public NVRPlayer nvrPlayer;
	public NVRHand[] nvrHands;
	public GameObject playerObject;

	private GameObject keyboard;
	private GameObject console;

	private int waitForConsoleToUpdate = 0;
	private int[] handInitializationBuffer;

	private GlobalTrackerManager trackerManager;



	// Use this for initialization
	void Start ()
	{
		//initilize the list of all objects that shouls be trakced
		objectToLastChange = new Dictionary<GameObject, TrackedChange> ();
		idToChangeLogPath = new Dictionary<string, string> ();
		if (filePath == null || filePath == "")
		{
			filePath = sceneSaver.resourcesFolderLocation;
		}
		privateFilePath = filePath;
		playerObject = GameObject.FindGameObjectWithTag ("NVR Player");
		nvrPlayer = playerObject.GetComponent<NVRPlayer> ();
		nvrHands = playerObject.GetComponentsInChildren<NVRHand> ();

		handInitializationBuffer = new int[2];
		trackerManager = GameObject.FindGameObjectWithTag ("MImEGlobalObject").GetComponent<GlobalTrackerManager> ();

	}


	// Update is called once per frame
	void Update ()
	{
		nvrHands = playerObject.GetComponentsInChildren<NVRHand> ();


		for (int i = 0; i < nvrHands.Length; i++)
		{
			if (nvrHands [i].CurrentHandState != HandState.Uninitialized)
			{
				if (handInitializationBuffer [i] < 2)
				{
					handInitializationBuffer [i]++;
				}
				else if (nvrHands [i].Inputs [NVRButtons.Touchpad].PressDown)
				{
					//Debug.Log("TOCUHPAD HAS BEEN PRESSED");
					NextStepInRecordingSequance ();
				}
			}
		}

		if (isRecordingRunning == true)
		{
			if (waitForConsoleToUpdate < 2)
			{
				waitForConsoleToUpdate++;
			}
			else if (waitForConsoleToUpdate == 2)
			{

				DelayedStartRecording ();
				waitForConsoleToUpdate = 5;
			}
			Debug.Log ("[DEBUG]");
			/*Debug.Log("[DEBUG] isRecordingRunning == true");
			foreach (GameObject someObject in trackedObjectsList)
			{
				List<TrackedChange> changes = someObject.GetComponent<ObjectStateTracker>().mostRecentChanges;
				int lastIndex = changes.Count() - 1;
				string systemObjectChangeLogPath = filePath + "\\ChangeLogs\\" + recordingName + " " + someObject.name + ".txt";



				if (objectToLastChange[someObject] == null && changes.Count() > 0)
				{
					using (System.IO.StreamWriter file = new System.IO.StreamWriter(@systemObjectChangeLogPath))
		 	 		{
						foreach (TrackedChange trackedChange in changes) {
							file.WriteLine(trackedChange.ToString());
						}
					}
				}
				else if (changes.Count() > 0 && objectToLastChange[someObject] != changes[lastIndex])
				{

					List<string> linesToAppened = new List<string>();
					for (int i = changes.IndexOf(objectToLastChange[someObject]) + 1; i < changes.Count; i++)
					{
						linesToAppened.Add(changes[i].ToString());
					}

					//File.AppendAllLines(systemObjectChangeLogPath, linesToAppened);

					using (System.IO.StreamWriter file = new System.IO.StreamWriter(@systemObjectChangeLogPath))
					{
						foreach (string line in linesToAppened)
						{
							file.WriteLine(line);
						}
					}
				}
			}*/




			if (useTimer == true)
			{
				if (frameCounter > frameCap)
				{
					StopRecording ();
				}

				frameCounter++;
			}
		}


	}



	private void NextStepInRecordingSequance ()
	{
		if (keyboard == null && isRecordingRunning == false)
		{
			LoadKeyboard ();
		}
		else if (keyboard != null)
		{
			trackerManager.StartAction ();
			StartRecording ();
		}
		else if (isRecordingRunning == true && waitForConsoleToUpdate == 5)
		{
			trackerManager.EndAction ();
			StopRecording ();
		}
	}


	public void LoadKeyboard ()
	{
		NVRHead nvrHead = playerObject.GetComponentInChildren<NVRHead> ();
		if (keyboard == null)
		{
			UnityEngine.Object keyboardPrefab = Resources.Load ("PreFabs/Keyboard V2");

			keyboard = (GameObject)GameObject.Instantiate (keyboardPrefab, nvrHead.gameObject.transform);

			//keyboard.transform.rotation.SetLookRotation(nvrHead.gameObject.transform.position);
			NVRCanvasInput nvrCanvasInput = nvrPlayer.GetComponent<NVRCanvasInput> ();
			nvrCanvasInput.canvasCameraInit = false;
			nvrCanvasInput.GeometryBlocksLaser = false;
		}

		if (console == null)
		{
			UnityEngine.Object consolePrefab = Resources.Load ("PreFabs/RecordingConsole");
			console = (GameObject)GameObject.Instantiate (consolePrefab, nvrHead.gameObject.transform);
		}
	}

	private void DestroyKeyboard ()
	{
		if (keyboard != null)
		{
			nvrPlayer.GetComponent<NVRCanvasInput> ().GeometryBlocksLaser = false;
			;
			GameObject.Destroy (keyboard);
		}
	}


	public void StartRecording ()
	{
		if (!isRecordingRunning && keyboard != null)
		{
			string textBuffer = keyboard.GetComponent<CanvasKeyboard> ().currentTextBuffer;
			if (textBuffer != "")
			{
				recordingName = textBuffer;
				filePath = privateFilePath + "\\" + recordingName;
			}
			else
			{
				console.GetComponent<TextMesh> ().text += "\nYou Must enter a name for the recording.";
				return;
			}
			DestroyKeyboard ();


			//console.GetComponent<TextMesh>().text += "\n[WARNING] Video feed may freeze due to large \namount of unregistered objects in vacinity.";
			idToChangeLogPath = new Dictionary<string, string> ();
			isRecordingRunning = true;


		}
	}

	private void DelayedStartRecording ()
	{

		nvrHands = playerObject.GetComponentsInChildren<NVRHand> ();

		sceneSaver.writeToFile (recordingName + " SceneStart", filePath);
		GameObject[] trackedObjects = GameObject.FindGameObjectsWithTag ("ObjectStateTracker");
		trackedObjectsList = new List<GameObject> (trackedObjects);
		foreach (GameObject someObject in trackedObjectsList)
		{
			string systemObjectChangeLogPath = filePath + "\\ChangeLogs\\" + recordingName + " " + someObject.name + ".txt";
			Directory.CreateDirectory (Path.GetDirectoryName (systemObjectChangeLogPath));
			System.IO.File.Create (@systemObjectChangeLogPath).Dispose ();
			idToChangeLogPath.Add (someObject.GetComponent<ObjectStateTracker> ().state.id, systemObjectChangeLogPath);
			/*List<TrackedChange> changes = someObject.GetComponent<ObjectStateTracker>().mostRecentChanges;
			if (changes.Count() == 0)
			{
			objectToLastChange.Add(someObject, null);
		}
		else
		{
		objectToLastChange.Add(someObject, changes[changes.Count() - 1]);
		}*/

		}
		kinectGestureFilePath = filePath + "\\" + recordingName + " kinectGesture.txt";
		Directory.CreateDirectory (Path.GetDirectoryName (kinectGestureFilePath));
		System.IO.File.Create (@kinectGestureFilePath).Dispose ();
		console.GetComponent<TextMesh> ().text += "\nTo end the recording, simply press down on the TouchPad.";
		console.GetComponent<TextMesh> ().text += "\nRecording now in progress.";
		dateTimeOfRecordingStart = System.DateTime.Now;

	}

	public void StopRecording ()
	{
		if (isRecordingRunning)
		{
			isRecordingRunning = false;
			sceneSaver.writeToFile (recordingName + " SceneEnd", filePath);
			GameObject.Destroy (console);
			waitForConsoleToUpdate = 0;
		}
	}


	public void WriteOSTJSON (string jSON, string id)
	{
		if (isRecordingRunning == true)
		{
			string systemChangeLogPath = idToChangeLogPath [id];
			//using (System.IO.StreamWriter fileOST = new System.IO.StreamWriter(@systemChangeLogPath))
			using (System.IO.StreamWriter fileOST = File.AppendText (systemChangeLogPath))
			{
				string CompleteLine = "Current time: " + System.DateTime.Now.TimeOfDay + ", Time from start of recording: " + System.DateTime.Now.Subtract (dateTimeOfRecordingStart) + ", JSON: " + jSON;
				fileOST.WriteLine (CompleteLine);
			}
		}
	}


	public void WriteKSTJSON (string jSON)
	{
		if (isRecordingRunning == true)
		{
			//using (System.IO.StreamWriter fileKST = new System.IO.StreamWriter(@kinectGestureFilePath))
			using (System.IO.StreamWriter fileKST = File.AppendText (kinectGestureFilePath))
			{
				string CompleteLine = "Current time: " + System.DateTime.Now.TimeOfDay + ", Time from start of recording: " + System.DateTime.Now.Subtract (dateTimeOfRecordingStart) + ", " + ViveHandsStateToString () + "JSON: " + jSON;
				fileKST.WriteLine (CompleteLine);
			}
		}
	}

	private string ViveHandsStateToString ()
	{
		string output = "";
		for (int i = 0; i < nvrHands.Count (); i++)
		{
			GameObject currentHand = nvrHands [i].gameObject;
			Vector3 handposition = currentHand.transform.position;
			float positionX = handposition.x;
			float positionY = handposition.y;
			float positionZ = handposition.z;
			string positionString = "position: " + positionX.ToString ("R") + "," + positionY.ToString ("R") + "," + positionZ.ToString ("R");

			Quaternion handRotation = currentHand.transform.rotation;
			float rotationX = handRotation.x;
			float rotationY = handRotation.y;
			float rotationZ = handRotation.z;
			float rotationW = handRotation.w;
			string rotationString = "rotation: " + rotationX.ToString () + "," + rotationY.ToString () + "," + rotationZ.ToString () + "," + rotationW.ToString ();

			string isUsePressed = "Use Pressed: " + nvrHands [i].UseButtonPressed;
			string isHoldPressed = "Hold Pressed: " + nvrHands [i].HoldButtonPressed;

			output = output + "hand " + i + ": " + isHoldPressed + ", " + isUsePressed + ", " + positionString + ", " + rotationString + ", ";
		}
		return output;
	}

}
