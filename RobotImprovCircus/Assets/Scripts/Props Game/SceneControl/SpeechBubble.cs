﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crosstales.RTVoice;
using UnityEngine.UI;
public class SpeechBubble : MonoBehaviour
{

    public string VoiceName;
    public static Text speechTextVRView;
    public Vector3 speechBubbleOffset = new Vector3(0.315008f, 4.03f, 1.443972f);
    private AgentPlayer playback;
    private GameObject agentObj;

    public void Start()
    {
        agentObj = GameObject.FindGameObjectWithTag("Agent");
        playback = GameObject.FindGameObjectWithTag("MImEGlobalObject").GetComponent<AgentPlayer>();
        speechTextVRView = GameObject.FindGameObjectWithTag("SpeechTextVRView").GetComponent<Text>();
    }

    // Update the position of the speech bubble after the Agent updates its position.
    void LateUpdate()
    {
        transform.position = agentObj.transform.position + speechBubbleOffset;
    }

    public void ChangeDisplayText()
    {
        if (speechTextVRView != null)
        {
            speechTextVRView.text = "I am " + playback.recording.GetGesture().pretendActionName + "ing with my " + playback.recording.GetGesture().pretendObjectName;
        }
        // Debug.Log("VR SPEECH TEXT " + speechTextVRView.text);
        // Debug.Log("AUDIENCE SPEECH TEXT " + speechTextAudienceView.text);
    }

    public void DeleteDisplayText()
    {
        if (speechTextVRView != null)
        {
            speechTextVRView.text = " ";
        }
    }

    public void SayHm()
    {
        if (speechTextVRView != null)
        {
            speechTextVRView.text = "Hmmm...";
        }
    }

    public void Speak()
    {
        if (speechTextVRView != null)
        {
            Speaker.Speak(speechTextVRView.text, null, Speaker.VoiceForName(VoiceName));
        }
        else
        {
            // Debug.Log("Speech Text VR View was null.");
        }
    }
}
