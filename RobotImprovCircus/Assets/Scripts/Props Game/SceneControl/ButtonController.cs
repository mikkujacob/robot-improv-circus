﻿using UnityEngine;
using System.Collections;
using NewtonVR;

public class ButtonController : MonoBehaviour
{
    public NVRButton Button;

    private int playerScore = 0;

    public bool changeTurn = false;
    
    //The sound effect for the buzzer
    private AudioSource buzzerSound;

    private LevelManager levelManager;

    private void Start()
    {
        //get the buzzer sound
        buzzerSound = GameObject.Find("BuzzerSound").GetComponent<AudioSource>();
        //set the volume for the buzzer sound
        buzzerSound.volume = 1f;

        levelManager = gameObject.GetComponent<LevelManager>();
    }


    private void Update()
    {
        if (Button.ButtonDown)
        {
            if (levelManager.currentTurn == LevelManager.TurnType.PLAYER)
            {
                buzzerSound.Play();
                playerScore = playerScore + 10;
                levelManager.changeTurnNotOrigStructure();
            }
        }
    }
        
    public int GetPlayerScore()
    {
        if (playerScore != 0)
        {
            return playerScore;
        } else
        {
            return 0;
        }
    }

    public void SetPlayerScore(int score)
    {
        playerScore = score;
    }

    public bool GetButtonDown()
    {
        if (Button.ButtonDown)
        {
            return true;
        } else
        {
            return false;
        }
    }
}