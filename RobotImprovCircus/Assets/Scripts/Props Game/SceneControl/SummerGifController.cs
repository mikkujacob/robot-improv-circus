﻿using System.Collections.Generic;
using RootMotion.FinalIK;
using UnityEngine;
using System;
using System.IO;

public class SummerGifController : MonoBehaviour
{

    private string streamingAssetsPath;

    public List<GestureDefinition> listOfGestureDefinitions;

    private bool isPlaying;

    private Executive executive;
    private PropManager propManager;
    private Animator robotAIanimator;
    private RobotAI robotAIScript;
    private AgentPlayer agentPlayer;

    public int gestureIndex;

    // Start is called before the first frame update
    void Start()
    {

        robotAIanimator = GameObject.Find("Robot Partner").GetComponent<Animator>();
        robotAIScript = GameObject.Find("Robot Partner").GetComponent<RobotAI>();
        robotAIScript.enabled = false;
        robotAIanimator.enabled = false;

        executive = gameObject.GetComponent<Executive>();
        propManager = gameObject.GetComponent<PropManager>();
        listOfGestureDefinitions = new List<GestureDefinition>();
        agentPlayer = gameObject.GetComponent<AgentPlayer>();
        gestureIndex = 0;

        streamingAssetsPath = Application.streamingAssetsPath;
    }

    /// <summary>
    /// Fixed update method.
    /// </summary>
    void FixedUpdate()
    {

    }

    void Update()
    {
        HandleInput();
    }


    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isPlaying)
            {
                //Stop Everything
                isPlaying = false;
                agentPlayer.Reset();
            } else
            {
                StartAgentPlay();
                isPlaying = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //Move to the next action in the queue
            //propManager.GenerateSpecificProp("GET PROP NAME FROM FILE");
            if (gestureIndex < listOfGestureDefinitions.Count - 1)
            {
                gestureIndex++;
            } else
            {
                gestureIndex = 0;
            }
            Debug.Log("Gesture Index: " + gestureIndex + " of " + (listOfGestureDefinitions.Count - 1));
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //Move to the last action in the queue

            if (gestureIndex > 0)
            {
                gestureIndex--;
            } else
            {
                gestureIndex = listOfGestureDefinitions.Count - 1;
            }
            Debug.Log("Gesture Index: " + gestureIndex + " of " + (listOfGestureDefinitions.Count - 1));
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            //Stop everything
            isPlaying = false;
            agentPlayer.Reset();
        }
        
        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("C Pressed");
            //Calculate the Scores
            try
            {
                CalculateScores(listOfGestureDefinitions);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR: " + e.Message);
            }
        }
        
        
        if(Input.GetKeyDown(KeyCode.S))
        {
            RecordProps();
        }

        
        if (Input.GetKeyDown(KeyCode.M))
        {
            MakeFiles();
            //RecordTimeseries27000();
        }
        
    }

    public void StartAgentPlay()
    {
        //Play action from agentPlayer
        //executive.AddNewGesture(listOfGestureDefinitions[0]);

        propManager.ResetProp();
        propManager.GenerateSpecificProp(listOfGestureDefinitions[gestureIndex].propObject.name);

        robotAIanimator.enabled = true;
        robotAIScript.enabled = true;
        robotAIScript.setupAnimation();
    }

    public void CalculateScores(List<GestureDefinition> gestures)
    {
        string path = streamingAssetsPath + "/gestureScores.txt";

        //Create file if it doesnt exist
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "");
        }

        List<GestureDefinition> trimmedGestures = new List<GestureDefinition>();

        foreach(GestureDefinition gesture in gestures)
        {
            trimmedGestures.Add(executive.TrimGesture(gesture, Executive.dataType));
        }

        foreach (GestureDefinition gesture in trimmedGestures)
        {
            //Debug.Log("Player States: " + gesture.playerStates.Count);

            //GestureDefinition trimmed = executive.TrimGesture(gesture, Executive.dataType);

            //calculate the score
            //executive.PostLocalizationUpdate(gesture);
            executive.FindCreativeSpaceLocation(gesture);
            executive.PostLocalizationUpdate(gesture);
        }
        foreach(GestureDefinition gesture in trimmedGestures)
        {
            //GestureDefinition trimmed = executive.TrimGesture(gesture, Executive.dataType);

            float[] scores = executive.FindCreativeSpaceLocation(gesture);
            string line = "Novelty: " + scores[0] + "; Surprise: " + scores[1] + "; Value: " + scores[2];
            File.AppendAllText(path, line + Environment.NewLine);
        }
        Debug.Log("Calculated Scores");
    }

    private void MakeFiles()
    {
        Debug.Log("M Pressed");
        //NOVELTY

        string noveltyTimeseries = streamingAssetsPath + "/NoveltyTimeSeries.timeseries";
        string noveltyAffordanceLabels = streamingAssetsPath + "/NoveltyAffordanceLabels.labels";
        string noveltyPretendObjectLabels = streamingAssetsPath + "/NoveltyPretendObjectLabels.labels";
        string noveltyPretendActionLabels = streamingAssetsPath + "/NoveltyPretendActionLabels.labels";

        //Create file if it doesnt exist
        if (!File.Exists(noveltyTimeseries))
        {
            File.WriteAllText(noveltyTimeseries, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(noveltyAffordanceLabels))
        {
            File.WriteAllText(noveltyAffordanceLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(noveltyPretendObjectLabels))
        {
            File.WriteAllText(noveltyPretendObjectLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(noveltyPretendActionLabels))
        {
            File.WriteAllText(noveltyPretendActionLabels, "");
        }


        //VALUE

        string valueTimeseries = streamingAssetsPath + "/ValueTimeSeries.timeseries";
        string valueAffordanceLabels = streamingAssetsPath + "/ValueAffordanceLabels.labels";
        string valuePretendObjectLabels = streamingAssetsPath + "/ValuePretendObjectLabels.labels";
        string valuePretendActionLabels = streamingAssetsPath + "/ValuePretendActionLabels.labels";

        //Create file if it doesnt exist
        if (!File.Exists(valueTimeseries))
        {
            File.WriteAllText(valueTimeseries, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(valueAffordanceLabels))
        {
            File.WriteAllText(valueAffordanceLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(valuePretendObjectLabels))
        {
            File.WriteAllText(valuePretendObjectLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(valuePretendActionLabels))
        {
            File.WriteAllText(valuePretendActionLabels, "");
        }


        //SURPRISE

        string surpriseTimeseries = streamingAssetsPath + "/SurpriseTimeSeries.timeseries";
        string surpriseAffordanceLabels = streamingAssetsPath + "/SurpriseAffordanceLabels.labels";
        string surprisePretendObjectLabels = streamingAssetsPath + "/SurprisePretendObjectLabels.labels";
        string surprisePretendActionLabels = streamingAssetsPath + "/SurprisePretendActionLabels.labels";

        //Create file if it doesnt exist
        if (!File.Exists(surpriseTimeseries))
        {
            File.WriteAllText(surpriseTimeseries, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(surpriseAffordanceLabels))
        {
            File.WriteAllText(surpriseAffordanceLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(surprisePretendObjectLabels))
        {
            File.WriteAllText(surprisePretendObjectLabels, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(surprisePretendActionLabels))
        {
            File.WriteAllText(surprisePretendActionLabels, "");
        }

        int[] linesToSaveNoveltyHigh = new int[] { 71, 406, 707, 740, 742, 768, 789, 797, 853, 906 }; //High
        int[] linesToSaveNoveltyLow = new int[] { 34, 67, 169, 242, 262, 398, 418, 498, 499, 545 }; //Low
        int[] linesToSaveNoveltyMid = new int[] { 11, 17, 48, 49, 56, 57, 59, 77, 98, 263, 291, 392, 435, 596, 702, 718, 756, 832, 926, 958 }; //Mid

        int[] linesToSaveValueHigh = new int[] { 308, 782, 783, 843, 901, 917, 944, 947, 953, 967 }; //High
        int[] linesToSaveValueLow = new int[] { 141, 145, 321, 337, 352, 413, 552, 908, 924, 934 }; //Low
        int[] linesToSaveValueMid = new int[] { 20, 32, 68, 72, 106, 115, 121, 187, 237, 266, 279, 296, 304, 323, 324, 340, 346, 348, 355, 357 }; //Mid

        int[] linesToSaveSurpriseHigh = new int[] { 111, 145, 230, 285, 413, 438, 518, 582, 768, 900 }; //High
        int[] linesToSaveSurpriseLow = new int[] { 190, 213, 317, 342, 505, 539, 546, 586, 607, 822 }; //Low
        int[] linesToSaveSurpriseMid = new int[] { 33, 41, 72, 100, 102, 256, 265, 447, 473, 494, 584, 596, 597, 638, 669, 726, 734, 781, 867, 909 }; //Mid

        StreamReader timeseriesReader = null;
        StreamReader labelsReader = null;
        StreamReader pretendObjectReader = null;
        StreamReader pretendActionReader = null;

        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");
        labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
        pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
        pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");

        string timeseriesLine;
        string affordanceLine;
        string objectLine;
        string actionLine;
        int fileIndex = 0;

        int linesToSaveNoveltyIndex = 0;
        int linesToSaveValueIndex = 0;
        int linesToSaveSurpriseIndex = 0;

        //Add all the high values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            affordanceLine = labelsReader.ReadLine();
            objectLine = pretendObjectReader.ReadLine();
            actionLine = pretendActionReader.ReadLine();

            //Novelty
            if (linesToSaveNoveltyHigh[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(noveltyAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveNoveltyIndex < linesToSaveNoveltyHigh.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueHigh[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(valueAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(valuePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(valuePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveValueIndex < linesToSaveValueHigh.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseHigh[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(surpriseAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(surprisePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(surprisePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveSurpriseIndex < linesToSaveSurpriseHigh.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }

            fileIndex++;
        }

        Debug.Log("Added High Values");

        //Reset Values
        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");
        labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
        pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
        pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");

        timeseriesLine = "";
        affordanceLine = "";
        objectLine = "";
        actionLine = "";
        fileIndex = 0;

        linesToSaveNoveltyIndex = 0;
        linesToSaveValueIndex = 0;
        linesToSaveSurpriseIndex = 0;

        //Add all the low values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            affordanceLine = labelsReader.ReadLine();
            objectLine = pretendObjectReader.ReadLine();
            actionLine = pretendActionReader.ReadLine();

            //Novelty
            if (linesToSaveNoveltyLow[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(noveltyAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveNoveltyIndex < linesToSaveNoveltyLow.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueLow[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(valueAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(valuePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(valuePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveValueIndex < linesToSaveValueLow.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseLow[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(surpriseAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(surprisePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(surprisePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveSurpriseIndex < linesToSaveSurpriseLow.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }

            fileIndex++;
        }

        Debug.Log("Added Low Values");

        //Reset Values
        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");
        labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
        pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
        pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");

        timeseriesLine = "";
        affordanceLine = "";
        objectLine = "";
        actionLine = "";
        fileIndex = 0;

        linesToSaveNoveltyIndex = 0;
        linesToSaveValueIndex = 0;
        linesToSaveSurpriseIndex = 0;

        //Add all the mid values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            affordanceLine = labelsReader.ReadLine();
            objectLine = pretendObjectReader.ReadLine();
            actionLine = pretendActionReader.ReadLine();

            //Novelty
            if (linesToSaveNoveltyMid[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(noveltyAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(noveltyPretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveNoveltyIndex < linesToSaveNoveltyMid.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueMid[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(valueAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(valuePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(valuePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveValueIndex < linesToSaveValueMid.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseMid[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                File.AppendAllText(surpriseAffordanceLabels, affordanceLine + Environment.NewLine);
                File.AppendAllText(surprisePretendObjectLabels, objectLine + Environment.NewLine);
                File.AppendAllText(surprisePretendActionLabels, actionLine + Environment.NewLine);

                if (linesToSaveSurpriseIndex < linesToSaveSurpriseMid.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }

            fileIndex++;
        }

        Debug.Log("Added Mid Values");

        Debug.Log("Done");
    } 

    private void RecordProps()
    {
        string path = streamingAssetsPath + "/propsUsed.txt";

        //Create file if it doesnt exist
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "");
        }

        foreach (GestureDefinition gesture in listOfGestureDefinitions)
        {
            string line = gesture.propObject.name;
            File.AppendAllText(path, line + Environment.NewLine);
        }
        Debug.Log("Wrote Prop Names");
    }

    private void RecordTimeseries27000()
    {
        Debug.Log("S Pressed");
        //NOVELTY

        string noveltyTimeseries = streamingAssetsPath + "/NoveltyTimeSeries27000.timeseries";
        string surpriseTimeseries = streamingAssetsPath + "/SurpriseTimeSeries27000.timeseries";
        string valueTimeseries = streamingAssetsPath + "/ValueTimeSeries27000.timeseries";

        //Create file if it doesnt exist
        if (!File.Exists(noveltyTimeseries))
        {
            File.WriteAllText(noveltyTimeseries, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(surpriseTimeseries))
        {
            File.WriteAllText(surpriseTimeseries, "");
        }

        //Create file if it doesnt exist
        if (!File.Exists(valueTimeseries))
        {
            File.WriteAllText(valueTimeseries, "");
        }

        int[] linesToSaveNoveltyHigh = new int[] { 1, 126, 322, 419, 446, 472, 481, 562, 623, 656, 668 }; //High
        int[] linesToSaveNoveltyLow = new int[] { 332, 396, 459, 473, 502, 512, 598, 608, 728, 735 }; //Low
        int[] linesToSaveNoveltyMid = new int[] { 95, 200, 232, 305, 429, 469, 504, 521, 542, 584, 586, 601, 663, 682, 692, 702, 705, 717, 768, 946 }; //Mid

        int[] linesToSaveValueHigh = new int[] { 13, 290, 299, 307, 308, 371, 394, 408, 448, 544 }; //High
        int[] linesToSaveValueLow = new int[] { 168, 755, 763, 765, 767, 791, 936, 953, 955, 966 }; //Low
        int[] linesToSaveValueMid = new int[] { 18, 24, 32, 43, 78, 82, 86, 92, 111, 144, 170, 179, 762, 798, 815, 841, 873, 892, 893, 969 }; //Mid

        int[] linesToSaveSurpriseHigh = new int[] { 145, 216, 362, 414, 481, 607, 787, 923, 955, 968 }; //High
        int[] linesToSaveSurpriseLow = new int[] { 153, 159, 169, 209, 371, 560, 617, 637, 639, 903 }; //Low
        int[] linesToSaveSurpriseMid = new int[] { 15, 69, 73, 86, 101, 192, 309, 347, 379, 391, 400, 476, 611, 625, 674, 734, 891, 897, 935, 971 }; //Mid

        StreamReader timeseriesReader = null;
        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-27000.timeseries");
        string timeseriesLine;
        int fileIndex = 1;

        int linesToSaveNoveltyIndex = 0;
        int linesToSaveValueIndex = 0;
        int linesToSaveSurpriseIndex = 0;

        //Add all the high values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            //Novelty
            if (linesToSaveNoveltyHigh[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveNoveltyIndex < linesToSaveNoveltyHigh.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueHigh[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveValueIndex < linesToSaveValueHigh.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseHigh[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveSurpriseIndex < linesToSaveSurpriseHigh.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }
            fileIndex++;
        }

        Debug.Log("Added High Values");

        //Reset Values
        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");

        timeseriesLine = "";
        fileIndex = 1;

        linesToSaveNoveltyIndex = 0;
        linesToSaveValueIndex = 0;
        linesToSaveSurpriseIndex = 0;

        //Add all the mid values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            //Novelty
            if (linesToSaveNoveltyLow[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveNoveltyIndex < linesToSaveNoveltyLow.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueLow[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveValueIndex < linesToSaveValueLow.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseLow[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveSurpriseIndex < linesToSaveSurpriseLow.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }
            fileIndex++;
        }
        Debug.Log("Added Low Values");

        //Reset Values
        timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");

        timeseriesLine = "";
        fileIndex = 1;

        linesToSaveNoveltyIndex = 0;
        linesToSaveValueIndex = 0;
        linesToSaveSurpriseIndex = 0;

        //Add all the mid values
        while ((timeseriesLine = timeseriesReader.ReadLine()) != null)
        {
            //Novelty
            if (linesToSaveNoveltyMid[linesToSaveNoveltyIndex] == fileIndex)
            {
                File.AppendAllText(noveltyTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveNoveltyIndex < linesToSaveNoveltyMid.Length - 1)
                {
                    linesToSaveNoveltyIndex++;
                }
            }

            //Value
            if (linesToSaveValueMid[linesToSaveValueIndex] == fileIndex)
            {
                File.AppendAllText(valueTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveValueIndex < linesToSaveValueMid.Length - 1)
                {
                    linesToSaveValueIndex++;
                }
            }

            //Surprise
            if (linesToSaveSurpriseMid[linesToSaveSurpriseIndex] == fileIndex)
            {
                File.AppendAllText(surpriseTimeseries, timeseriesLine + Environment.NewLine);
                if (linesToSaveSurpriseIndex < linesToSaveSurpriseMid.Length - 1)
                {
                    linesToSaveSurpriseIndex++;
                }
            }
            fileIndex++;
        }
        Debug.Log("Added Mid Values");
        Debug.Log("Done");
    }
}