﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The main class for handling the audience's feedback
/// </summary>
public class FeedbackParticlesystemManager : MonoBehaviour
{
    private List<GameObject> audienceMemberObjectList;
    private int audienceCount = 84; //7 per row x 4 rows per section x 3 sections.
    private int rows = 21;
    private int columns = 4;
    public int audienceRegionsHorizontal = 7;
    public int audienceRegionsVertical = 2;
    private List<HashSet<Transform>> audienceSetsHorizontal;
    private List<HashSet<Transform>> audienceSetsVertical;
    private Dictionary<Vector3, GameObject> feedbackSystemsDictionary;

    private Material material0;
    private Material material1;
    private Material material2;
    private Material material3;

    int feedbackLevel;

    private AudioSource HCApplause;
    private AudioSource SRApplause;
    private AudioSource SLApplause;

    /// <summary>
    /// Starts the component
    /// </summary>
    void Start()
    {
        SLApplause = GameObject.Find("SL Applause").GetComponent<AudioSource>();
        HCApplause = GameObject.Find("HC Applause").GetComponent<AudioSource>();
        SRApplause = GameObject.Find("SR Applause").GetComponent<AudioSource>();

        feedbackLevel = 0;

        material0 = Resources.Load<Material>("01-Like");
        material1 = Resources.Load<Material>("02-Smiley");
        material2 = Resources.Load<Material>("03-Clown");
        material3 = Resources.Load<Material>("04-Love");

        audienceMemberObjectList = new List<GameObject>(GameObject.FindGameObjectsWithTag("AudienceMember"));

        audienceSetsHorizontal = new List<HashSet<Transform>>();
        for(int i = 0; i < audienceRegionsHorizontal; i++)
        {
            audienceSetsHorizontal.Add(new HashSet<Transform>());
        }

        audienceSetsVertical = new List<HashSet<Transform>>();
        for(int i = 0; i < audienceRegionsVertical; i++)
        {
            audienceSetsVertical.Add(new HashSet<Transform>());
        }

        GameObject AudienceFeedbackParent = GameObject.Find("AudienceFeedback");

        feedbackSystemsDictionary = new Dictionary<Vector3, GameObject>();
        int count = 0;
        foreach(GameObject audience in audienceMemberObjectList)
        {
            count++;
            string[] nameParts = audience.name.Split('.');
            int horizontalIndex = int.Parse(nameParts[2]) / (rows / audienceRegionsHorizontal);
            int verticalIndex = int.Parse(nameParts[1]) / (columns / audienceRegionsVertical);
            Transform headTransform = null;
            if(audience.transform.Find("RobotType1") != null)
            {
                headTransform = audience.transform.Find("Armature_Robot1").transform.Find("hips").transform.Find("spine").transform.Find("chest").transform.Find("neck").transform.Find("head").transform;
            }
            else if(audience.transform.Find("RobotType2") != null)
            {
                headTransform = audience.transform.Find("Hips").transform.Find("Spine").transform.Find("Chest").transform.Find("Neck").transform.Find("head").transform;
            }
            else if(audience.transform.Find("RobotType3") != null)
            {
                headTransform = audience.transform.Find("Armature").transform.Find("Hips").transform.Find("Spine").transform.Find("Chest").transform.Find("Neck").transform.Find("Head").transform;
            }
            audienceSetsHorizontal[horizontalIndex].Add(headTransform);
            audienceSetsVertical[verticalIndex].Add(headTransform);

            feedbackSystemsDictionary.Add(headTransform.position, new GameObject("ParticleSystem" + audience.name));
            GameObject particleSystemObject = feedbackSystemsDictionary[headTransform.position];
            particleSystemObject.transform.SetParent(AudienceFeedbackParent.transform);
            particleSystemObject.transform.position = headTransform.position;
            particleSystemObject.transform.rotation = headTransform.rotation;
            particleSystemObject.AddComponent<ParticleSystem>();
            ParticleSystem ps = particleSystemObject.GetComponent<ParticleSystem>();
            ps.Stop();
            var main = ps.main;
            main.duration = 1f;
            main.simulationSpeed = 1f;
            main.loop = false;
            main.gravityModifier = -1f;
            main.startLifetime = 2f;
            main.startSpeed = 1f;
            main.maxParticles = 3;
            var shape = ps.shape;
            shape.shapeType = ParticleSystemShapeType.ConeVolume;
            shape.angle = 15f;
            shape.radius = 1f;
            particleSystemObject.transform.GetComponent<ParticleSystemRenderer>().material = material0;
            particleSystemObject.transform.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.VerticalBillboard;
        }
        audienceCount = count;
    }

    /// <summary>
    /// Updates the component
    /// </summary>
    void Update()
    {
        HandleKeyboardInput();
    }

    public void HandleKeyboardInput()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            int horizontal = 0;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SLApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.W))
        {
            int horizontal = 1;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SLApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.E))
        {
            int horizontal = 2;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            int horizontal = 3;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            int horizontal = 4;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.Y))
        {
            int horizontal = 5;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SRApplause.Play();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            int horizontal = 6;
            int vertical = 1;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SRApplause.Play();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            int horizontal = 0;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SLApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.S))
        {
            int horizontal = 1;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SLApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            int horizontal = 2;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            int horizontal = 3;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.G))
        {
            int horizontal = 4;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            HCApplause.Play();
        }
        if(Input.GetKeyDown(KeyCode.H))
        {
            int horizontal = 5;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SRApplause.Play();
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            int horizontal = 6;
            int vertical = 0;
            PlayAudinceFeedbackFromGroup(horizontal, vertical);
            SRApplause.Play();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            feedbackLevel = 0;
            SwitchFeedbackTexture(feedbackLevel);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            feedbackLevel = 1;
            SwitchFeedbackTexture(feedbackLevel);
        }
        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            feedbackLevel = 2;
            SwitchFeedbackTexture(feedbackLevel);
        }
        if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            feedbackLevel = 3;
            SwitchFeedbackTexture(feedbackLevel);
        }
    }

    public void PlayAudinceFeedbackFromGroup(int horizontal, int vertical)
    {
        HashSet<Transform> set = new HashSet<Transform>(audienceSetsHorizontal[horizontal]);
        set.IntersectWith(audienceSetsVertical[vertical]);
        foreach (Transform transform in set)
        {
            feedbackSystemsDictionary[transform.position].GetComponent<ParticleSystem>().Play();
        }
    }

    /// <summary>
    /// Changes the emojis the audience uses to respond
    /// </summary>
    /// <param name="feedbackLevel">The emoji to be used</param>
    public void SwitchFeedbackTexture(int feedbackLevel)
    {
        Material material = null;
        switch (feedbackLevel)
        {
            case 0:
            {
                material = material0;
                break;
            }
            case 1:
            {
                material = material1;
                    break;
                }
            case 2:
            {
                material = material2;
                    break;
                }
            case 3:
            {
                material = material3;
                    break;
            }
            default:
            {
                break;
            }
        }

        foreach (GameObject psObject in feedbackSystemsDictionary.Values)
        {
            psObject.transform.GetComponent<ParticleSystemRenderer>().material = material;
            psObject.transform.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.VerticalBillboard;
        }
    }

    /// <summary>
    /// Adjusts the volume of the audience's applause
    /// </summary>
    /// <param name="feedbackLevel">How loud the sound should be.
    ///  0 being the quietest and 3 being the loudest</param>
    public void ControlApplauseParameters(int feedbackLevel)
    {
        float volume = 0;

        switch (feedbackLevel)
        {
            case 0:
                {
                    volume = 0.25f;
                    break;
                }
            case 1:
                {
                    volume = 0.5f;
                    break;
                }
            case 2:
                {
                    volume = 0.75f;
                    break;
                }
            case 3:
                {
                    volume = 1f;
                    break;
                }
            default:
                {
                    break;
                }
        }

        SLApplause.volume = volume;
        HCApplause.volume = volume;
        SRApplause.volume = volume;
    }

    public void PlayApplause()
    {
        SLApplause.Play();
        HCApplause.Play();
        SRApplause.Play();
    }
}
