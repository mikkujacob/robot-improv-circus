﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Crosstales.RTVoice;
using UnityEngine.UI;

public class AudienceSpeechBubble : MonoBehaviour
{
    private Vector3 initialAgentPosition;
    private float deltaX;
    private float deltaY;
    private Vector3 initialPosition;
    public static Text speechTextAudienceView;
    private AgentPlayer playback;
    private GameObject agentObj;
    public string VoiceName;

    private void Start()
    {
        initialAgentPosition = GameObject.FindGameObjectWithTag("Agent").transform.position;
        initialPosition = transform.position;
        speechTextAudienceView = GameObject.FindGameObjectWithTag("SpeechTextAudienceView").GetComponent<Text>();
        agentObj = GameObject.FindGameObjectWithTag("Agent");
        playback = GameObject.FindGameObjectWithTag("MImEGlobalObject").GetComponent<AgentPlayer>();
    }

    public void ChangeDisplayText()
    {
        if (speechTextAudienceView != null)
        {
            speechTextAudienceView.text = "I am " + playback.recording.GetGesture().pretendActionName + "ing with my " + playback.recording.GetGesture().pretendObjectName;
        }
    }

    public void DeleteDisplayText()
    {
        if (speechTextAudienceView != null)
        {
            speechTextAudienceView.text = " ";
        }
    }

    public void SayHm()
    {
        if (speechTextAudienceView != null)
        {
            speechTextAudienceView.text = "Hmmm...";
        }
    }

    void LateUpdate()
    {
        deltaX = GameObject.FindGameObjectWithTag("Agent").transform.position.x - initialAgentPosition.x;
        deltaY = GameObject.FindGameObjectWithTag("Agent").transform.position.y - initialAgentPosition.y;
        transform.position = initialPosition + new Vector3(deltaX, deltaY, 0);
        //transform.position = transform.position + new Vector3(-1.6f, 4.4f, 2.0f);
    }

    public void Speak()
    {
        if (speechTextAudienceView != null)
        {
            Speaker.Speak(speechTextAudienceView.text, null, Speaker.VoiceForName(VoiceName));
        }
        else
        {
            // Debug.Log("Speech Text Audience View was null.");
        }
    }

}
