﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using NewtonVR;

//TODO: Extra Scoreboard for the Audience?
//BUG: During Demo, if on Agent's turn during demo and press enter, does not spawn prop for player
public class LevelManager : MonoBehaviour
{
    //if true use the original turn structure
    //if false set the num of turns for each round
    //each round is one prop
    public bool origTurnStructure;
    private int numTurns = 5;
    private int turnsCompleted = 0;
    private bool agentTurnOver;
    
    //The sound effect for the countdown timer
    private AudioSource timerSound;
    //The sound effect for the poof to vanish prop
    private AudioSource poofSound;

    //Get the smoke for the poof
    private GameObject smoke;

    private GameObject instructionText;
    private GameObject audienceInstructionText;
    private GameObject audienceTitleText;

    private GameObject turnText;
    private GameObject timerText;
    private GameObject timerNumberText;
    private GameObject roundText;
    private GameObject roundNumberText;

    private GameObject instructionPanel;
    private GameObject audienceInstructionPanel;
    private GameObject audienceTitlePanel;

    //private GameObject turnPanel;
    //private GameObject timerPanel;
    //private GameObject roundPanel;
    //private GameObject roundNumberPanel;

    private GameObject agentScoreText;
    private GameObject agentScoreNumber;

    private GameObject playerScoreText;
    private GameObject playerScoreNumber;

    public enum StateType { START, INSTRUCTIONS, DEMO, ROUND, END };
    public enum TurnType { ROUNDOVER, PLAYER, AGENT }

    private StateType currentState;
    private StateType previousState;
    private StateType[] stateTypes;
    private int stateTypeCount;
    private int currentStateIndex;

    public TurnType currentTurn;
    private TurnType previousTurn;
    private TurnType[] turnTypes;
    private int turnTypeCount;
    private int currentTurnIndex;

    private int time;
    public int agentTimeLimit = 30;
    public int playerTimeLimit = 30;

    int playerScore = 0;
    int agentScore = 0;

    private string agentScoreTextString;
    private string playerScoreTextString;

    private string startString;
    private string instructionsString;
    private string endString;
    private string playerTurnString;
    private string agentTurnString;
    private string roundString;
    private string timerString;
    private string demoPlayerTurnString;
    private string demoAgentTurnString;
    private string audienceTitleString;
    private int roundNumber = 0;

    HumanPlayer humanPlayer;
    AgentPlayer agentPlayer;
    DeepIMAGINATIONInterface DeepIMAGINATION;
    FeedbackParticlesystemManager feedback;
    PlayerStateRecording recorder;
    PropManager propManager;
    ButtonController buttonController;
    Executive executive;

    private Animator robotAIanimator;
    private RobotAI robotAIScript;

    // Use this for initialization
    void Start()
    {
        //get the countdown timer
        timerSound = GameObject.Find("TimerSound").GetComponent<AudioSource>();
        //set the volume for the timer sound
        timerSound.volume = 1f;

        //get the poof sound
        poofSound = GameObject.Find("PoofSound").GetComponent<AudioSource>();
        //set the volume for the poof sound
        poofSound.volume = 1f;

        DeepIMAGINATION = gameObject.GetComponent<DeepIMAGINATIONInterface>();
        feedback = gameObject.GetComponent<FeedbackParticlesystemManager>();
        recorder = gameObject.GetComponent<PlayerStateRecording>();
        propManager = gameObject.GetComponent<PropManager>();
        humanPlayer = gameObject.GetComponent<HumanPlayer>();
        agentPlayer = gameObject.GetComponent<AgentPlayer>();
        buttonController = gameObject.GetComponent<ButtonController>();
        executive = gameObject.GetComponent<Executive>();

        instructionText = GameObject.Find("Instructions");
        audienceInstructionText = GameObject.Find("AudienceInstructions");
        audienceTitleText = GameObject.Find("AudienceTitle");

        turnText = GameObject.Find("Turn");
        turnText.GetComponent<Text>().text = "";
        timerText = GameObject.Find("Timer");
        timerNumberText = GameObject.Find("TimerNumberText");
        timerNumberText.GetComponent<Text>().text = "";
        roundText = GameObject.Find("Round");
        roundNumberText = GameObject.Find("RoundNumberText");
        roundNumberText.GetComponent<Text>().text = "";

        instructionPanel = GameObject.Find("InstructionsPanel");
        audienceInstructionPanel = GameObject.Find("AudienceInstructionsPanel");
        audienceTitlePanel = GameObject.Find("AudienceTitlePanel");

        //turnPanel = GameObject.Find("TurnPanel");
        //timerPanel = GameObject.Find("TimerPanel");
        //roundPanel = GameObject.Find("RoundPanel");
        //roundNumberPanel = GameObject.Find("Round Number Panel");

        agentScoreText = GameObject.Find("AgentScoreText");
        agentScoreNumber = GameObject.Find("AgentScoreNumberText");
        agentScoreNumber.GetComponent<Text>().text = "";

        playerScoreText = GameObject.Find("PlayerScoreText");
        playerScoreNumber = GameObject.Find("PlayerScoreNumberText");
        playerScoreNumber.GetComponent<Text>().text = "";

        agentScoreTextString = "AGENT SCORE";
        playerScoreTextString = "PLAYER SCORE";

        startString = "Welcome to the Robot Improv Circus!\n\nWould you like to play the Props game?";
        instructionsString = "Take turns to improvise actions with your robot partner.\nUse the mystery props.\nYour turn will be " + playerTimeLimit + " seconds long.\n\nHave fun!";
        endString = "Thank you for playing!\n\nCome back soon!";
        playerTurnString = "YOUR TURN";
        agentTurnString = "AGENT'S TURN";
        roundString = "ROUND: ";
        timerString = "TIME";
        demoPlayerTurnString = "YOUR TURN (TRIAL)";
        demoAgentTurnString = "AGENT'S TURN (TRIAL)";
        audienceTitleString = "INSTRUCTIONS IN VR";

        stateTypes = (StateType[])Enum.GetValues(typeof(StateType));
        stateTypeCount = stateTypes.Length;
        currentStateIndex = 0;
        //Sets current and previous state to START
        currentState = stateTypes[currentStateIndex];
        previousState = stateTypes[currentStateIndex];
        SwitchState(currentState);

        turnTypes = (TurnType[])Enum.GetValues(typeof(TurnType));
        turnTypeCount = turnTypes.Length;
        currentTurnIndex = 0;
        //Sets the current turn and previous turn to ROUNDOVER
        currentTurn = turnTypes[currentTurnIndex];
        previousTurn = turnTypes[currentTurnIndex];

        robotAIanimator = GameObject.Find("Robot Partner").GetComponent<Animator>();
        robotAIScript = GameObject.Find("Robot Partner").GetComponent<RobotAI>();
        robotAIScript.enabled = false;
        robotAIanimator.enabled = false;

        //CHANGE TO GO BACK TO ORIGINAL STRUCTURE
        origTurnStructure = false;
        agentPlayer.origTurnStructure = origTurnStructure;
        agentTurnOver = false;
        agentPlayer.agentTurnOver = agentTurnOver;
    }

    // Update is called once per frame
    public void Update()
    {
        HandleKeyboardInput();

        UpdateTime();

        if (currentState == StateType.ROUND)
        {
            UpdateScoreBoard();
        }

        if (!origTurnStructure)
        {
            agentTurnOver = agentPlayer.agentTurnOver;
            if (agentTurnOver)
            {
                //Invoke("ToggleTurn", 3);
                ToggleTurn();
                SwitchTurn(currentTurn);
                agentPlayer.agentTurnOver = false;
            }
        }
    }

    public void UpdateTime()
    {
        if (timerNumberText.activeSelf)
        {
            timerNumberText.GetComponent<Text>().text = time.ToString();
        }
	}

    /// <summary>
    /// Manages switching between states
    /// States can be Start, Instructions, Round, or End
    /// </summary>
    /// <param name="state"></param>
	public void SwitchState(StateType state)
    {
        if (currentState == StateType.START)
        {
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = startString;
            audienceInstructionText.GetComponent<Text>().text = startString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
        else if (currentState == StateType.INSTRUCTIONS)
        {
            agentPlayer.InitializeOriginalIK();
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = instructionsString;
            audienceInstructionText.GetComponent<Text>().text = instructionsString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
        else if (currentState == StateType.DEMO)
        {
            robotAIanimator.enabled = true;

            TurnOnTimerPanel();
            instructionText.GetComponent<Text>().text = "";
            audienceInstructionText.GetComponent<Text>().text = "";
            audienceTitleText.GetComponent<Text>().text = "";

            roundText.GetComponent<Text>().text = roundString;
            agentScoreText.GetComponent<Text>().text = agentScoreTextString;
            playerScoreText.GetComponent<Text>().text = playerScoreTextString;

            propManager.ResetProp();
            propManager.GenerateProp();

            SwitchTurnDemo(currentTurn);
        }
        else if (currentState == StateType.ROUND)
        {
            ResetScoreBoard();

            propManager.ResetProp();
            propManager.GenerateProp();

            TurnOnTimerPanel();
            instructionText.GetComponent<Text>().text = "";
            audienceInstructionText.GetComponent<Text>().text = "";
            audienceTitleText.GetComponent<Text>().text = "";

            roundText.GetComponent<Text>().text = roundString;
            agentScoreText.GetComponent<Text>().text = agentScoreTextString;
            playerScoreText.GetComponent<Text>().text = playerScoreTextString;
            SwitchTurn(currentTurn);
        }
        else if (currentState == StateType.END)
        {
            ResetScoreBoard();
            TurnOffTimerPanel();
            instructionText.GetComponent<Text>().text = endString;
            audienceInstructionText.GetComponent<Text>().text = endString;
            audienceTitleText.GetComponent<Text>().text = audienceTitleString;
        }
    }

    /// <summary>
    /// Switches between turns
    /// Turns can be Player or Agent
    /// </summary>
    /// <param name="turn"></param>
    public void SwitchTurn(TurnType turn)
    {
        if (currentTurn == TurnType.PLAYER)
        {
            turnText.GetComponent<Text>().text = playerTurnString;
            humanPlayer.StartPlay();
            if (origTurnStructure)
            {
                StartTimer(playerTimeLimit);
            }
            recorder.StartRecording();
        }
        else if (currentTurn == TurnType.AGENT)
        {
            turnText.GetComponent<Text>().text = agentTurnString;
            //DeepIMAGINATION.GenerateAction();

            robotAIanimator.enabled = true;
            robotAIScript.enabled = true;
            robotAIScript.setupAnimation();
            //CHANGE AGENT PLAYER
            //agentPlayer.StartPlay();

            if (origTurnStructure)
            {
                StartTimer(agentTimeLimit);
            }
            agentScore = agentScore + 10;

        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            propManager.GenerateProp();
            ToggleTurn();
            SwitchTurn(currentTurn);
        }
    }

    public void SwitchTurnDemo(TurnType turn)
    {
        if (currentTurn == TurnType.PLAYER)
        {
            turnText.GetComponent<Text>().text = demoPlayerTurnString;
            humanPlayer.StartPlay();
        }
        else if (currentTurn == TurnType.AGENT)
        {
            turnText.GetComponent<Text>().text = demoAgentTurnString;
            //DeepIMAGINATION.GenerateAction();
            robotAIanimator.enabled = true;
            robotAIScript.enabled = true;
            robotAIScript.setupAnimation();
            //CHANGE AGENT PLAYER
            //agentPlayer.StartPlay();
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            propManager.GenerateProp();
            ToggleTurnDemo();
            SwitchTurnDemo(currentTurn);
        }
    }


    public void HandleKeyboardInput()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            executive.ChangeCurentCurve("rising");
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            executive.ChangeCurentCurve("falling");
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            executive.ChangeCurentCurve("flat");
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (currentState == StateType.DEMO)
            {
                ToggleTurnDemo();
                SwitchTurnDemo(currentTurn);
            }
            else if (currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (currentState == StateType.DEMO)
            {
                ToggleTurnDemo();
                SwitchTurnDemo(currentTurn);
            }
            else if (currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            previousTurn = currentTurn;
            currentTurnIndex = 0;
            currentTurn = TurnType.ROUNDOVER;

            currentStateIndex = (currentStateIndex < stateTypeCount - 1) ? currentStateIndex + 1 : 0;
            previousState = currentState;
            currentState = stateTypes[currentStateIndex];
            SwitchState(currentState);

            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }

            if (currentState != StateType.ROUND)
            {
                CancelInvoke("DecrementTimer");
            }
            TurnOffTimer();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            previousTurn = currentTurn;
            currentTurnIndex = 0;
            currentTurn = TurnType.ROUNDOVER;

            currentStateIndex = (currentStateIndex < stateTypeCount - 1) ? currentStateIndex + 1 : 0;
            previousState = currentState;
            currentState = stateTypes[currentStateIndex];
            SwitchState(currentState);

            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }

            if (currentState != StateType.ROUND)
            {
                CancelInvoke("DecrementTimer");
            }
        }
    }

    public void ToggleTurn()
    {
        if (currentTurn == TurnType.PLAYER)
        {
            //if the turn gets changed stop the timer
            TurnOffTimer();
            feedback.PlayApplause();
            recorder.StopRecording();
            humanPlayer.StopPlay();
            previousTurn = currentTurn;
            currentTurn = TurnType.AGENT;
        }
        else if (currentTurn == TurnType.AGENT)
        {
            feedback.PlayApplause();
            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }
            previousTurn = currentTurn;
            turnsCompleted++;
            if (origTurnStructure)
            {
                currentTurn = TurnType.ROUNDOVER;
            } else
            {
                if(turnsCompleted == numTurns)
                {
                    currentTurn = TurnType.ROUNDOVER;
                    turnsCompleted = 0;
                } else
                {
                    propManager.GenerateProp();
                    currentTurn = TurnType.PLAYER;
                }
            }
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            roundNumber++;
            roundNumberText.GetComponent<Text>().text = "0" + roundNumber.ToString();
            previousTurn = currentTurn;
            currentTurn = TurnType.PLAYER;
        }
    }

    public void ToggleTurnDemo()
    {
        if (currentTurn == TurnType.PLAYER)
        {
            feedback.PlayApplause();
            humanPlayer.StopPlay();
            previousTurn = currentTurn;
            //currentTurn = TurnType.ROUNDOVER;
            currentTurn = TurnType.AGENT;

        }
        else if (currentTurn == TurnType.AGENT)
        {
            feedback.PlayApplause();
            if (agentPlayer.IsPlaying())
            {
                agentPlayer.StopPlay();
            }
            previousTurn = currentTurn;
            //currentTurn = TurnType.PLAYER;
            currentTurn = TurnType.ROUNDOVER;
        }
        else if (currentTurn == TurnType.ROUNDOVER)
        {
            roundNumberText.GetComponent<Text>().text = "0" + roundNumber.ToString();
            previousTurn = currentTurn;
            //currentTurn = TurnType.AGENT;
            currentTurn = TurnType.PLAYER;
        }
    }

    public void changeTurnNotOrigStructure()
    {
        if (!origTurnStructure)
        {
            if (currentTurn == TurnType.PLAYER)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }
        }
    }

    public void StartTimer(int time)
    {
        CancelInvoke("DecrementTimer");
        this.time = time;
        InvokeRepeating("DecrementTimer", 1.0f, 1.0f);
    }

    public void DecrementTimer()
    {
        time--;
        if (time == 0)
        {
            CancelInvoke("DecrementTimer");
            propManager.ResetProp();
            poofSound.Play();
            if (currentState == StateType.ROUND)
            {
                ToggleTurn();
                SwitchTurn(currentTurn);
            }

        }
        //trigger the timer sound to play
        if (time == 5)
        {
            timerSound.Play();
        }
    }

    public void TurnOffTimer()
    {
        //if the turn gets changed stop the timer
        if (timerSound.isPlaying)
        {
            timerSound.Stop();
        }
    }

    private void UpdateScoreBoard()
    {
        playerScore = buttonController.GetPlayerScore();
        string agentScoreString = agentScore.ToString();
        agentScoreNumber.GetComponent<Text>().text = agentScoreString;
        playerScoreNumber.GetComponent<Text>().text = playerScore.ToString();

        if (origTurnStructure)
        {
            if (buttonController.GetButtonDown() && currentTurn == TurnType.PLAYER)
            {
                recorder.StopRecording();
                recorder.StartRecording();
            }
        }
    }

    private void ResetScoreBoard()
    {
        agentScore = 0;
        buttonController.SetPlayerScore(0);
        roundNumber = 0;
        time = 0;
        UpdateScoreBoard();
        previousTurn = currentTurn;
        currentTurn = TurnType.ROUNDOVER;
        currentTurnIndex = 0;
    }

    public void IncreaseAgentScore()
    {
        agentScore = agentScore + 10;
    }

    /// <summary>
    /// Turns off the timer and watch panels //TODO: FIX THIS TO SEE IF IT ACTUALLY NEEDS TO BE TURNED ON OR OFF
    /// Turns on the instruction text
    /// </summary>
    public void TurnOffTimerPanel()
    {
        instructionText.SetActive(true);
        audienceInstructionText.SetActive(true);
        audienceTitleText.SetActive(true);

        instructionPanel.SetActive(true);
        audienceInstructionPanel.SetActive(true);
        audienceTitlePanel.SetActive(true);

        //agentScoreNumber.SetActive(false);
        //agentScoreNumberPanel.SetActive(false);
        //agentScoreText.SetActive(false);
        //agentScoreTextPanel.SetActive(false);

        //playerScoreNumber.SetActive(false);
        //playerScoreNumberPanel.SetActive(false);
        //playerScoreText.SetActive(false);
        //playerScoreTextPanel.SetActive(false);

        //roundNumberText.SetActive(false);
        //roundNumberPanel.SetActive(false);

        //roundText.SetActive(false);
        //roundPanel.SetActive(false);
        //turnText.SetActive(false);
        //turnPanel.SetActive(false);
        //timerText.SetActive(false);
        //timerNumberText.SetActive(false);
        //timerPanel.SetActive(false);
    }

    /// <summary>
    /// Turns on the timer and watch panels
    /// Turns off the instruction text
    /// </summary>
    public void TurnOnTimerPanel()
    {
        instructionText.SetActive(false);
        audienceInstructionText.SetActive(false);
        audienceTitleText.SetActive(false);

        instructionPanel.SetActive(false);
        audienceInstructionPanel.SetActive(false);
        audienceTitlePanel.SetActive(false);

        agentScoreNumber.SetActive(true);
        //agentScoreNumberPanel.SetActive(true);
        agentScoreText.SetActive(true);
        //agentScoreTextPanel.SetActive(true);

        playerScoreNumber.SetActive(true);
        //playerScoreNumberPanel.SetActive(true);
        playerScoreText.SetActive(true);
        //playerScoreTextPanel.SetActive(true);

        roundNumberText.SetActive(true);
        //roundNumberPanel.SetActive(true);

        roundText.SetActive(true);
        //roundPanel.SetActive(true);
        turnText.SetActive(true);
        //turnPanel.SetActive(true);
        timerText.SetActive(true);
        timerNumberText.SetActive(true);
        //timerPanel.SetActive(true);
    }
}
