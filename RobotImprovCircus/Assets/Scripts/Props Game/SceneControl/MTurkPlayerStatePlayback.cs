﻿using System.Collections.Generic;
using RootMotion.FinalIK;
using UnityEngine;
using System;
using System.IO;

public class MTurkPlayerStatePlayback : MonoBehaviour
{
    private DeepIMAGINATIONInterface DeepIMAGINATION;
    private GifRecording GIFRecorder;

    private GUITextControl textControl;

    private string streamingAssetsPath;

    public RecordedGesture recording;
    private int currentRecordingIndex;

    //Constant for smoothing between 0 and 1. Lower value is smoother but has more lag and less variation.
    public float EMASmoothingAlphaParameter = 0.2f;

    //+/- this value is how much distance a point will be from 0,0 in a near variant
    public float xNoiseScaleNearVariant = 0.1f;
    //+/- this value is how much distance a point will be from 0,0 in a near variant
    public float yNoiseScaleNearVariant = 0.1f;
    //+/- this value is how much distance a point will be from 0,0 in a far variant
    public float xNoiseScaleFarVariant = 2.0f;
    //+/- this value is how much distance a point will be from 0,0 in a far variant
    public float yNoiseScaleFarVariant = 2.0f;

    private bool isInit;
    private bool isPlaying;

    private string currentObjectName;
    private Dictionary<string, GameObject> gameObjectDictionary;
    private List<GameObject> gameObjectKeyList;
    private Transform propSpawnLocation;
    private Dictionary<string, Vector3> localScales = new Dictionary<string, Vector3> { { "SMALL", new Vector3(0.25f, 0.25f, 0.25f) }, { "MEDIUM", new Vector3(0.5f, 0.5f, 0.5f) }, { "LARGE", new Vector3(1f, 1f, 1f) } };

    private GameObject playerObject;
    private Transform headTransform;
    private Transform pelvisTransform;
    private Transform leftHandTransform;
    private Transform rightHandTransform;

    private GameObject IKObject;
    private VRIK IK;
    private Transform[] skeletalTransforms;
    private FrozenGraphLoader model;
    public int GesturesLoaded { get; private set; } = 0;
    private List<GestureDefinition> listOfGestureDefinitions;
    private float[,] param;
    private float[] affordance;
    private List<float> vals;

    private string whichHand = "right";
    public GestureType DataSetType;

    private List<string> generatedNameCacheList;

    public enum GestureType { Human, AgentMimicry, NearVariant, FarVariant };

    /// <summary>
    /// Start this instance.
    /// </summary>
    void Start()
    {
        isInit = false;
        streamingAssetsPath = Application.streamingAssetsPath;
        listOfGestureDefinitions = new List<GestureDefinition>();
        generatedNameCacheList = new List<string>();

        textControl = GameObject.Find("Text").GetComponent<GUITextControl>();

        gameObjectDictionary = new Dictionary<string, GameObject>();
        gameObjectKeyList = new List<GameObject>();
        gameObjectKeyList.AddRange(GameObject.FindGameObjectsWithTag("ObjectStateTracker"));
        foreach (GameObject gameObj in gameObjectKeyList)
        {
            gameObjectDictionary.Add(gameObj.name, gameObj);
        }

        propSpawnLocation = GameObject.Find("PropSpawnLocation").transform;

        //Call InitializeTracking() after a slight delay.
        Invoke("InitializeTracking", 0.5f);

        //Call LoadGesturesFromDisk() after a slight delay.
        Invoke("LoadGesturesFromDisk", 3.0f);
    }

    private void LoadGesturesFromDisk()
    {
        if (Directory.Exists(streamingAssetsPath))
        {
            // timeseries .timeseries file
            // affordance .labels file
            // pretend action .labels file
            // pretend object .labels file
            if (Directory.GetFiles(streamingAssetsPath, "*.labels").Length >= 3 && Directory.GetFiles(streamingAssetsPath, "*.timeseries").Length >= 1)
            {
                // Load gestures from timeseries and labels file
                StreamReader timeseriesReader = null;
                StreamReader labelsReader = null;
                StreamReader pretendObjectReader = null;
                StreamReader pretendActionReader = null;

                if (Executive.dataType == DataEnum.Data27000)
                {
                    timeseriesReader = new StreamReader(streamingAssetsPath + "/MTurkTimeseries.timeseries");
                    labelsReader = new StreamReader(streamingAssetsPath + "/MTurkAffordanceLabels.labels");
                    pretendObjectReader = new StreamReader(streamingAssetsPath + "/MTurkObjectLabels.labels");
                    pretendActionReader = new StreamReader(streamingAssetsPath + "/MTurkActionLabels.labels");
                }
                else if (Executive.dataType == DataEnum.Data16000)
                {
                    Debug.Log("ERROR: Don't have timeseries for 16000 model. Switch back to the 27000 model please.");
                }

                int filesloaded = 0;
                // int filestoload = 59; //TODO: REMOVE AFTER TESTING! FULL DATASET IS CURRENTLY 59
                string pretendObjectLabel;
                while ((pretendObjectLabel = pretendObjectReader.ReadLine()) != null)// && filesloaded < filestoload) //TODO: REMOVE " && filesloaded < filestoload" CONDITION AFTER TESTING!
                {
                    float[] inputTimeSeries = Array.ConvertAll<string, float>(timeseriesReader.ReadLine().Split(','), StringToFloat);
                    affordance = Array.ConvertAll<string, float>(labelsReader.ReadLine().Split(','), StringToFloat);

                    //string pretendObjectLabel = pretendObjectReader.ReadLine();
                    string pretendActionLabel = pretendActionReader.ReadLine();
                    pretendActionLabel = pretendActionLabel.Substring(0, pretendActionLabel.IndexOf("."));

                    var affordanceKey = affordance;
                    //Debug.Log("KEY:  " + toStringFloat(affordanceKey));
                    string propName = "";

                    if(generatedNameCacheList.Count > filesloaded)
                    {
                        propName = generatedNameCacheList[filesloaded];
                    }
                    else
                    {
                        propName = propAffordanceNameLookup[toStringFloat(affordanceKey)];

                        if (propName.Equals("Squid w Tentacles Partless OR Horseshoe"))
                        {
                            if (UnityEngine.Random.value <= 0.5F)
                            {
                                propName = "Squid w Tentacles Partless";
                            }
                            else
                            {
                                propName = "Horseshoe";
                            }
                        }

                        generatedNameCacheList.Add(propName);
                    }

                    GestureDefinition gesture = null;
                    switch (DataSetType)
                    {
                        case GestureType.Human:
                        {
                            gesture = GenerateHumanGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel);
                            GifRecording.dataset = "Dataset1";
                            GifRecording.whichLine = 0;
                            break;
                        }
                        case GestureType.AgentMimicry:
                        {
                            gesture = GenerateAgentMimicryGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel);
                            GifRecording.dataset = "Dataset2";
                            GifRecording.whichLine = 0;
                            break;
                        }
                        case GestureType.NearVariant:
                        {
                            gesture = GenerateNearVariantGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel);
                            GifRecording.dataset = "Dataset3";
                            GifRecording.whichLine = 0;
                            break;
                        }
                        case GestureType.FarVariant:
                        {
                            gesture = GenerateFarVariantGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel);
                            GifRecording.dataset = "Dataset4";
                            GifRecording.whichLine = 0;
                            break;
                        }
                        default:
                        {
                            gesture = GenerateHumanGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel);
                            GifRecording.dataset = "Dataset1";
                            GifRecording.whichLine = 0;
                            break;
                        }
                    }

                    listOfGestureDefinitions.Add(gesture);
                    Debug.Log("listOfGestureDefinition:" + listOfGestureDefinitions[listOfGestureDefinitions.Count - 1].pretendObjectName);

                    GesturesLoaded += 1;

                    filesloaded++;
                }
            }
            else
            {
                throw new Exception("No gestures files exists in StreamingAssets Folder");
            }

            if (listOfGestureDefinitions.Count > 0)
            {
                recording = new RecordedGesture(listOfGestureDefinitions[0], DataEnum.Data27000);
                currentRecordingIndex = 0;

                if (recording != null)
                {
                    currentObjectName = recording.GetGesture().propObject.name;
                    GameObject propObj = gameObjectDictionary[currentObjectName];
                    propObj.transform.position = propSpawnLocation.position;
                    propObj.transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
                    propObj.transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
                    propObj.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", UnityEngine.Random.ColorHSV(0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
                    propObj.transform.GetChild(0).localScale = localScales["MEDIUM"];//recording.GetGesture().propObject.size];
                    Debug.Log("Initialized prop for recording");
                }
                else
                {
                    Debug.Log("Recording was null!");
                }

                if (recording != null)
                {
                    textControl.SetTimedMessage("Cued next gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex], 2);
                    Debug.Log("Cued next gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex]);
                }
                else
                {
                    Debug.Log("Recording was null!");
                }
            }
            else
            {
                Debug.Log("No gestures loaded");
            }
        }
    }

    /// <summary>
    /// Generates a human gesture from disk.
    /// </summary>
    /// <returns>The human gesture.</returns>
    /// <param name="inputTimeSeries">Input time series.</param>
    /// <param name="propName">Property name.</param>
    /// <param name="pretendActionLabel">Pretend action label.</param>
    /// <param name="pretendObjectLabel">Pretend object label.</param>
    private GestureDefinition GenerateHumanGestureFromDisk(float[] inputTimeSeries, string propName, string pretendActionLabel, string pretendObjectLabel)
    {
        float[,] paramjointdata = new float[1, inputTimeSeries.Length];

        for (int i = 0; i < inputTimeSeries.Length; i++)
        {
            paramjointdata[0, i] = inputTimeSeries[i];
        }

        GestureDefinition gesture = new GestureDefinition(paramjointdata, propName, true, 0, Executive.dataType);

        gesture.IsGestureTooShort = false;

        gesture.pretendActionName = pretendActionLabel;
        gesture.pretendObjectName = pretendObjectLabel;

        return gesture;
    }

    /// <summary>
    /// Generates the agent mimicry gesture from disk. Calls a helper method and passes in 0 noise 
    /// to get the exact mimicry of the human gesture by the agent.
    /// </summary>
    /// <returns>The agent mimicry gesture from disk.</returns>
    /// <param name="inputTimeSeries">Input time series.</param>
    /// <param name="affordance">Affordance.</param>
    /// <param name="propName">Property name.</param>
    /// <param name="pretendActionLabel">Pretend action label.</param>
    /// <param name="pretendObjectLabel">Pretend object label.</param>
    private GestureDefinition GenerateAgentMimicryGestureFromDisk(float[] inputTimeSeries, string propName, string pretendActionLabel, string pretendObjectLabel)
    {
        return GenerateVariantGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel, 0f, 0f);
    }

    /// <summary>
    /// Generates the near variant gesture from disk. Calls a helper method and passes in a small amount of noise
    /// to get a near variant of the human gesture by the agent.
    /// </summary>
    /// <returns>The near variant gesture from disk.</returns>
    /// <param name="inputTimeSeries">Input time series.</param>
    /// <param name="affordance">Affordance.</param>
    /// <param name="propName">Property name.</param>
    /// <param name="pretendActionLabel">Pretend action label.</param>
    /// <param name="pretendObjectLabel">Pretend object label.</param>
    private GestureDefinition GenerateNearVariantGestureFromDisk(float[] inputTimeSeries, string propName, string pretendActionLabel, string pretendObjectLabel)
    {
        return GenerateVariantGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel, xNoiseScaleNearVariant, yNoiseScaleNearVariant);
    }

    /// <summary>
    /// Generates the far variant gesture from disk. Calls a helper method and passes in a large amount of noise
    /// to get a far variant of the human gesture by the agent.
    /// </summary>
    /// <returns>The far variant gesture from disk.</returns>
    /// <param name="inputTimeSeries">Input time series.</param>
    /// <param name="affordance">Affordance.</param>
    /// <param name="propName">Property name.</param>
    /// <param name="pretendActionLabel">Pretend action label.</param>
    /// <param name="pretendObjectLabel">Pretend object label.</param>
    private GestureDefinition GenerateFarVariantGestureFromDisk(float[] inputTimeSeries, string propName, string pretendActionLabel, string pretendObjectLabel)
    {
        return GenerateVariantGestureFromDisk(inputTimeSeries, propName, pretendActionLabel, pretendObjectLabel, xNoiseScaleFarVariant, yNoiseScaleFarVariant);
    }

    /// <summary>
    /// Helper method to generate variant gestures from disk with the amount of noise passed in as parameters.
    /// </summary>
    /// <returns>The variant gesture from disk.</returns>
    /// <param name="inputTimeSeries">Input time series.</param>
    /// <param name="affordance">Affordance.</param>
    /// <param name="propName">Property name.</param>
    /// <param name="pretendActionLabel">Pretend action label.</param>
    /// <param name="pretendObjectLabel">Pretend object label.</param>
    /// <param name="XNoiseScale">X noise scale. This is +- the amount of noise for the X coordinate in latent space</param>
    /// <param name="YNoiseScale">Y noise scale. This is +- the amount of noise for the Y coordinate in latent space</param>
    private GestureDefinition GenerateVariantGestureFromDisk(float[] inputTimeSeries, string propName, string pretendActionLabel, string pretendObjectLabel, float XNoiseScale, float YNoiseScale)
    {
        param = new float[1, inputTimeSeries.Length + affordance.Length];
        float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);

        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }

        Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>()
        {
            { "concat", param },
            { "keep_prob", 1.0f }
        };

        // Generating output inference
        float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
        //Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
        float[,] outputSTDevs = model.GenerateOutput(generatorInputs, "encoder/dense_1/BiasAdd") as float[,];
        //Debug.Log("Result successfully obtained. Results in form: " + outputSTDevs.GetLength(0) + "x" + outputSTDevs.GetLength(1));

        // Finding the input's latent space value
        vals = new List<float>();
        foreach (float val in outputMeans)
        {
            vals.Add(val);
        }
        foreach (float val in outputSTDevs)
        {
            vals.Add(val);
        }
        vals.AddRange(affordance);

        float[,] paramjointdata = new float[1, inputTimeSeries.Length];

        for (int i = 0; i < inputTimeSeries.Length; i++)
        {
            paramjointdata[0, i] = inputTimeSeries[i];
        }

        //NOTE: Assumes 2D latent space in DeepIMAGINATION.
        float xAngle = UnityEngine.Random.value * 2 * Mathf.PI;
        float yAngle = UnityEngine.Random.value * 2 * Mathf.PI;
        Vector2 XYCoordinatesOnCircle = new Vector2(Mathf.Cos(xAngle) * XNoiseScale, Mathf.Sin(yAngle) * YNoiseScale);
        Debug.Log("X: " + XYCoordinatesOnCircle.x + ", Y: " + XYCoordinatesOnCircle.y + ", Distance: " + Vector2.Distance(XYCoordinatesOnCircle, new Vector2()));
        GestureDefinition gesture = DeepIMAGINATION.FindAction(new float[] { XYCoordinatesOnCircle.x, XYCoordinatesOnCircle.y }, propName);

        gesture.EMASmootheGesture(EMASmoothingAlphaParameter);

        gesture.IsGestureTooShort = false;

        gesture.pretendActionName = pretendActionLabel;
        gesture.pretendObjectName = pretendObjectLabel;

        Guid guid = Executive.GenerateGuid(vals, DateTime.Now);
        gesture.gestureID = guid.ToString();

        gesture.coordinateIndex = new GestureCoordinate(new Envelope(vals[0], vals[1]), guid, pretendActionLabel, pretendObjectLabel, propName);

        return gesture;
    }

    /// <summary>
    /// Initializes the tracking.
    /// </summary>
    void InitializeTracking()
    {
        DeepIMAGINATION = FindObjectOfType<DeepIMAGINATIONInterface>();
        model = DeepIMAGINATION.GetModel();

        GIFRecorder = FindObjectOfType<GifRecording>();

        playerObject = GameObject.FindGameObjectWithTag("NVR Player");

        IKObject = GameObject.FindGameObjectWithTag("PlayerAvatar");
        IK = IKObject.GetComponent<VRIK>();
        //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
        skeletalTransforms = IK.references.GetTransforms();

        headTransform = playerObject.transform.Find("DummyHead").transform;
        pelvisTransform = playerObject.transform.Find("DummyPelvis").transform;
        leftHandTransform = playerObject.transform.Find("DummyLeftHand").transform;
        rightHandTransform = playerObject.transform.Find("DummyRightHand").transform;

        Debug.Log("Tracking Initialized");
        textControl.SetTimedMessage("Tracking Initialized", 1);

        foreach (GameObject gameObj in gameObjectKeyList)
        {
            gameObj.SetActive(false);
        }
        isInit = true;
    }

    /// <summary>
    /// Fixed update method.
    /// </summary>
    void FixedUpdate()
    {
        if (isInit && isPlaying)
        {
            ApplyRecordedData(recording.GetNearestFrameInterpolated(System.DateTime.Now));
        }
    }

    void Update()
    {
        if (isInit)
        {
            HandleInput();
        }
    }

    void ApplyRecordedData(PlayerStateDefinition nextState)
    {
        PlayerStateDefinition a = nextState;
        headTransform.position = nextState.headPosition.ToVector3();
        headTransform.rotation = nextState.headRotation.ToQuaternion();

        pelvisTransform.position = nextState.skeletalPositions[1].ToVector3();
        pelvisTransform.rotation = nextState.skeletalRotations[1].ToQuaternion();

        leftHandTransform.position = nextState.leftHandPosition.ToVector3();
        leftHandTransform.rotation = nextState.leftHandRotation.ToQuaternion();

        rightHandTransform.position = nextState.rightHandPosition.ToVector3();
        rightHandTransform.rotation = nextState.rightHandRotation.ToQuaternion();

        if (Executive.dataType == DataEnum.Data27000)
        {
            currentObjectName = recording.GetGesture().propObject.name;
            GameObject prop = gameObjectDictionary[currentObjectName];

            //prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
            //prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;

            if (whichHand == "right")
            {
                Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
                Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
                prop.transform.parent = rightHandTransform;
                prop.transform.localPosition = new Vector3();
                prop.transform.localRotation = new Quaternion();
                prop.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                rightHandTransform.position = position;
                rightHandTransform.rotation = rotation;
            }
            else
            {
                Vector3 position = new Vector3(leftHandTransform.position.x, leftHandTransform.position.y, leftHandTransform.position.z);
                Quaternion rotation = new Quaternion(leftHandTransform.rotation.x, leftHandTransform.rotation.y, leftHandTransform.rotation.z, leftHandTransform.rotation.w);
                prop.transform.parent = leftHandTransform;
                prop.transform.localPosition = new Vector3();
                prop.transform.localRotation = new Quaternion();
                prop.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                leftHandTransform.position = position;
                leftHandTransform.rotation = rotation;
            }
        }
        else
        {
            gameObjectDictionary[currentObjectName].transform.GetChild(0).gameObject.transform.position = nextState.objectPosition.ToVector3();
            gameObjectDictionary[currentObjectName].transform.GetChild(0).gameObject.transform.rotation = nextState.objectRotation.ToQuaternion();
        }
    }

    void DelayedPlayback()
    {
        recording.Play();

        Debug.Log("Playback Started...");
        textControl.SetTimedMessage("Playback Started...", 1);
        isPlaying = true;
    }

    void HandleInput()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            if(isPlaying)
            {
                isPlaying = false;

                gameObjectDictionary[currentObjectName].SetActive(false);

                Debug.Log("Playback Stopped...");
                textControl.SetTimedMessage("Playback stopped...", 1);

                recording.Stop();
            }
            listOfGestureDefinitions.Clear();
            LoadGesturesFromDisk();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (whichHand == "right")
            {
                whichHand = "left";
            }
            else
            {
                whichHand = "right";
            }
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            Transform store = leftHandTransform;
            leftHandTransform = rightHandTransform;
            rightHandTransform = store;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!isPlaying)
            {
                if (Executive.dataType == DataEnum.Data27000)
                {
                    currentObjectName = recording.GetGesture().propObject.name;
                    GameObject propObj = gameObjectDictionary[currentObjectName];

                    if (whichHand == "right")
                    {
                        Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
                        Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
                        propObj.transform.parent = rightHandTransform;
                        propObj.transform.localPosition = new Vector3();
                        propObj.transform.localRotation = new Quaternion();
                        propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                        propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                        propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                        rightHandTransform.position = position;
                        rightHandTransform.rotation = rotation;
                    }
                    else
                    {
                        Vector3 position = new Vector3(leftHandTransform.position.x, leftHandTransform.position.y, leftHandTransform.position.z);
                        Quaternion rotation = new Quaternion(leftHandTransform.rotation.x, leftHandTransform.rotation.y, leftHandTransform.rotation.z, leftHandTransform.rotation.w);
                        propObj.transform.parent = leftHandTransform;
                        propObj.transform.localPosition = new Vector3();
                        propObj.transform.localRotation = new Quaternion();
                        propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                        propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                        propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                        leftHandTransform.position = position;
                        leftHandTransform.rotation = rotation;
                    }
                }
                else
                {
                    gameObjectDictionary[currentObjectName].transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
                    gameObjectDictionary[currentObjectName].transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
                }

                gameObjectDictionary[currentObjectName].SetActive(true);
                DelayedPlayback();
            }
            else
            {
                isPlaying = false;

                gameObjectDictionary[currentObjectName].SetActive(false);

                Debug.Log("Playback Stopped...");
                textControl.SetTimedMessage("Playback stopped...", 1);

                recording.Stop();
            }
        }

        //TODO: Make sure to fix Left Arrow code, it doesn't work the same way currently and needs to be fixed.
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            isPlaying = false;
            recording.Stop();

            currentRecordingIndex = (currentRecordingIndex < listOfGestureDefinitions.Count - 1) ? currentRecordingIndex + 1 : 0;
            recording = new RecordedGesture(listOfGestureDefinitions[currentRecordingIndex], DataEnum.Data27000);

            gameObjectDictionary[currentObjectName].SetActive(false);
            currentObjectName = recording.GetGesture().propObject.name;
            GameObject propObj = gameObjectDictionary[currentObjectName];

            if (Executive.dataType == DataEnum.Data27000)
            {

                if (whichHand == "right")
                {
                    Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
                    Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
                    propObj.transform.parent = rightHandTransform;
                    propObj.transform.localPosition = new Vector3();
                    propObj.transform.localRotation = new Quaternion();
                    propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                    propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                    propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                    rightHandTransform.position = position;
                    rightHandTransform.rotation = rotation;
                }
                else
                {
                    Vector3 position = new Vector3(leftHandTransform.position.x, leftHandTransform.position.y, leftHandTransform.position.z);
                    Quaternion rotation = new Quaternion(leftHandTransform.rotation.x, leftHandTransform.rotation.y, leftHandTransform.rotation.z, leftHandTransform.rotation.w);
                    propObj.transform.parent = leftHandTransform;
                    propObj.transform.localPosition = new Vector3();
                    propObj.transform.localRotation = new Quaternion();
                    propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                    propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                    propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                    leftHandTransform.position = position;
                    leftHandTransform.rotation = rotation;
                }
            }
            else
            {
                propObj.transform.position = propSpawnLocation.position;
                propObj.transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
                propObj.transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
            }

            propObj.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", UnityEngine.Random.ColorHSV(0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
            propObj.transform.GetChild(0).localScale = localScales["MEDIUM"];//recording.GetGesture().propObject.size];
            propObj.SetActive(true);

            textControl.SetTimedMessage("Playback stopped. Cued next gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex], 1);
            Debug.Log("Playback stopped. Cued next gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex]);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            isPlaying = false;
            recording.Stop();

            currentRecordingIndex = (currentRecordingIndex > 0) ? currentRecordingIndex - 1 : listOfGestureDefinitions.Count - 1;
            recording = new RecordedGesture(listOfGestureDefinitions[currentRecordingIndex], DataEnum.Data27000);

            gameObjectDictionary[currentObjectName].SetActive(false);
            currentObjectName = recording.GetGesture().propObject.name;
            GameObject propObj = gameObjectDictionary[currentObjectName];

            if (Executive.dataType == DataEnum.Data27000)
            {

                if (whichHand == "right")
                {
                    Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
                    Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
                    propObj.transform.parent = rightHandTransform;
                    propObj.transform.localPosition = new Vector3();
                    propObj.transform.localRotation = new Quaternion();
                    propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                    propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                    propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                    rightHandTransform.position = position;
                    rightHandTransform.rotation = rotation;
                }
                else
                {
                    Vector3 position = new Vector3(leftHandTransform.position.x, leftHandTransform.position.y, leftHandTransform.position.z);
                    Quaternion rotation = new Quaternion(leftHandTransform.rotation.x, leftHandTransform.rotation.y, leftHandTransform.rotation.z, leftHandTransform.rotation.w);
                    propObj.transform.parent = leftHandTransform;
                    propObj.transform.localPosition = new Vector3();
                    propObj.transform.localRotation = new Quaternion();
                    propObj.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f); //MEDIUM size
                    propObj.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
                    propObj.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

                    leftHandTransform.position = position;
                    leftHandTransform.rotation = rotation;
                }
            }
            else
            {
                propObj.transform.position = propSpawnLocation.position;
                propObj.transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
                propObj.transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
            }

            propObj.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", UnityEngine.Random.ColorHSV(0f, 1f, 0.9f, 1f, 0.7f, 1f, 0.999f, 1f));
            propObj.transform.GetChild(0).localScale = localScales["MEDIUM"];//recording.GetGesture().propObject.size];
            propObj.SetActive(true);

            textControl.SetTimedMessage("Playback stopped. Cued previous gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex], 1);
            Debug.Log("Playback stopped. Cued previous gesture " + currentRecordingIndex + ": " + listOfGestureDefinitions[currentRecordingIndex]);

        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            isPlaying = false;
            recording.Stop();
            gameObjectDictionary[currentObjectName].SetActive(false);
            float[] samples = new float[DeepIMAGINATION.nLatents];
            for (int i = 0; i < DeepIMAGINATION.nLatents; i++)
            {
                samples[i] = (float)UnityEngine.Random.value * 4 - 3;
            }
            currentObjectName = DeepIMAGINATION.GetRandomKeyFromDictionary(DeepIMAGINATION.propNameAffordanceLookup);

            GestureDefinition gesture = DeepIMAGINATION.FindAction(samples, currentObjectName);
            recording = new RecordedGesture(gesture, Executive.dataType);
            Debug.Log("Generated gesture.");
        }
    }

    void OnApplicationQuit()
    {
        if (isInit && isPlaying)
        {
            isPlaying = false;

            if (recording != null)
            {
                recording.Stop();
                // m_Recorder.Save();
            }
            Debug.Log("Playback Stopped...");
            textControl.SetTimedMessage("Playback stopped...", 1);
        }
    }

    private static Dictionary<string, string> propAffordanceNameLookup = new Dictionary<string, string> {
        {"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Squid w Tentacles Partless OR Horseshoe"},
        //{"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Horseshoe"},
        {"0.6666667,0,0,0,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Lolliop w Cube Ends Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0.3333333,0,0,0,0.6666667,0.3333333,0,0,0,0,0", "Giant U w Flat Base Partless"},
        {"0,0.3333333,0.3333333,0,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0", "Big Curved Axe Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0,0,0.6666667,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0.3333333,0,0,0,0,0", "Sunflower Partless"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0", "Air Horn"},
        {"0,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Corn Dog"},
        {"0.3333333,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0,0.3333333,0,0,0,0,0", "Giant Electric Plug"},
        {"0,0.3333333,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0.3333333,0,0.6666667,0,0,0.3333333,0,0,0,0,0.3333333,0,0", "Ring w Tail End"},
        {"0,0.3333333,0,0,0,0.6666667,0,0.3333333,0.6666667,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0", "Ring w Inward Spokes"},
        {"0,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0", "Large Ring w Outward Spokes"},
        {"0,0.3333333,0,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0", "Tube w Cones"},
        {"0,0.6666667,0,0,0,0,0,0.6666667,0,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Thin Stick w Hammer End"},
        {"0,0,0,0,0,0,0.3333333,0,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0,0,0,0,0.3333333,0,0", "Helix"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Giant Golf T"},
        {"0,0,0,0.3333333,0,0.3333333,0,0,0,0.6666667,0,0,0,0,0.6666667,0,0,0,0.6666667,0,0,0,0,0,0", "Circle With Ring"},
        {"0,0,0,0,0.6666667,0,0,0,0.3333333,0.3333333,0,0.3333333,0.3333333,0,0,0,0,0,0.6666667,0,0,0,0,0,0", "Palm Tree Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Ladle"},
        {"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
        //{"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,1,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
    };

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }

    //Convert.ToSingle(Math.Round((decimal)affordanceKey[1], 7))
    private static float[] RoundFloat(float[] v, int dec)
    {
        float[] answer = new float[v.Length];
        for (int i = 0; i < v.Length; i++)
        {
            answer[i] = Convert.ToSingle(Math.Round((decimal)v[i], dec));
        }
        return answer;
    }

    private static string toStringFloat(float[] f_arr)
    {
        String answer = "";
        float[] rounded = RoundFloat(f_arr, 7);
        for (int i = 0; i < f_arr.Length - 1; i++)
        {
            answer += rounded[i].ToString();
            answer += ",";
        }
        answer += rounded[f_arr.Length - 1].ToString();
        return answer;
    }

    private static T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }
}