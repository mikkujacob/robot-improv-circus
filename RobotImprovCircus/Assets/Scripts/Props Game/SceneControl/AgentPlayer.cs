﻿using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using NewtonVR;
using RootMotion.FinalIK;
using UnityEngine;
using System;

public class AgentPlayer : Player
{

    public RecordedGesture recording;
    private Executive executive;

    private Animator robotAIanimator;

    PropManager propManager;
    private GameObject prop;
    private string propName;

    private bool isInit;
    private bool isPlaying = false;
    private bool isVRSpeechBubbleActive = true;
    private bool isAudienceSpeechBubbleActive = true;
    private GameObject IKTargetsObject;
    private Transform headTransform;
    private Transform pelvisTransform;
    private Transform leftHandTransform;
    private Transform rightHandTransform;
    private Transform initialRobotPartnerTransform;
    private Transform initialHeadTransform;
    private Transform initialPelvisTransform;
    private Transform initialLeftHandTransform;
    private Transform initialRightHandTransform;

    private Transform originalRobotPartnerTransform;
    private Transform originalHeadTransform;
    private Transform originalPelvisTransform;
    private Transform originalLeftHandTransform;
    private Transform originalRightHandTransform;
    private Vector3 originalHeadTransformPosition;
    private Vector3 originalPelvisTransformPosition;
    private Vector3 originalLeftHandTransformPosition;
    private Vector3 originalRightHandTransformPosition;
    private Quaternion originalHeadTransformRotation;
    private Quaternion originalPelvisTransformRotation;
    private Quaternion originalLeftHandTransformRotation;
    private Quaternion originalRightHandTransformRotation;

    private GameObject IKObject;
    private VRIK IK;
    private Transform[] skeletalTransforms;

    private Vector3 partnerScale;
    private Transform partnerMovementOriginTransform;
    private Transform propSpawnLocationTransform;

    private GestureDefinition currentGesture;
    private bool busy;

    private bool objectPositionGenerated;

    public int maxRandomGesturesPerTurn = 5;

    private GameObject speech;
    private SpeechBubble speechBubble;
    private GameObject audienceSpeech;
    private AudienceSpeechBubble audienceSpeechBubble;

    public bool origTurnStructure;
    public bool agentTurnOver;

    /*
    public bool recordingGifs;
    private SummerGifController summerGifController;
    */

    // Use this for initialization
    void Start()
    {
        /*
        recordingGifs = true;
        summerGifController = gameObject.GetComponent<SummerGifController>();
        */

        origTurnStructure = false;
        propManager = gameObject.GetComponent<PropManager>();

        executive = gameObject.GetComponent<Executive>();

        robotAIanimator = GameObject.Find("Robot Partner").GetComponent<Animator>();

        //prop = propManager.GetCurrentProp();
        //propName = propManager.GetCurrentPropName();

        isInit = false;
        isPlaying = false;
        InitializeTracking();

        if (Executive.dataType == DataEnum.Data27000)
        //if (DeepIMAGINATIONInterface.dataType == DataEnum.Data27000)
        {
            objectPositionGenerated = false;
        }
        else
        {
            objectPositionGenerated = true;
        }

        speech = GameObject.Find("VRSpeechBubbleImage");
        if (speech != null)
        {
            isVRSpeechBubbleActive = true;
            speechBubble = speech.GetComponent<SpeechBubble>();
        }
        else
        {
            isVRSpeechBubbleActive = false;
        }



        audienceSpeech = GameObject.Find("AudienceSpeechBubbleImage");
        if (audienceSpeech != null)
        {
            isAudienceSpeechBubbleActive = true;
            audienceSpeechBubble = audienceSpeech.GetComponent<AudienceSpeechBubble>();
        }
        else
        {
            isAudienceSpeechBubbleActive = false;
        }

        //Turn off Speech Bubble before starting play
        if (speech != null)
        {
            speech.SetActive(false);
            speechBubble.enabled = false;
            isVRSpeechBubbleActive = false;
        }

        if (audienceSpeech != null)
        {
            audienceSpeech.SetActive(false);
            audienceSpeechBubble.enabled = false;
            isAudienceSpeechBubbleActive = false;
        }
    }

    void FixedUpdate()
    {
        if (isInit && isPlaying)
        {
            GestureDefinition tempGesture;
            if (!busy)
            {
                if (executive.OutputQueue.Count != 0)
                {
                    // Debug.Log("OUTPUTQUEUE HAS CONTENTS: " + executive.OutputQueue.Count);
                    tempGesture = executive.ReadFromOutputQueue();
                    if (tempGesture != null)
                    {
                        // Debug.Log("GOT NON-NULL GESTURE: " + tempGesture.gestureID);
                        if (!tempGesture.IsGestureTooShort)
                        {
                            currentGesture = tempGesture;
                            // Debug.Log("GOT NEW GESTURE");
                        }
                        else
                        {
                            // Debug.Log("GESTURE IS TOO SHORT: " + tempGesture.gestureDuration);
                        }
                    }
                }
            }

            if (currentGesture != null && !busy)
            {
                ResetAgentPose();

                // Debug.Log("Ready to play non-null gesture");
                recording = new RecordedGesture(currentGesture);

                if (isVRSpeechBubbleActive)
                {
                    speechBubble.ChangeDisplayText();
                    speechBubble.Speak();
                }
                if (isAudienceSpeechBubbleActive)
                {
                    audienceSpeechBubble.ChangeDisplayText();
                }

                // Debug.Log("Displayed and spoke non-null gesture labels");
                busy = true;
                // Debug.Log("Busy: " + busy);
                if (recording == null)
                {
                    // Debug.Log("RECORDING IS NULL");
                }
                else
                {
                    // Debug.Log("Going to play recording.");
                    recording.Play();
                    // Debug.Log("Recording length: " + recording.gestureDuration);
                    // Debug.Log("Recording play start time: " + recording.playStartTime.ToString());
                }
            }

            if (currentGesture != null && recording != null && busy)
            {
                //Debug.Log("About to apply frame from recording");
                //Debug.Log("Current play time: " + System.DateTime.Now.ToString());
                ApplyRecordedData(recording.GetNearestFrameInterpolated(System.DateTime.Now));

                // Gesture is done playing
                if (recording.isFinishedPlaying)
                {
                    // Debug.Log("Finished playing gesture.");
                    StopCurrentRecording();
                    if (!origTurnStructure)
                    {
                        StopPlay();
                        agentTurnOver = true;
                    }
                }
            }
        }
    }

    public void StopCurrentRecording()
    {
        ResetAgentPose();
        if (recording != null)
        {
            recording.Stop();
        }

        // Debug.Log("Stopped playing one recording");
        currentGesture = null;
        busy = false;
    }

    void Update()
    {
        if (isInit)
        {
            HandleInput();
        }
    }

    /// <summary>
    /// Initializes the tracking.
    /// </summary>
    public void InitializeTracking()
    {
        IKTargetsObject = GameObject.FindGameObjectWithTag("IKTargets");

        IKObject = GameObject.FindGameObjectWithTag("Performer");
        IK = IKObject.GetComponent<VRIK>();

        initialRobotPartnerTransform = IKObject.GetComponent<Transform>();

        partnerScale = initialRobotPartnerTransform.localScale;

        //Joints: [root, pelvis, spine, chest, neck, head, leftShoulder, leftUpperArm, leftForearm, leftHand, rightShoulder, rightUpperArm, rightForearm, rightHand, leftThigh, leftCalf, leftFoot, leftToes, rightThigh, rightCalf, rightFoot, rightToes]
        skeletalTransforms = IK.references.GetTransforms();

        partnerMovementOriginTransform = GameObject.Find("PartnerMovementWorldOrigin").GetComponent<Transform>();

        headTransform = IKTargetsObject.transform.Find("IKHead").transform;
        pelvisTransform = IKTargetsObject.transform.Find("IKPelvis").transform;
        leftHandTransform = IKTargetsObject.transform.Find("IKLeftHand").transform;
        rightHandTransform = IKTargetsObject.transform.Find("IKRightHand").transform;

        initialHeadTransform = GameObject.Find("InitialIKHead").GetComponent<Transform>();
        initialPelvisTransform = GameObject.Find("InitialIKPelvis").GetComponent<Transform>();
        initialLeftHandTransform = GameObject.Find("InitialIKLeftHand").GetComponent<Transform>();
        initialRightHandTransform = GameObject.Find("InitialIKRightHand").GetComponent<Transform>();

        Debug.Log("Tracking Initialized");

        isInit = true;
    }

    public void InitializeOriginalIK()
    {
        originalRobotPartnerTransform = GameObject.Find("OriginalRobotPartnerLocation").transform;
        originalHeadTransform = GameObject.Find("OriginalIKHead").transform;
        originalPelvisTransform = GameObject.Find("OriginalIKPelvis").transform;
        originalLeftHandTransform = GameObject.Find("OriginalIKLeftHand").transform;
        originalRightHandTransform = GameObject.Find("OriginalIKRightHand").transform;

        originalHeadTransformPosition = new Vector3(originalHeadTransform.position.x, originalHeadTransform.position.y, originalHeadTransform.position.z);
        originalPelvisTransformPosition = new Vector3(originalPelvisTransform.position.x, originalPelvisTransform.position.y, originalPelvisTransform.position.z);
        originalLeftHandTransformPosition = new Vector3(originalLeftHandTransform.position.x, originalLeftHandTransform.position.y, originalLeftHandTransform.position.z);
        originalRightHandTransformPosition = new Vector3(originalRightHandTransform.position.x, originalRightHandTransform.position.y, originalRightHandTransform.position.z);

        originalHeadTransformRotation = new Quaternion(originalHeadTransform.rotation.x, originalHeadTransform.rotation.y, originalHeadTransform.rotation.z, originalHeadTransform.rotation.w);
        originalPelvisTransformRotation = new Quaternion(originalPelvisTransform.rotation.x, originalPelvisTransform.rotation.y, originalPelvisTransform.rotation.z, originalPelvisTransform.rotation.w);
        originalLeftHandTransformRotation = new Quaternion(originalLeftHandTransform.rotation.x, originalLeftHandTransform.rotation.y, originalLeftHandTransform.rotation.z, originalLeftHandTransform.rotation.w);
        originalRightHandTransformRotation = new Quaternion(originalRightHandTransform.rotation.x, originalRightHandTransform.rotation.y, originalRightHandTransform.rotation.z, originalRightHandTransform.rotation.w);
    }

    public void agentSpawnProp()
    {

        prop = propManager.GetCurrentProp();
        propName = propManager.GetCurrentPropName();
        prop.transform.localScale = propManager.GetCurrentPropSize();
        //prop.transform.GetChild(0).localScale = propManager.GetCurrentPropSize();

        propManager.SpawnAgentProp();
    }

    //Create bool in AgentPlayer that toggles from levelmanager for origTurnStructure
    //if ordinary turn strcuture stay the same but otherwise
    //if nothing on the output queue put a random gesture but only once
    //Need to add this in fixedupdate because you wont be able to see if something is on the output queue
    //may need to wait a little bit before you add the random gesture in order to account for processing time
    public void StartPlay()
    {
        InitializeTracking();
        isPlaying = true;

        
        if (speech != null)
        {
            speech.SetActive(true);
            speechBubble.enabled = true;
            isVRSpeechBubbleActive = true;
            speechBubble.SayHm();
        }

        if (audienceSpeech != null)
        {
            audienceSpeech.SetActive(true);
            audienceSpeechBubble.enabled = true;
            isAudienceSpeechBubbleActive = true;
            audienceSpeechBubble.SayHm();
        }
        

        //Start();
        //TODO:poof = propManager.GetPoof();

        //robotAIanimator.SetBool("agentPlaying", true);
        //robotAIanimator.enabled = false;

        //prop = propManager.GetCurrentProp();
        //propName = propManager.GetCurrentPropName();

        //executive.AddRandomGestureToOutputQueue(propName);

        if (origTurnStructure)
        {
            _ = Task.Run(() => executive.AddRandomGestureToOutputQueue(propName));

            for (int i = 0; i < maxRandomGesturesPerTurn - 1; i++) // Generates one guaranteed but rest are up to chance
            {
                bool generateRandom = UnityEngine.Random.value > 0.5f; // Coin toss
                if (generateRandom)
                {
                    _ = Task.Run(() => executive.AddRandomGestureToOutputQueue(propName));
                }
            }
        }
        /*
        else if(recordingGifs)
        {
            executive.AddSpecificGestureToOutPutQueue(summerGifController.listOfGestureDefinitions[summerGifController.gestureIndex]);
            //executive.AddNewGesture(summerGifController.listOfGestureDefinitions[0]);

        }
        */

        //else
        //{
        //    if (executive.OutputQueue.Count == 0)
        //    {
        //        Debug.Log("ADDED RANDOM");
        //        _ = Task.Run(() => executive.AddRandomGestureToOutputQueue(propName));
        //    }
        //}


        //propManager.SpawnProp();


        if (objectPositionGenerated)
        {
            //prop.transform.GetChild(0).gameObject.transform.position = recording.GetGesture().objectStartingPosition.ToVector3();
            //prop.transform.GetChild(0).gameObject.transform.rotation = recording.GetGesture().objectStartingRotation.ToQuaternion();
            //propManager.SetCurrentPropSize(propManager.GetCurrentPropSizeString());

            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            //Only for 27000
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;

            Vector3 position = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
            Quaternion rotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
            prop.transform.parent = rightHandTransform;
            prop.transform.localPosition = new Vector3();
            prop.transform.localRotation = new Quaternion();
            prop.transform.localScale = propManager.GetCurrentPropSize();
            prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
            prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

            rightHandTransform.position = position;
            rightHandTransform.rotation = rotation;
        }



        prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = false;
        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;
        prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3(rightHandTransform.position.x, rightHandTransform.position.y, rightHandTransform.position.z);
        //print(prop.transform.GetChild(0).gameObject.transform.localPosition);
        prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion(rightHandTransform.rotation.x, rightHandTransform.rotation.y, rightHandTransform.rotation.z, rightHandTransform.rotation.w);
        prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;


        recording = null;
        busy = false;

        // Debug.Log("Playback Started...");
        //TODO: set isPlaying = true when agent can pick up object.
        //isPlaying = true;
        //isPlaying = true;
    }

    public void CouplePropToHand()
    {
        //GameObject rightWrist = GameObject.Find("Right_Wrist_Joint_01");
        GameObject rightWrist = GameObject.Find("Right_Middle_Finger_Joint_01a");

        prop.transform.GetChild(0).position = rightWrist.transform.position;
        prop.transform.GetChild(0).rotation = rightWrist.transform.rotation;

        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().useGravity = false;

        //prop.transform.position = handlocation;
        //prop.transform.localPosition = handlocation;
        //prop.transform.GetChild(0).gameObject.transform.localPosition = handlocation;
        //prop.transform.GetChild(0).gameObject.transform.position = handlocation;
    }


    public void StopPlay()
    {
        busy = true;
        isPlaying = false;
        if (recording != null)
        {
            recording.Stop();
        }
        Reset();
        propManager.ResetProp();
        // Debug.Log("Playback Stopped...");
        if (isVRSpeechBubbleActive)
        {
            //SpeechBubble.speechTextVRView.text = " ";
            speechBubble.DeleteDisplayText();
        }
        if (isAudienceSpeechBubbleActive)
        {
            //AudienceSpeechBubble.speechTextAudienceView.text = " ";
            audienceSpeechBubble.DeleteDisplayText();
        }
    }

    public void ResetAgentPose()
    {
        IK.solver.SetIKPositionWeight(0);
        IK.solver.plantFeet = false;
        IK.solver.spine.positionWeight = 0;
        IK.solver.spine.rotationWeight = 0;
        IK.solver.spine.pelvisPositionWeight = 0;
        IK.solver.spine.pelvisRotationWeight = 0;
        IK.solver.leftArm.positionWeight = 0;
        IK.solver.leftArm.rotationWeight = 0;
        IK.solver.rightArm.positionWeight = 0;
        IK.solver.rightArm.rotationWeight = 0;
        IK.solver.locomotion.weight = 0;
        //IK.solver.leftLeg.positionWeight = 1;
        //IK.solver.leftLeg.rotationWeight = 1;
        //IK.solver.rightLeg.positionWeight = 1;
        //IK.solver.rightLeg.rotationWeight = 1;

        IKObject.transform.position = originalRobotPartnerTransform.position;
        IKObject.transform.rotation = originalRobotPartnerTransform.rotation;

        //headTransform.position = originalHeadTransformPosition;
        //headTransform.rotation = originalHeadTransformRotation;
        //pelvisTransform.position = originalPelvisTransformPosition;
        //pelvisTransform.rotation = originalPelvisTransformRotation;
        //leftHandTransform.position = originalLeftHandTransformPosition;
        //leftHandTransform.rotation = originalLeftHandTransformRotation;
        //rightHandTransform.position = originalRightHandTransformPosition;
        //rightHandTransform.rotation = originalRightHandTransformRotation;

        headTransform.position = initialHeadTransform.position;
        headTransform.rotation = initialHeadTransform.rotation;
        pelvisTransform.position = initialPelvisTransform.position;
        pelvisTransform.rotation = initialPelvisTransform.rotation;
        leftHandTransform.position = initialLeftHandTransform.position;
        leftHandTransform.rotation = initialLeftHandTransform.rotation;
        rightHandTransform.position = initialRightHandTransform.position;
        rightHandTransform.rotation = initialRightHandTransform.rotation;

        IK.solver.SetIKPositionWeight(1);
        IK.solver.plantFeet = true;
        IK.solver.spine.positionWeight = 1;
        IK.solver.spine.rotationWeight = 1;
        IK.solver.spine.pelvisPositionWeight = 1;
        IK.solver.spine.pelvisRotationWeight = 1;
        IK.solver.leftArm.positionWeight = 1;
        IK.solver.leftArm.rotationWeight = 1;
        IK.solver.rightArm.positionWeight = 1;
        IK.solver.rightArm.rotationWeight = 1;
        IK.solver.locomotion.weight = 1;
        //IK.solver.leftLeg.positionWeight = 0;
        //IK.solver.leftLeg.rotationWeight = 0;
        //IK.solver.rightLeg.positionWeight = 0;
        //IK.solver.rightLeg.rotationWeight = 0;
    }

    public void Reset()
    {
        ResetAgentPose();

        if (speech != null && speech.activeInHierarchy)
        {
            speechBubble.DeleteDisplayText();
            speech.SetActive(false);
            speechBubble.enabled = false;
            isVRSpeechBubbleActive = false;
        }

        if (audienceSpeech != null && audienceSpeech.activeInHierarchy)
        {
            audienceSpeechBubble.DeleteDisplayText();
            audienceSpeech.SetActive(false);
            audienceSpeechBubble.enabled = false;
            isAudienceSpeechBubbleActive = false;
        }

        prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().useGravity = true;

        if (!objectPositionGenerated)
        {
            //Only for 27000
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;

            prop.transform.parent = propManager.GetPropsObject().transform;
            prop.transform.localPosition = new Vector3();
            prop.transform.localRotation = new Quaternion();
            prop.transform.localScale = propManager.GetCurrentPropSize();
            prop.transform.GetChild(0).gameObject.transform.localPosition = new Vector3();
            prop.transform.GetChild(0).gameObject.transform.localRotation = new Quaternion();

        }
        else
        {
            //TODO: Test reset code for object position generated.
            prop.transform.GetChild(0).gameObject.GetComponent<MeshCollider>().enabled = true;
            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = false;

            prop.transform.localScale = propManager.GetCurrentPropSize();
        }
    }

    //used in LevelManager to know if the agent is playing or not
    public bool IsPlaying()
    {
        return isPlaying;
    }

    public void SetPlaying(bool play)
    {
        isPlaying = play;
    }

    void ApplyRecordedData(PlayerStateDefinition nextState)
    {

        headTransform.position = Vector3.Scale(nextState.headPosition.ToVector3(), partnerScale) + partnerMovementOriginTransform.position;
        headTransform.rotation = nextState.headRotation.ToQuaternion();

        leftHandTransform.position = Vector3.Scale(nextState.leftHandPosition.ToVector3(), partnerScale) + partnerMovementOriginTransform.position;
        leftHandTransform.rotation = nextState.leftHandRotation.ToQuaternion();

        rightHandTransform.position = Vector3.Scale(nextState.rightHandPosition.ToVector3(), partnerScale) + partnerMovementOriginTransform.position;
        rightHandTransform.rotation = nextState.rightHandRotation.ToQuaternion();

        pelvisTransform.position = Vector3.Scale(nextState.skeletalPositions[1].ToVector3(), partnerScale) + partnerMovementOriginTransform.position;
        pelvisTransform.rotation = nextState.skeletalRotations[1].ToQuaternion();

        if (objectPositionGenerated && prop != null && prop.activeInHierarchy && nextState.objectPosition != null && nextState.objectRotation != null)
        {
            //prop.transform.GetChild(0).position = Vector3.Scale(nextState.objectPosition.ToVector3(), partnerScale) + partnerMovementOriginTransform.position;
            //prop.transform.GetChild(0).rotation = nextState.objectRotation.ToQuaternion();

            //GameObject rightWrist = GameObject.Find("Right_Wrist_Joint_01");
            GameObject rightWrist = GameObject.FindGameObjectWithTag("AgentRightHand");
            GameObject leftWrist = GameObject.FindGameObjectWithTag("AgentLeftHand");
            //GameObject leftWrist = GameObject.Find("Left_Wrist_Joint_01");

            prop.transform.GetChild(0).gameObject.GetComponent<Rigidbody>().useGravity = false;
            
            /*
            prop.transform.GetChild(0).position = rightWrist.transform.position;
            prop.transform.GetChild(0).rotation = nextState.objectRotation.ToQuaternion();
            //prop.transform.GetChild(0).rotation = rightWrist.transform.rotation;
            */

            
            if(Vector3.Distance(nextState.rightHandPosition.ToVector3(), prop.transform.GetChild(0).position) <= Vector3.Distance(nextState.leftHandPosition.ToVector3(), prop.transform.GetChild(0).position))
            //if (Vector3.Distance(rightWrist.transform.position, prop.transform.GetChild(0).position) <= Vector3.Distance(leftWrist.transform.position, prop.transform.GetChild(0).position))
            {
                prop.transform.GetChild(0).position = rightWrist.transform.position;
                prop.transform.GetChild(0).rotation = nextState.objectRotation.ToQuaternion();
                //prop.transform.GetChild(0).rotation = rightWrist.transform.rotation;
            } else
            {
                prop.transform.GetChild(0).position = leftWrist.transform.position;
                prop.transform.GetChild(0).rotation = nextState.objectRotation.ToQuaternion();
                //prop.transform.GetChild(0).rotation = leftWrist.transform.rotation;
            }
            
        }
    }

    void HandleInput()
    {
        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleActionPlayback();
        }*/

        if (Input.GetKeyDown(KeyCode.T))
        {
            _ = Task.Run(() => executive.AddRandomGestureToInputQueue(propName));
        }
    }

    public void ToggleActionPlayback()
    {
        if (!isPlaying)
        {
            StartPlay();
        }
        else
        {
            StopPlay();
        }
    }

}
