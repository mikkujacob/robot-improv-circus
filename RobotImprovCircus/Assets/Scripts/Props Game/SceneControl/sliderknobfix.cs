﻿using NewtonVR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sliderknobfix : MonoBehaviour {

    Rigidbody mamaRollingThatRigidBody;

	// Use this for initialization
	void Start () {

        mamaRollingThatRigidBody = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void unfreezeZPos()
    {
        mamaRollingThatRigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY;
    }

    public void freezeZPos()
    {
        mamaRollingThatRigidBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
    }
}
