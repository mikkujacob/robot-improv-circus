﻿using UnityEngine;
using Moments;

[RequireComponent(typeof(Recorder)), AddComponentMenu("")]
public class GifRecording : MonoBehaviour
{
    Recorder m_Recorder;
    float m_Progress = 0f;
    string m_LastFile = "";
    bool m_IsSaving = false;
    bool playingGesture = false;
    public static int whichLine = 0;

    public static string dataset = "Dataset1";

    // Gets a filename : GifCapture-yyyyMMddHHmmssffff
    public static string GenerateFileName()
    {
        return "Surprise_GIF_" + (GifRecording.whichLine + 1).ToString();
    }

    void Start()
    {
        m_Recorder = GetComponent<Recorder>();
        //m_Recorder.Record();

        m_Recorder.SaveFolder = Application.streamingAssetsPath + "/GIFs/" + dataset + "/";
        m_Recorder.OnPreProcessingDone = OnProcessingDone;
        m_Recorder.OnFileSaveProgress = OnFileSaveProgress;
        m_Recorder.OnFileSaved = OnFileSaved;
    }

    void OnProcessingDone()
    {
        // All frames have been extracted and sent to a worker thread for compression !
        // The Recorder is ready to record again, you can call Record() here if you don't
        // want to wait for the file to be compresse and saved.
        // Pre-processing is done in the main thread, but frame compression & file saving
        // has its own thread, so you can save multiple gif at once.

        m_IsSaving = true;
    }

    void OnFileSaveProgress(int id, float percent)
    {
        // This callback is probably not thread safe so use it at your own risks.
        // Percent is in range [0;1] (0 being 0%, 1 being 100%).
        m_Progress = percent * 100f;
    }

    void OnFileSaved(int id, string filepath)
    {
        // Our file has successfully been compressed & written to disk !
        m_LastFile = filepath;
        m_IsSaving = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (playingGesture)
            {
                m_Recorder.SaveFolder = Application.streamingAssetsPath + "/GIFs/" + dataset + "/";
                // Compress & save the buffered frames to a gif file. We should check the State
                // of the Recorder before saving, but for the sake of this example we won't, so
                // you'll see a warning in the console if you try saving while the Recorder is
                // processing another gif.
                m_Recorder.Save();
                m_Progress = 0f;
                playingGesture = false;
                Debug.Log("STOP RECORDING");
            }
            else
            {
                m_Recorder.Record();
                playingGesture = true;
                Debug.Log("RECORDING GESTURE AS GIF");
            }
        }
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            whichLine--;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            whichLine++;
        }
    }
}
