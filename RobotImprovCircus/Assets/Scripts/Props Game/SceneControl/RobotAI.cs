﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using RootMotion.FinalIK;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class RobotAI : MonoBehaviour
{
    private Animator anim;
    private NavMeshAgent navMeshAgent;
    public GameObject propLocation;
    public GameObject agentLocation;
    public AIState aiState;
    private VRIK IK;

    private AgentPlayer agentPlayer;

    void Start()
    {
        /*
        agentPlayer = GameObject.Find("MImEGlobalObject").GetComponent<AgentPlayer>();

        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        //setNextWaypoint();
        aiState = AIState.Idle;

        IK = GetComponent<VRIK>();
        turnOnAnimation();


        //anim.SetBool("agentPlaying", false);
        */
    }

    // AI states to mirror animation states in the Animation controller
    public enum AIState
    {
        Forward, //walking forward
        Back,
        Grabbing,
        Idle
    };

    void Update()
    {
        anim.SetFloat("vely", navMeshAgent.velocity.magnitude / navMeshAgent.speed);
        //Debug.Log("REACHED PROP: " + anim.GetBool("reachedProp") + ";   AGENT PLAYING: " + anim.GetBool("agentPlaying") + ";   GRABBED PROP: " + anim.GetBool("grabbed"));

        switch (aiState)
        {
            case AIState.Idle:
                // Switch to Forward state if the idle agent has not reached the Prop
                if (anim.GetBool("reachedProp") == false && anim.GetBool("agentPlaying") == true && anim.GetBool("grabbed") == false && anim.GetBool("inPlace") == false)
                {
                    aiState = AIState.Forward;
                }

                //Switch to Grabbing state if the Idle agent has not picked up the prop
                if (anim.GetBool("reachedProp") == true && anim.GetBool("grabbed") == false && anim.GetBool("agentPlaying") == true && anim.GetBool("inPlace") == false)
                {
                    //setNextWaypoint(agentLocation);
                    aiState = AIState.Grabbing;
                }

                //Switch to Back state if agent has grabbed prop
                if (anim.GetBool("reachedProp") == true && anim.GetBool("grabbed") == true && anim.GetBool("agentPlaying") == true && anim.GetBool("inPlace") == false)
                {
                    //Add next frame location from summergif next gif
                    setNextWaypoint(agentLocation);
                    navMeshAgent.isStopped = false;
                    anim.SetFloat("vely", navMeshAgent.velocity.magnitude / navMeshAgent.speed);
                    aiState = AIState.Back;
                }
                if(anim.GetBool("reachedProp") == true && anim.GetBool("grabbed") == true && anim.GetBool("agentPlaying") == true && anim.GetBool("inPlace") == true)
                {
                    turnOffAnimation();
                }

                break;

            case AIState.Forward:
                IK.solver.plantFeet = false;
                // Switch to Idle state if the walking agent has reached the prop
                if (navMeshAgent.remainingDistance < 0.1f && !navMeshAgent.pathPending && anim.GetBool("agentPlaying") == true)
                {
                    //Debug.Log("reached!!!");
                    navMeshAgent.isStopped = true;
                    anim.SetFloat("vely", 0);
                    anim.SetBool("reachedProp", true);
                    aiState = AIState.Idle;
                }
                break;

            case AIState.Back:
                agentPlayer.CouplePropToHand();

                if (navMeshAgent.remainingDistance < 0.1f && !navMeshAgent.pathPending && anim.GetBool("agentPlaying") == true)
                {
                    //Debug.Log("reached!!!");
                    navMeshAgent.isStopped = true;
                    anim.SetFloat("vely", 0);
                    anim.SetBool("inPlace", true);
                    aiState = AIState.Idle;
                    turnOffAnimation();
                }
                break;

            case AIState.Grabbing:

                // Set the boolean named grab to true if the grabbing animation is played 
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Grab Low") && anim.GetBool("agentPlaying") == true)
                {
                    //anim.SetBool("grabbed", true);
                    //Debug.Log(anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
                    if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.83f)
                    {
                        //anim.StopPlayback();
                        anim.SetBool("grabbed", true);
                        //turnOffAnimation();
                        //setNextWaypoint(agentLocation);
                        aiState = AIState.Idle;
                    } else if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f)
                    {
                        agentPlayer.CouplePropToHand();
                    }
                }
                // Go back to the Idle state after playing the grab animation
                //else
                //aiState = AIState.Idle;
                //break;
                break;
            default:
                break;
        }
    }

    private void setNextWaypoint(GameObject waypoint)
    {
        navMeshAgent.SetDestination(waypoint.transform.position);
    }

    public void setupAnimation()
    {
        aiState = AIState.Idle;

        agentPlayer = GameObject.Find("MImEGlobalObject").GetComponent<AgentPlayer>();
        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        IK = GetComponent<VRIK>();

        turnOnAnimation();
    }

    private void turnOnAnimation()
    {
        navMeshAgent.enabled = true;
        anim.SetBool("agentPlaying", true);
        anim.SetBool("grabbed", false);
        anim.SetBool("reachedProp", false);
        anim.SetBool("inPlace", false);

        IK.solver.SetIKPositionWeight(0);
        IK.solver.plantFeet = false;
        IK.solver.spine.positionWeight = 0;
        IK.solver.spine.rotationWeight = 0;
        IK.solver.spine.pelvisPositionWeight = 0;
        IK.solver.spine.pelvisRotationWeight = 0;

        setNextWaypoint(propLocation);

        agentPlayer.agentSpawnProp();
    }

    private void turnOffAnimation()
    {
        IK.solver.SetIKPositionWeight(1);
        IK.solver.plantFeet = true;
        IK.solver.spine.positionWeight = 1;
        IK.solver.spine.rotationWeight = 1;
        IK.solver.spine.pelvisPositionWeight = 1;
        IK.solver.spine.pelvisRotationWeight = 1;
        navMeshAgent.enabled = false;

        anim.SetBool("agentPlaying", false);
        anim.SetBool("grabbed", false);
        anim.SetBool("reachedProp", false);
        anim.SetBool("inPlace", false);

        anim.enabled = false;
        aiState = AIState.Idle;
        agentPlayer.agentSpawnProp();
        agentPlayer.StartPlay();
        this.enabled = false;
    }
}
