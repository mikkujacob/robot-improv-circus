﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectStateDefinition
{
    /// <summary>
    /// The identifier.
    /// </summary>
	public string id;

	/// <summary>
	/// The linked.
	/// </summary>
	public bool linked;

	/// <summary>
	/// The links.
	/// </summary>
	public List<string> links;

	/// <summary>
	/// The animate.
	/// </summary>
    public bool animate;

	/// <summary>
	/// The contact.
	/// </summary>
	public bool contact;

	/// <summary>
	/// The contacts.
	/// </summary>
	public List<string> contacts;

	/// <summary>
	/// The open.
	/// </summary>
    public bool open;

	/// <summary>
	/// The containing.
	/// </summary>
	public bool containing;

	/// <summary>
	/// The contains.
	/// </summary>
	public List<string> contains;

	/// <summary>
	/// The contained.
	/// </summary>
	public bool contained;

	/// <summary>
	/// The containers.
	/// </summary>
	public List<string> containers;

	/// <summary>
	/// The location.
	/// </summary>
    public Vector3Definition location;

	/// <summary>
	/// The orientation.
	/// </summary>
	public Vector4Definition orientation;

	/// <summary>
	/// The above.
	/// </summary>
	public List<string> above;

	/// <summary>
	/// The below.
	/// </summary>
	public List<string> below;

	/// <summary>
	/// The behind.
	/// </summary>
	public List<string> behind;

	/// <summary>
	/// The ahead.
	/// </summary>
	public List<string> ahead;

	/// <summary>
	/// The right of.
	/// </summary>
	public List<string> rightOf;

	/// <summary>
	/// The left of.
	/// </summary>
	public List<string> leftOf;

	/// <summary>
	/// The near.
	/// </summary>
	public List<string> near;

	/// <summary>
	/// The far.
	/// </summary>
	public List<string> far;

	/// <summary>
	/// The changes.
	/// </summary>
	public List<TrackedChange> changes;

	/// <summary>
	/// The GlobalTrackerManager
	/// </summary>
	public GlobalTrackerManager globalTrackerManager;

	/// <summary>
	/// Initializes a new instance of the <see cref="ObjectStateDefinition"/> class.
	/// </summary>
    public ObjectStateDefinition()
    {
        id = System.Guid.NewGuid().ToString();

        linked = false;
		links = new List<string>();

		animate = false;

		contact = false;
		contacts = new List<string>();

		open = false;

		containing = false;
		contains = new List<string>();

		contained = false;
		containers = new List<string>();

		location = new Vector3Definition();
        orientation = new Vector4Definition();

		above = new List<string>();
        below = new List<string>();
        behind = new List<string>();
        ahead = new List<string>();
        rightOf = new List<string>();
        leftOf = new List<string>();

		near = new List<string>();
        far = new List<string>();

		changes = new List<TrackedChange>();
    }

	public string toString (string someString) {
		return someString;
	}

	public string toString (bool someBool) {
		return someBool + "";
	}

	public string toString (List<string> stringList) {
		string returnString = "";
		foreach (string someString in stringList) {

			returnString += globalTrackerManager.idToName[someString] + ", ";
		}
		return returnString;
	}

	public string toString (Vector3Definition vector) {
		return "x: " + vector.x + ", y: " + vector.y + ", z: " + vector.z;
	}

	public string toString (Vector4Definition quaternion) {
		return "x: " + quaternion.x + ", y: " + quaternion.y + ", z: " + quaternion.z + ", w: " + quaternion.w;
	}

	public string directToString (List<string> stringList) {
		string returnString = "";
		foreach (string someString in stringList) {
			returnString += someString + ", ";
		}
		return returnString;
	}




}
