﻿using System;
using UnityEngine;

[System.Serializable]
public class GestureCoordinate : ISpatialData
{
    public Guid Guid;

    public Envelope Envelope { get; set; }

    public string pretendActionLabel;
    public string pretendObjectLabel;
    public string propName;

    public GestureCoordinate() : this(new Envelope()) { }

    public GestureCoordinate(Envelope envelope) : this(envelope, Guid.NewGuid()) {}

    public GestureCoordinate(Envelope envelope, Guid guid) : this(envelope, guid, "", "", "") {}

    public GestureCoordinate(Envelope envelope, Guid guid, string action_label, string prop_label, string prop_Name)
    {
        this.Envelope = envelope;
        this.Guid = guid;
        this.pretendActionLabel = action_label;
        this.pretendObjectLabel = prop_label;
        this.propName = prop_Name;
    }

    public GestureCoordinate DeepCopy()
    {
        Envelope cloneEnvelope;
        if(this.Envelope != null)
        {
            cloneEnvelope = new Envelope(this.Envelope.MinX, this.Envelope.MinY, this.Envelope.MaxX, this.Envelope.MaxY);
        }
        else
        {
            cloneEnvelope = null;
        }

        Guid cloneGuid;
        cloneGuid = new Guid(this.Guid.ToByteArray());

        GestureCoordinate clone = new GestureCoordinate(cloneEnvelope, cloneGuid, String.Copy(this.pretendActionLabel), String.Copy(this.pretendObjectLabel), String.Copy(this.propName));

        return clone;
    }

    public string Serialize()
    {
        return JsonUtility.ToJson(this);
    }

    public object Deserialize(string stringObj)
    {
        return JsonUtility.FromJson<PlayerStateDefinition>(stringObj);
    }
}
