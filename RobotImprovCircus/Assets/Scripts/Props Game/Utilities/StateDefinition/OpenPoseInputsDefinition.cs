﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OpenPoseInputsDefinition
{
    public float version;
    public OpenPosePersonDefinition[] people;

    public OpenPoseInputsDefinition()
    {
        version = 0.0f;
        people = new OpenPosePersonDefinition[1];
    }

    public OpenPoseInputsDefinition(float version, OpenPosePersonDefinition[] people)
    {
        this.version = version;
        this.people = new OpenPosePersonDefinition[people.Length];
        Array.Copy(people, this.people, people.Length);
    }

    public override string ToString()
    {
        return "Version: " + version.ToString() + "\n" +
            "People: " + OpenPosePersonDefinition.Array1DToString(people, ",\n", true);
    }
}
