﻿using System.Collections;
using System;
using LiteDB;

[System.Serializable]
public class SerializedGestureDefinition
{
    [BsonId]
    public Guid id;
    public string serializedGesture { get; set; }

    public SerializedGestureDefinition()
    {
        id = Guid.NewGuid();
        serializedGesture = "";
    }

    public SerializedGestureDefinition(Guid id, string gestureString)
    {
        this.id = id;
        this.serializedGesture = gestureString;
    }

    public SerializedGestureDefinition(GestureDefinition gesture)
    {
        FromGestureDefinition(gesture);
    }

    public void FromGestureDefinition(GestureDefinition gesture)
    {
        id = new Guid(gesture.gestureID);
        serializedGesture = gesture.Serialize();
    }

    public GestureDefinition ToGestureDefinition()
    {
        GestureDefinition gesture = new GestureDefinition();
        return gesture.Deserialize(serializedGesture);
    }
}
