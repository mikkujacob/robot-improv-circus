﻿using UnityEngine;

[System.Serializable]
public class TrackedChange
{
	public string name;
	public string oldValue;
	public string newValue;

	public TrackedChange ()
	{
		this.name = "";
		this.oldValue = "";
		this.newValue = "";
	}

	public TrackedChange(string name, string oldValue, string newValue)
	{
		this.name = name;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public string ToString()
	{
		return name + ": " + oldValue + " -> " + newValue;
	}
}
