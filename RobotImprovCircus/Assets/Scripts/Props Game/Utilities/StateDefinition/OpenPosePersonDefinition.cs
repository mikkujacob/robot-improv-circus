﻿using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class OpenPosePersonDefinition
{
    public enum COCO_OpenPose_Keypoints
    {
        Nose,
        Neck,
        RightShoulder,
        RightElbow,
        RightWrist,
        LeftShoulder,
        LeftElbow,
        LeftWrist,
        RightHip,
        RightKnee,
        RvAnkle,
        LeftHip,
        LeftKnee,
        LeftAnkle,
        RightEye,
        LeftEye,
        RightEar,
        LeftEar,
        Background
    };

    public float[] pose_keypoints_2d;
    public float[] face_keypoints_2d;
    public float[] hand_left_keypoints_2d;
    public float[] hand_right_keypoints_2d;
    public float[] pose_keypoints_3d;
    public float[] face_keypoints_3d;
    public float[] hand_left_keypoints_3d;
    public float[] hand_right_keypoints_3d;

    public OpenPosePersonDefinition()
    {
        pose_keypoints_2d = new float[18];
        face_keypoints_2d = new float[18];
        hand_left_keypoints_2d = new float[18];
        hand_right_keypoints_2d = new float[18];
        pose_keypoints_3d = new float[18];
        face_keypoints_3d = new float[18];
        hand_left_keypoints_3d = new float[18];
        hand_right_keypoints_3d = new float[18];
    }

    public OpenPosePersonDefinition(float[] pose2d, float[] face2d, float[] handleft2d, float[] handright2d, float[] pose3d, float[] face3d, float[] handleft3d, float[] handright3d)
    {
        pose_keypoints_2d = new float[18];
        if(pose2d != null)
        {
            Array.Copy(pose2d, pose_keypoints_2d, pose2d.Length);
        }
        face_keypoints_2d = new float[18];
        if(face2d != null)
        {
            Array.Copy(face2d, face_keypoints_2d, face2d.Length);
        }
        hand_left_keypoints_2d = new float[18];
        if(handleft2d != null)
        {
            Array.Copy(handleft2d, hand_left_keypoints_2d, handleft2d.Length);
        }
        hand_right_keypoints_2d = new float[18];
        if(handright2d != null)
        {
            Array.Copy(handright2d, hand_right_keypoints_2d, handright2d.Length);
        }
        pose_keypoints_3d = new float[18];
        if(pose3d != null)
        {
            Array.Copy(pose3d, pose_keypoints_3d, pose3d.Length);
        }
        face_keypoints_3d = new float[18];
        if(face3d != null)
        {
            Array.Copy(face3d, face_keypoints_3d, face3d.Length);
        }
        hand_left_keypoints_3d = new float[18];
        if(handleft3d != null)
        {
            Array.Copy(handleft3d, hand_left_keypoints_3d, handleft3d.Length);
        }
        hand_right_keypoints_3d = new float[18];
        if(handright3d != null)
        {
            Array.Copy(handright3d, hand_right_keypoints_3d, handright3d.Length);
        }
    }

    public override string ToString()
    {
        return "Pose 2D: " + Array1DToString(pose_keypoints_2d, ", ", false) + "\n" + 
            "Face 2D: " + Array1DToString(face_keypoints_2d, ", ", false) + "\n" +
            "Hand left 2D: " + Array1DToString(hand_left_keypoints_2d, ", ", false) + "\n" +
            "Hand Right 2D: " + Array1DToString(hand_right_keypoints_2d, ", ", false) + "\n" +
            "Pose 3D: " + Array1DToString(pose_keypoints_3d, ", ", false) + "\n" +
            "Face 3D: " + Array1DToString(face_keypoints_3d, ", ", false) + "\n" +
            "Hand left 3D: " + Array1DToString(hand_left_keypoints_3d, ", ", false) + "\n" +
            "Hand Right 3D: " + Array1DToString(hand_right_keypoints_3d, ", ", false);
    }

    public static string Array1DToString<T>(T[] array, string separator, Boolean hasCurlyBraces)
    {
        if(array == null || array.Length == 0)
        {
            return "[]";
        }

        string result = "[";

        foreach(T element in array)
        {
            result += (hasCurlyBraces ? "{" : "") + element.ToString() + (hasCurlyBraces ? "}" : "") + separator;
        }

        return result.Substring(0, result.Length - separator.Length) + "]";
    }
}
