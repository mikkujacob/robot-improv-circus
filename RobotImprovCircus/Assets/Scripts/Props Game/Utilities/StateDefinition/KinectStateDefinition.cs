﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class KinectStateDefinition
{
    public List<Vector3Definition> jointPositions;
    public List<Vector4Definition> jointAngles;
    public JIDX anchorJoint;

//    public static JPIDX[] ALL_JPIDX =
//    {
//        JPIDX.HEAD, JPIDX.NECK, 
//        JPIDX.LEFT_SHOULDER, JPIDX.LEFT_ELBOW, JPIDX.LEFT_HAND, 
//        JPIDX.RIGHT_SHOULDER, JPIDX.RIGHT_ELBOW, JPIDX.RIGHT_HAND, 
//        JPIDX.TORSO, 
//        JPIDX.LEFT_HIP, JPIDX.LEFT_KNEE, JPIDX.LEFT_FOOT, 
//        JPIDX.RIGHT_HIP, JPIDX.RIGHT_KNEE, JPIDX.RIGHT_FOOT
//    };

    public KinectStateDefinition()
    {
        jointPositions = new List<Vector3Definition>();
        foreach(JIDX jidx in System.Enum.GetValues(typeof(JIDX)))
        {
            jointPositions.Add(new Vector3Definition());
        }
        jointAngles = new List<Vector4Definition>();
        foreach(JIDX jidx in System.Enum.GetValues(typeof(JIDX)))
        {
            jointAngles.Add(new Vector4Definition());
        }
        anchorJoint = JIDX.SpineBase;
    }

    public void ResetState()
    {
        jointPositions.Clear();
        jointAngles.Clear();
    }
}

public enum JIDX
{
    SpineBase, //Base of the spine
    SpineMid, //Middle of the spine
    
    Neck, //Neck
    Head, //Head
    
    ShoulderLeft, //Left shoulder
    ElbowLeft, //Left elbow
    WristLeft, //Left wrist
    HandLeft, //Left hand
    
    ShoulderRight, //Right shoulder
    ElbowRight, //Right elbow
    WristRight, //Right wrist;
    HandRight, //Right hand
    
    HipLeft, //Left hip
    KneeLeft, //Left knee
    AnkleLeft, //Left ankle
    FootLeft, //Left foot
    
    HipRight, //Right hip
    KneeRight, //Right knee
    AnkleRight, //Right ankle
    FootRight, //Right foot
    
    SpineShoulder, //Spine at the shoulder
    
    HandTipLeft, //Tip of the left hand
    ThumbLeft, //Left thumb
    
    HandTipRight, //Tip of the right hand
    ThumbRight //Right thumb
}

//public enum JPIDX
//{
//    HEAD, NECK, 
//    LEFT_COLLAR, LEFT_SHOULDER, LEFT_ELBOW, LEFT_HAND, LEFT_FINGERTIP, 
//    RIGHT_COLLAR, RIGHT_SHOULDER, RIGHT_ELBOW, RIGHT_HAND, RIGHT_FINGERTIP, 
//    TORSO, WAIST, 
//    LEFT_HIP, LEFT_KNEE, LEFT_ANKLE, LEFT_FOOT, 
//    RIGHT_HIP, RIGHT_KNEE, RIGHT_ANKLE, RIGHT_FOOT
//}

////TODO: Correct order of angles in enum to root structure of skeleton
//public enum JAIDX
//{
//    HEAD, NECK, 
//    LEFT_COLLAR, LEFT_SHOULDER, LEFT_ELBOW, LEFT_HAND, LEFT_FINGERTIP, 
//    RIGHT_COLLAR, RIGHT_SHOULDER, RIGHT_ELBOW, RIGHT_HAND, RIGHT_FINGERTIP, 
//    TORSO, WAIST, 
//    LEFT_HIP, LEFT_KNEE, LEFT_ANKLE, LEFT_FOOT, 
//    RIGHT_HIP, RIGHT_KNEE, RIGHT_ANKLE, RIGHT_FOOT
//}