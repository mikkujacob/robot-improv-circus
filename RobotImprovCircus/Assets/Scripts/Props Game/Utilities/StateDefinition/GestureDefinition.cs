﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using LiteDB;
using Newtonsoft.Json;

[System.Serializable]
public class GestureDefinition
{
    public static float MinimumGestureLength = 3.0f;

    [BsonId]
    public string gestureID;

    public string timestamp;

    //public string objectName;
    //public string objectSize;
    public Vector3Definition objectStartingPosition;
    public Vector4Definition objectStartingRotation;

    public string pretendActionName;
    public string pretendObjectName;

    public float gestureDuration;
    public bool IsGestureTooShort;
    public int skipN;
    public DataEnum dataType;

    public PropDefinition propObject;
    public GestureCoordinate coordinateIndex;

    //public ListGenericDefinition<PlayerStateDefinition> playerStates;
    public List<PlayerStateDefinition> playerStates;

    public GestureDefinition() : this(DataEnum.Data27000) {}
    //public GestureDefinition() : this(Guid.NewGuid().ToString(), System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff"), "", "", new Vector3Definition(), new Vector4Definition(), new ListGenericDefinition<PlayerStateDefinition>()) { }
    public GestureDefinition (DataEnum dataE = DataEnum.Data27000) : this(Guid.NewGuid().ToString(), System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-fff"), "", "", new Vector3Definition(), new Vector4Definition(), new ListGenericDefinition<PlayerStateDefinition>(), dataE, " "," ") {}

    public GestureDefinition(GestureDefinition gesture, DataEnum dataE = DataEnum.Data27000) : this(gesture.gestureID, gesture.timestamp, gesture.propObject.name, gesture.propObject.size, gesture.objectStartingPosition, gesture.objectStartingRotation, gesture.playerStates, dataE, gesture.pretendActionName, gesture.pretendObjectName) { }

    public GestureDefinition(string id, string timeString, string objName, string objSize, Vector3Definition objectPosition, Vector4Definition objectRotation, List<PlayerStateDefinition> states, DataEnum dataE = DataEnum.Data27000, string pActionName = " ", string pObjectName = " ")
    {
        gestureID = id;
        timestamp = timeString;
        //objectName = objName;
        //objectSize = objSize;
        objectStartingPosition = objectPosition;
        objectStartingRotation = objectRotation;
        playerStates = new ListGenericDefinition<PlayerStateDefinition>(states);
        coordinateIndex = new GestureCoordinate();
        propObject = new PropDefinition(objName, objSize);
        //propObject.size = objSize;
        IsGestureTooShort = false;
        dataType = dataE;
        skipN = 0;
        pretendActionName = pActionName;
        pretendObjectName = pObjectName;
    }

    public GestureDefinition(float[,] jointData, string propName, bool reMap, int skipN, DataEnum dataType) : this(dataType)
    {
        LoadStates(jointData, propName, reMap, skipN);
    }

    public void AddState(PlayerStateDefinition state)
    {
        playerStates.Add(state);
    }

    /// <summary>
    /// Loads the frames into the gesture definition object
    /// </summary>
    /// <param name="jointData"></param>
    /// <param name="reMap"></param>
    /// <param name="skipN"></param>
    public void LoadStates(float[,] jointData, string propName, bool reMap, int skipN)
    {
        propObject.name = propName;
        this.skipN = skipN;
        //JointData 27000 is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
        //JointData 16000 is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, PropPosition, PropRotation
        try
        {
            int frameCount = 0;
            int skipCount = 0;
            PlayerStateDefinition state;
            int dataLength = (int) dataType;

            int increment = 1;
            float timeIncrementSecondsFloat = 0.0111111f;
            long timeincrementTicksLong = 111111L;
            switch(dataType)
            {
                case DataEnum.Data27000:
                    increment = 30;
                    timeIncrementSecondsFloat = 0.0111111f;
                    timeincrementTicksLong = 111111L;
                    break;

                case DataEnum.Data16000:
                    increment = 35;
                    timeIncrementSecondsFloat = 0.0222222f;
                    timeincrementTicksLong = 222222L;
                    break;
            }

            if(reMap)
            {
                jointData = Remap2DArrayValues(jointData, 0, 1, -2, 2);
            }

            int endFrameIndex = FindZeroIndex(jointData, dataType, true);

            DateTime startingTimestamp = DateTime.ParseExact(this.timestamp, "yyyy-MM-dd-HH-mm-ss-fff", System.Globalization.CultureInfo.InvariantCulture);
            //UnityEngine.Debug.Log("Start Timestamp: " + startingTimestamp.ToString("yyyy-MM-dd-HH-mm-ss-fff"));

            for(int index = 0; index < endFrameIndex; index += increment)
            {
                if (skipN > 0)
                {
                    if (skipCount != 0)
                    {
                        skipCount = (skipCount + 1) % (skipN + 1);
                        frameCount++;
                        continue;
                    }
                    else
                    {
                        skipCount++;
                    }
                }

                state = new PlayerStateDefinition();
                //UnityEngine.Debug.Log("Current Frame Index: " + frameCount);
                //UnityEngine.Debug.Log("Time Elapsed (Ticks): " + frameCount * timeincrementLong);
                DateTime currentTimestamp = startingTimestamp.Add(TimeSpan.FromTicks(frameCount * timeincrementTicksLong));
                state.timestamp = currentTimestamp.ToString("yyyy-MM-dd-HH-mm-ss-fff");
                //UnityEngine.Debug.Log("Current Timestamp: " + currentTimestamp.ToString("yyyy-MM-dd-HH-mm-ss-fff"));

                state.headPosition = new Vector3Definition(jointData[0, index + 0], jointData[0, index + 1], jointData[0, index + 2]);
                state.headRotation = new Vector4Definition(jointData[0, index + 3], jointData[0, index + 4], jointData[0, index + 5], jointData[0, index + 6]);
                state.leftHandPosition = new Vector3Definition(jointData[0, index + 7], jointData[0, index + 8], jointData[0, index + 9]);
                state.leftHandRotation = new Vector4Definition(jointData[0, index + 10], jointData[0, index + 11], jointData[0, index + 12], jointData[0, index + 13]);
                state.rightHandPosition = new Vector3Definition(jointData[0, index + 14], jointData[0, index + 15], jointData[0, index + 16]);
                state.rightHandRotation = new Vector4Definition(jointData[0, index + 17], jointData[0, index + 18], jointData[0, index + 19], jointData[0, index + 20]);
                state.skeletalPositions[1] = new Vector3Definition(jointData[0, index + 21], jointData[0, index + 22], jointData[0, index + 23]);
                state.skeletalRotations[1] = new Vector4Definition(jointData[0, index + 24], jointData[0, index + 25], jointData[0, index + 26], jointData[0, index + 27]);

                switch (dataType)
                {
                    case DataEnum.Data27000:
                        state.leftControllerGripButtonDown = jointData[0, index + 28] > 0.5;
                        state.leftControllerGripButtonUp = !state.leftControllerGripButtonDown;
                        state.rightControllerGripButtonDown = jointData[0, index + 29] > 0.5;
                        state.rightControllerGripButtonUp = !state.rightControllerGripButtonDown;
                        if (state.rightControllerGripButtonDown && state.leftControllerGripButtonUp)
                        {
                            state.objectPosition = new Vector3Definition(state.rightHandPosition.x, state.rightHandPosition.y, state.rightHandPosition.z);
                            state.objectRotation = new Vector4Definition(state.rightHandRotation.x, state.rightHandRotation.y, state.rightHandRotation.z, state.rightHandRotation.w);
                        }
                        else if (state.rightControllerGripButtonUp && state.leftControllerGripButtonDown)
                        {
                            state.objectPosition = new Vector3Definition(state.leftHandPosition.x, state.leftHandPosition.y, state.leftHandPosition.z);
                            state.objectRotation = new Vector4Definition(state.leftHandRotation.x, state.leftHandRotation.y, state.leftHandRotation.z, state.leftHandRotation.w);
                        }
                        break;

                    case DataEnum.Data16000:
                        state.objectPosition = new Vector3Definition(jointData[0, index + 28], jointData[0, index + 29], jointData[0, index + 30]);
                        state.objectRotation = new Vector4Definition(jointData[0, index + 31], jointData[0, index + 32], jointData[0, index + 33], jointData[0, index + 34]);
                        break;
                }

                AddState(state);
                frameCount++;
            }

            gestureDuration = frameCount * timeIncrementSecondsFloat;
        }
        catch (Exception e)
        {
            throw e;
        }

        SetDuration();

        //NOTE: Intentionally commented in order to do length checks INSIDE CARNIVAL ONLY.
        //if(gestureDuration < MinimumGestureLength)
        //{
        //    IsGestureTooShort = true;
        //}
    }

    public void EMASmootheGesture(float alpha)
    {
        for(int i = 1; i < playerStates.Count; i++)
        {
            playerStates[i] = PlayerStateDefinition.EMAInterpolateState(playerStates[i], playerStates[i-1], alpha);
        }
    }

    public string Serialize()
	{
        //		return JsonUtility.ToJson (this);

        //Original version used till now.
        //string serialized = gestureID + "$" + timestamp + "$" + propObject.name + "$" + propObject.size + "$" + JsonUtility.ToJson(objectStartingPosition) + "$" + JsonUtility.ToJson(objectStartingRotation) + "$" + pretendActionName + "$" + pretendObjectName + "$";
        //string serialized = gestureID + "$" + timestamp + "$" + propObject.Serialize() + "$" + JsonUtility.ToJson(objectStartingPosition) + "$" + JsonUtility.ToJson(objectStartingRotation) + "$" + pretendActionName + "$" + pretendObjectName + "$" + coordinateIndex.Serialize() + "$";

        //foreach (PlayerStateDefinition state in playerStates)
        //{
        //    serialized += JsonUtility.ToJson(state) + "^";
        //}

        //serialized = serialized.Substring(0, serialized.Length - 1);

        string serialized = JsonConvert.SerializeObject(this);

        //Debug.Log(serialized);

        return serialized;
    }

    //TODO: Add Json deserialization
    public GestureDefinition Deserialize(string stringObj)
    {
        // return JsonUtility.FromJson<GestureDefinition> (stringObj);
		//string[] data = stringObj.Split ('$');
		//string[] states = data [8].Split ('^');
		//ListGenericDefinition<PlayerStateDefinition> stateDefs = new ListGenericDefinition<PlayerStateDefinition> ();
		//foreach (string state in states)
		//{
		//	stateDefs.Add (JsonUtility.FromJson<PlayerStateDefinition> (state));
		//}

        //TODO: TEST AND REVERT COMMENTED STATE IF JSON.NET DOESN'T WORK!
        //if(data.Length > 6)
        //{
        //	return new GestureDefinition (data [0], data [1], data [2], data [3], JsonUtility.FromJson<Vector3Definition> (data [4]), JsonUtility.FromJson<Vector4Definition> (data [5]), stateDefs, pActionName: data[6], pObjectName: data[7]);
        //}
        //else
        //{
        //	return new GestureDefinition(data[0], data[1], data[2], data[3], JsonUtility.FromJson<Vector3Definition>(data[4]), JsonUtility.FromJson<Vector4Definition>(data[5]), stateDefs);
        //}

        return JsonConvert.DeserializeObject<GestureDefinition>(stringObj);
	}

    /// <summary>
    /// Method that returns an index for when the gesture ends and zero padding begins.
    /// </summary>
    /// <param name="jointData"></param>
    /// <param name="epsilon"></param>
    /// <param name="dataType"></param>
    /// <returns></returns>
    private int FindZeroIndex(float[,] jointData, DataEnum dataType, bool useMedianFilter)
    {
        int increment = 0;
        int windowSize = 0;
        int mandatoryPadding = 0;

        switch(dataType)
        {
            case DataEnum.Data27000:
                increment = 30;
                windowSize = 19;
                break;

            case DataEnum.Data16000:
                increment = 35;
                windowSize = 9;
                mandatoryPadding = 250;
                break;
        }
        int dataLength = jointData.GetLength(1) - mandatoryPadding;
        float[] medianJointData = new float[dataLength];

        for(int index = 0; index < increment * (windowSize - 1) / 2; index ++)
        {
            medianJointData[index] = jointData[0, index];
        }
        for(int index = increment * (windowSize - 1) / 2; index < dataLength - increment * (windowSize - 1) / 2; index ++)
        {
            List<float> windowItems = new List<float>();
            for(int windowIndex = 0; windowIndex < windowSize; windowIndex++)
            {
                int frameIndex = (windowIndex * 2 + 1 - windowSize) / 2 * increment + index;
                windowItems.Add(jointData[0, frameIndex]);
            }
            medianJointData[index] = windowItems.Median<float>();
        }
        for(int index = dataLength - increment * (windowSize - 1) / 2; index < dataLength; index ++)
        {
            medianJointData[index] = jointData[0, index];
        }

        float maxMedianDerivative = 0;
        int maxMedianIndex = 1; //Pick item BEFORE the largest slope to discourage having any balled up poses.
        for(int index = 0; index < dataLength - increment; index += increment)
        {
            float[] derivativeFrame = new float[increment];
            for(int frameIndex = 0; frameIndex < increment; frameIndex++)
            {
                if(useMedianFilter)
                {
                    derivativeFrame[frameIndex] = medianJointData[index + frameIndex] - medianJointData[index + frameIndex + increment];
                }
                else
                {
                    derivativeFrame[frameIndex] = jointData[0, index + frameIndex] - jointData[0, index + frameIndex + increment];
                }
            }

            float median = derivativeFrame.Median<float>();
            if(Math.Abs(median) > Math.Abs(maxMedianDerivative))
            {
                maxMedianDerivative = median;
                maxMedianIndex = index;
            }
        }
        // Debug.Log("End Frame Index is " + maxMedianIndex);

        return maxMedianIndex;
    }

    /// <summary>
    /// Checks to see if the joint data is near zero
    /// </summary>
    /// <param name="jointData">The gesture's joint data</param>
    /// <param name="index">The index of jointData</param>
    /// <param name="epsilon">Values below this amount will round to zero.</param>
    /// <returns>True if all the joint data is close to zero, otherwise false.</returns>
    private bool IsZeros(float[,] jointData, int index, float epsilon)
    {
        //JointData  is formatted as: HeadPosition, HeadRotation, LeftHandPosition, LeftHandRotation, RightHandPosition, RightHandRotation, PelvisPosition, PelvisRotation, LeftControllerGripButton, RightControllerGripButton
        if (jointData[0, index + 0] < epsilon &&      //HEAD POSITION X,Z
             jointData[0, index + 7] < epsilon &&     //LEFT HAND POSITION X
           jointData[0, index + 21] < epsilon)       //PELVIS POSITION X
        {
            return true;
        }
        ////If all joints are below 0 on Y axis
        //if (jointData[0, index + 1] < 0 &&
        //    jointData[0, index + 8] < 0 &&
        //    jointData[0, index + 15] < 0 &&
        //    jointData[0, index + 22] < 0)
        //{
        //    return true;
        //}
        return false;
    }

    /// <summary>
    /// Remap an entire 2D array at once into a new range.
    /// </summary>
    /// <param name="oldArray">Original 2D array</param>
    /// <param name="oldMin">Original minimum</param>
    /// <param name="oldMax">Original maximum</param>
    /// <param name="newMin">New minimum</param>
    /// <param name="newMax">New maximum</param>
    /// <returns>New 2D array with values remapped to new range</returns>
    private float[,] Remap2DArrayValues(float[,] oldArray, float oldMin, float oldMax, float newMin, float newMax)
    {
        float[,] newArray = new float[oldArray.GetLength(0), oldArray.GetLength(1)];

        for(int i = 0; i < oldArray.GetLength(0); i++)
        {
            for(int j = 0; j < oldArray.GetLength(1); j++)
            {
                newArray[i, j] = RemapValue(oldArray[i, j], oldMin, oldMax, newMin, newMax);
            }
        }

        return newArray;
    }

    private float RemapValue(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (newMax - newMin) * (oldValue - oldMin) / (oldMax - oldMin) + newMin;
    }

    public List<PlayerStateDefinition> GetPlayerStateDefinitions()
    {
        return playerStates;
    }

    public void SetPropDetails(string name, string size)
    {
        this.SetName(name);
        //objectSize = size;
        propObject.size = size;
    }

    public void SetName(string name)
    {
        //objectName = name;
        propObject.SetName(name);
    }

    public void SetDuration()
    {
        if(playerStates != null && playerStates.Count > 0)
        {
            DateTime lastTimeStamp = StringToDateTime(playerStates[playerStates.Count - 1].timestamp);
            DateTime firstTimeStamp = StringToDateTime(timestamp);
            TimeSpan duration = lastTimeStamp.Subtract(firstTimeStamp);
            gestureDuration = (float)(duration.TotalSeconds);
            // Debug.Log("Gesture Duration: " + gestureDuration);
            if(gestureDuration <= MinimumGestureLength)
            {
                IsGestureTooShort = true;
            }
            // Debug.Log("Is gesture too short? " + IsGestureTooShort);
        }
        else
        {
            // Debug.Log("Gesture with no player states");
            gestureDuration = 0;
            IsGestureTooShort = true;
        }
    }

    public GestureDefinition DeepCopy()
    {
        try
        {
            GestureDefinition clone = (GestureDefinition)MemberwiseClone();

            string temp = String.Copy(this.gestureID);

            clone.gestureID = String.Copy(this.gestureID);
            clone.timestamp = String.Copy(this.timestamp);

            //clone.objectName = String.Copy(this.objectName);
            //clone.objectSize = String.Copy(this.objectSize);
            clone.objectStartingPosition = this.objectStartingPosition.DeepCopy();
            clone.objectStartingRotation = this.objectStartingRotation.DeepCopy();

            clone.propObject = new PropDefinition(this.propObject.name, this.propObject.size, this.propObject.affordance);

            if (this.coordinateIndex != null)
            {
                clone.coordinateIndex = this.coordinateIndex.DeepCopy();
            }

            clone.playerStates = new ListGenericDefinition<PlayerStateDefinition>(this.playerStates.ConvertAll(playerState => playerState.DeepClone()));

            return clone;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR DeepCopy: " + e.Message);

            return new GestureDefinition(Executive.dataType);
        }
    }

    /// <summary>
    /// Converts a string representation of a date into a DateTime object.
    /// The format of the string must match "yyyy-MM-dd-HH-mm-ss-fff"
    /// </summary>
    /// <param name="timestamp">The string with the proper date/time format</param>
    /// <returns>A DateTime object form of the string</returns>
    private DateTime StringToDateTime(string timestamp)
    {
        string format = "yyyy-MM-dd-HH-mm-ss-fff";
        DateTime time = DateTime.ParseExact(timestamp, format, System.Globalization.CultureInfo.InvariantCulture);
        return time;
    }
}
