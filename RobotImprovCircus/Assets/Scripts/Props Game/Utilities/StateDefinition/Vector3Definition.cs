﻿using UnityEngine;

[System.Serializable]
public class Vector3Definition
{
    public float x;
    public float y;
    public float z;

    public Vector3Definition() { }

    public Vector3Definition(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3Definition(Vector3 location)
    {
        this.x = location.x;
        this.y = location.y;
        this.z = location.z;
    }

	public Vector3 ToVector3()
	{
		return new Vector3 (x, y, z);
	}

    public Vector3Definition DeepCopy()
    {
        return (Vector3Definition) this.MemberwiseClone();
    }

    public static Vector3Definition EMASmoothe(Vector3Definition vectort, Vector3Definition vectortminus1, float alpha)
    {
        Vector3Definition newVector = new Vector3Definition();

        newVector.x = alpha * vectort.x + (1 - alpha) * vectortminus1.x;
        newVector.y = alpha * vectort.y + (1 - alpha) * vectortminus1.y;
        newVector.z = alpha * vectort.z + (1 - alpha) * vectortminus1.z;

        return newVector;
    }
}
