﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

public static class Vector2Extension
{
    public static Vector2 Rotate(Vector2 v, float degrees)
    {
        float radian = ToRadians(degrees);

        float sin = (float)Math.Sin(radian);
        float cos = (float)Math.Cos(radian);

        float x = (cos * v.X) - (sin * v.Y);
        float y = (sin * v.X) + (cos * v.Y);

        return new Vector2(x, y);
    }

    private static float ToRadians(float degrees)
    {
        return (float) (degrees / 180 * Math.PI);
    }
}
