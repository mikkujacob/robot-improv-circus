﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ModelInterface : MonoBehaviour
{
    public TextAsset GraphAsset;
    protected FrozenGraphLoader Model = new FrozenGraphLoader();
    protected int nInputs;

    abstract protected void InitModel();

    public FrozenGraphLoader GetModel()
    {
        return Model;
    }
}
