﻿using LiteDB;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class LoadGestures : MonoBehaviour
{
    private string streamingAssetsPath;
    public static string _strConnection = "Filename=RIC.litedb4; Mode=Shared; upgrade=true";
    private RTree<GestureCoordinate> rTree;
    private TemporalMemory tMemory;
    private FrozenGraphLoader model;
    private RTree<ReducedGesture> GestureDRTree;
    private RTree<ReducedWord> ActionDRTree;
    private RTree<ReducedWord> ObjectDRTree;
    private GesturalDimRed GestureDR;
    private SemanticDimRed SemanticDR;
    public int GesturesLoaded { get; private set; } = 0;

    private static Dictionary<string, float[]> WordVectorLookup = new Dictionary<string, float[]>();
    private string WordVectorFileName;

    float[,] param;
    float[] affordance;
    List<float> vals;

    private SummerGifController summerGifController;
    private bool calculatingScores;

    public void Start()
    {
        streamingAssetsPath = Application.streamingAssetsPath;
        calculatingScores = true;
        if (calculatingScores)
        {
            summerGifController = gameObject.GetComponent<SummerGifController>();
        }
    }

    public void InitiateLoader(RTree<GestureCoordinate> rT, TemporalMemory tM, FrozenGraphLoader m, RTree<ReducedGesture> gDRT, RTree<ReducedWord> aDRT, RTree<ReducedWord> oDRT, GesturalDimRed gDR, SemanticDimRed sDR)
    {
        rTree = rT;
        tMemory = tM;
        model = m;
        GestureDRTree = gDRT;
        ActionDRTree = aDRT;
        ObjectDRTree = oDRT;
        GestureDR = gDR;
        SemanticDR = sDR;
    }

    public void LoadGesturesFromDisk()
    {
        LoadWordVectorDict(); // TODO: add to liteDB
        using (var db = new LiteDatabase(_strConnection))
        {
            //Load gestures from LitDB
            LiteCollection<SerializedGestureDefinition> gestureStrings = db.GetCollection<SerializedGestureDefinition>("gestures");

            //If no gesture in LiteDB
            if(gestureStrings.Count() == 0)
            {
                // Look in StreamingAssets for
                if(Directory.Exists(streamingAssetsPath))
                {
                    // timeseries .timeseries file
                    // affordance .labels file
                    // pretend action .labels file
                    // pretend object .labels file
                    if(Directory.GetFiles(streamingAssetsPath, "*.labels").Length >= 3 && Directory.GetFiles(streamingAssetsPath, "*.timeseries").Length >= 1)
                    {
                        // Load gestures from timeseries and labels file
                        StreamReader timeseriesReader = null;
                        StreamReader labelsReader = null;
                        StreamReader pretendObjectReader = null;
                        StreamReader pretendActionReader = null;

                        if(Executive.dataType == DataEnum.Data27000)
                        {
                            timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-27000.timeseries");
                            labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
                            pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
                            pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");
                        }
                        else if(Executive.dataType == DataEnum.Data16000)
                        {
                            
                            timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");
                            labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
                            pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
                            pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");
                            
                            
                            /*
                            timeseriesReader = new StreamReader(streamingAssetsPath + "/SurpriseTimeSeries.timeseries");
                            labelsReader = new StreamReader(streamingAssetsPath + "/SurpriseAffordanceLabels.labels");
                            pretendObjectReader = new StreamReader(streamingAssetsPath + "/SurprisePretendObjectLabels.labels");
                            pretendActionReader = new StreamReader(streamingAssetsPath + "/SurprisePretendActionLabels.labels");
                            */
                        }

                        int filesloaded = 0; //TODO: REMOVE AFTER TESTING!
                        int filestoload = 973; //Swap with next commented line during final work 
                        //int filestoload = 10; //TODO: Comment when done testing
                        //int filestoload = 40;
                        string pretendObjectLabel;
                        while((pretendObjectLabel = pretendObjectReader.ReadLine()) != null && filesloaded < filestoload) //TODO: REMOVE " && filesloaded < filestoload" CONDITION AFTER TESTING!
                        {
                            float[] inputTimeSeries = Array.ConvertAll<string, float>(timeseriesReader.ReadLine().Split(','), StringToFloat);
                            affordance = Array.ConvertAll<string, float>(labelsReader.ReadLine().Split(','), StringToFloat);

                            //string pretendObjectLabel = pretendObjectReader.ReadLine();
                            string pretendActionLabel = pretendActionReader.ReadLine();
                            //pretendActionLabel = pretendActionLabel.Substring(0, pretendActionLabel.IndexOf("."));

                            param = new float[1, inputTimeSeries.Length + affordance.Length];
                            float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);

                            for(int i = 0; i < values.Length; i++)
                            {
                                param[0, i] = values[i];
                            }

                            Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>()
                            {
                                { "concat", param },
                                { "keep_prob", 1.0f }
                            };

                            // Generating output inference
                            float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
                            //Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
                            float[,] outputSTDevs = model.GenerateOutput(generatorInputs, "encoder/dense_1/BiasAdd") as float[,];
                            //Debug.Log("Result successfully obtained. Results in form: " + outputSTDevs.GetLength(0) + "x" + outputSTDevs.GetLength(1));

                            // Finding the input's latent space value
                            vals = new List<float>();
                            foreach(float val in outputMeans)
                            {
                                vals.Add(val);
                            }
                            foreach(float val in outputSTDevs)
                            {
                                vals.Add(val);
                            }
                            vals.AddRange(affordance);

                            var affordanceKey = affordance;
                            //Debug.Log("KEY:  " + toStringFloat(affordanceKey));
                            float[,] paramjointdata = new float[1, inputTimeSeries.Length];

                            for(int i = 0; i < inputTimeSeries.Length; i++)
                            {
                                paramjointdata[0, i] = inputTimeSeries[i];
                            }

                            string propName = propAffordanceNameLookup[toStringFloat(affordanceKey)];
                            if(propName.Equals("Squid w Tentacles Partless OR Horseshoe"))
                            {
                                if(UnityEngine.Random.value <= 0.5F)
                                {
                                    propName = "Squid w Tentacles Partless";
                                }
                                else
                                {
                                    propName = "Horseshoe";
                                }
                            }
                            GestureDefinition gesture = new GestureDefinition(paramjointdata, propName, true, 0, Executive.dataType);
                            //gesture.GetGesture().objectName = propAffordanceNameLookup[toStringFloat(affordanceKey)];
                            //gesture.GetGesture().objectSize = propAffordanceNameLookup[toStringFloat(affordanceKey)];

                            gesture.pretendActionName = pretendActionLabel;
                            gesture.pretendObjectName = pretendObjectLabel;

                            //var guid = System.Guid.NewGuid();
                            var guid = Executive.GenerateGuid(vals, System.DateTime.Now);
                            gesture.gestureID = guid.ToString();

                            GestureCoordinate gestureCoordinate = new GestureCoordinate(new Envelope(vals[0], vals[1]), guid, pretendActionLabel, pretendObjectLabel, propName);

                            gesture.coordinateIndex = gestureCoordinate;

                            // Insert into LiteDB
                            gestureStrings.Insert(guid.ToString(), new SerializedGestureDefinition(gesture));
                            
                            // insert into RTree
                            rTree.Insert(gestureCoordinate);

                            // insert into Episodic Memory
                            tMemory.Insert(guid, GesturesLoaded);
                            
                            // insert into Reduced Gestural Dimensionality RTree
                            float[] gestureDRVector = GestureDR.Reduce(inputTimeSeries);
                            ReducedGesture redGesture = new ReducedGesture(new Envelope(gestureDRVector[0], gestureDRVector[1]), guid);
                            GestureDRTree.Insert(redGesture);

                            // insert into Reduced Action Dimensionality RTree
                            if (WordVectorLookup.ContainsKey(pretendActionLabel))
                            {
                                float[] actionDRVector = SemanticDR.Reduce(WordVectorLookup[pretendActionLabel]);
                                ReducedWord redActionWord = new ReducedWord(new Envelope(actionDRVector[0], actionDRVector[1]), pretendActionLabel);
                                ActionDRTree.Insert(redActionWord);
                            }


                            // insert into Reduced Object Dimensionality RTree
                            if (WordVectorLookup.ContainsKey(pretendObjectLabel))
                            {
                                float[] objectDRVector = SemanticDR.Reduce(WordVectorLookup[pretendObjectLabel]);
                                ReducedWord redObjectWord = new ReducedWord(new Envelope(objectDRVector[0], objectDRVector[1]), pretendObjectLabel);
                                ObjectDRTree.Insert(redObjectWord);
                            }

                            GesturesLoaded += 1;
                            /*
                            if (calculatingScores)
                            {
                                summerGifController.listOfGestureDefinitions.Add(gesture);
                            }
                            */

                            filesloaded++; //TODO: REMOVE AFTER TESTING!
                        }
                    }
                    else
                    {
                        throw new Exception("No gestures files exists in StreamingAssets Folder");
                    }

                }
            }
            else
            {
                // Debug.Log("Loading " + gestureStrings.Count() + " gestures");
                // Debug.Log("Loading start time: " + System.DateTime.Now);

                List<SerializedGestureDefinition> gestures = new List<SerializedGestureDefinition>(gestureStrings.FindAll());

                foreach(SerializedGestureDefinition gestureString in gestures)
                {
                    bool IsBadLoading = false;

                    GestureDefinition gesture = gestureString.ToGestureDefinition();

                    if(gesture == null)
                    {
                        // Debug.Log("Mapped GestureDefinition is null");
                        IsBadLoading = true;
                    }
                    //else
                    //{
                    //    if(gesture.playerStates == null)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.playerStates is null");
                    //        IsBadLoading = true;
                    //    }
                    //    else if(gesture.playerStates.Count == 0)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.playerStates is empty");
                    //        IsBadLoading = true;
                    //    }

                    //    if(gesture.objectStartingPosition == null)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.objectStartingPosition is null");
                    //        IsBadLoading = true;
                    //    }

                    //    if(gesture.objectStartingRotation == null)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.objectStartingRotation is null");
                    //        IsBadLoading = true;
                    //    }

                    //    if(gesture.propObject == null)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.propObject is null");
                    //        IsBadLoading = true;
                    //    }

                    //    if(gesture.coordinateIndex == null)
                    //    {
                    //        // Debug.Log("Mapped Gesture Definition.coordinateIndex is null");
                    //        IsBadLoading = true;
                    //    }
                    //}

                    if(!IsBadLoading)
                    {

                        //Debug.Log("Gesture Definition Loaded without problems");

                        // insert into RTree
                        //rTree = UnityEngine.Object.FindObjectOfType<Executive>().rTree;
                        rTree.Insert(gesture.coordinateIndex);

                        //Debug.Log("Inserted gesture coordinate into RTree");

                        // insert into Episodic Memory
                        tMemory.Insert(new Guid(gesture.gestureID), GesturesLoaded);

                        // insert into Reduced Gestural Dimensionality RTree
                        float[] gestureDRVector = GestureDR.Reduce(gesture);
                        ReducedGesture redGesture = new ReducedGesture(new Envelope(gestureDRVector[0], gestureDRVector[1]), new Guid(gesture.gestureID));
                        GestureDRTree.Insert(redGesture);

                        if (WordVectorLookup.ContainsKey(gesture.pretendActionName))
                        {
                            // insert into Reduced Action Dimensionality RTree
                            float[] actionDRVector = SemanticDR.Reduce(WordVectorLookup[gesture.pretendActionName]);
                            ReducedWord redActionWord = new ReducedWord(new Envelope(actionDRVector[0], actionDRVector[1]), gesture.pretendActionName);
                            ActionDRTree.Insert(redActionWord);
                        }
                        
                        if (WordVectorLookup.ContainsKey(gesture.pretendObjectName))
                        {
                            // insert into Reduced Object Dimensionality RTree
                            float[] objectDRVector = SemanticDR.Reduce(WordVectorLookup[gesture.pretendObjectName]);
                            ReducedWord redObjectWord = new ReducedWord(new Envelope(objectDRVector[0], objectDRVector[1]), gesture.pretendObjectName);
                            ObjectDRTree.Insert(redObjectWord);
                        }

                        GesturesLoaded += 1;

                        //Debug.Log("Inserted guid into Episodic Memory");
                    }
                    else
                    {
                        //Debug.Log("LiteDB load was unsuccessful");
                    }
                }

                // Debug.Log("Loading end time: " + System.DateTime.Now);
            }
        }
    }

    /// <summary>
    /// Populates WordVectorDictionary with the action/object words and their 300 vectors
    /// </summary>
    private void LoadWordVectorDict()
    {
        if (Directory.Exists(streamingAssetsPath))
        {
            StreamReader wordReader = new StreamReader(streamingAssetsPath + "/word_dict_cc.txt");
            string line = wordReader.ReadLine();
            while(line != null)
            {
                string[] lineSplit = line.Split(',');
                string word = lineSplit[0];
                float[] vector = new float[lineSplit.Length - 1];
                for (int i = 1; i < lineSplit.Length; i++)
                {
                    vector[i - 1] = StringToFloat(lineSplit[i]);
                }

                WordVectorLookup.Add(word, vector);
                line = wordReader.ReadLine();
            }
        }
    }

    //public class MyEqualityComparer : IEqualityComparer<float[]>
    //{
    //    public bool Equals(float[] x, float[] y)
    //    {
    //        if (x.Length != y.Length)
    //        {
    //            return false;
    //        }
    //        for (int i = 0; i < x.Length; i++)
    //        {
    //            if (x[i] != y[i])
    //            {
    //                return false;
    //            }
    //        }
    //        return true;
    //    }

    //    public int GetHashCode(float[] obj)
    //    {
    //        int result = 17;
    //        for (int i = 0; i < obj.Length; i++)
    //        {
    //            unchecked
    //            {
    //                result = result * 23 + (int)(obj[i]*375) + i;
    //            }
    //        }
    //        return result;
    //        //return System.Guid.NewGuid().GetHashCode();
    //    }
    //}

    //private static Dictionary<float[], string> propAffordanceNameLookup1 = new Dictionary<float[], string>(new MyEqualityComparer()) {
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Squid w Tentacles Partless"},
    //    {new float[]{0.6666667F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Lolliop w Cube Ends Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0.3333333F,0,0,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Giant U w Flat Base Partless"},
    //    {new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}, "Big Curved Axe Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0.3333333F,0,0,0,0,0}, "Sunflower Partless"},
    //    {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}, "Air Horn"},
    //    {new float[]{0,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Corn Dog"},
    //    {new float[]{0.3333333F,0.6666667F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}, "Giant Electric Plug"},
    //    {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.6666667F,0,0,0,0.3333333F,0,0.6666667F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}, "Ring w Tail End"},
    //    {new float[]{0,0.3333333F,0,0,0,0.6666667F,0,0.3333333F,0.6666667F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}, "Ring w Inward Spokes"},
    //    {new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}, "Large Ring w Outward Spokes"},
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}, "Tube w Cones"},
    //    {new float[]{0,0.6666667F,0,0,0,0,0,0.6666667F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Thin Stick w Hammer End"},
    //    {new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}, "Horseshoe"},
    //    {new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}, "Helix"},
    //    {new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Giant Golf T"},
    //    {new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.6666667F,0,0,0,0,0.6666667F,0,0,0,0.6666667F,0,0,0,0,0,0}, "Circle With Ring"},
    //    {new float[]{0,0,0,0,0.6666667F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.6666667F,0,0,0,0,0,0}, "Palm Tree Partless"},
    //    {new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.6666667F,0,0,0,0,0,0}, "Ladle"},
    //    {new float[]{0,0.6666667F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.6666667F,0,0,0,0,0,0}, "Plunger"}
    //};

    private static Dictionary<string, string> propAffordanceNameLookup = new Dictionary<string, string> {
        {"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Squid w Tentacles Partless OR Horseshoe"},
        //{"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Horseshoe"},
        {"0.6666667,0,0,0,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Lolliop w Cube Ends Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0.3333333,0,0,0,0.6666667,0.3333333,0,0,0,0,0", "Giant U w Flat Base Partless"},
        {"0,0.3333333,0.3333333,0,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0", "Big Curved Axe Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0,0,0.6666667,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0.3333333,0,0,0,0,0", "Sunflower Partless"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0", "Air Horn"},
        {"0,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Corn Dog"},
        {"0.3333333,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0,0.3333333,0,0,0,0,0", "Giant Electric Plug"},
        {"0,0.3333333,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0.3333333,0,0.6666667,0,0,0.3333333,0,0,0,0,0.3333333,0,0", "Ring w Tail End"},
        {"0,0.3333333,0,0,0,0.6666667,0,0.3333333,0.6666667,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0", "Ring w Inward Spokes"},
        {"0,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0", "Large Ring w Outward Spokes"},
        {"0,0.3333333,0,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0", "Tube w Cones"},
        {"0,0.6666667,0,0,0,0,0,0.6666667,0,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Thin Stick w Hammer End"},
        {"0,0,0,0,0,0,0.3333333,0,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0,0,0,0,0.3333333,0,0", "Helix"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Giant Golf T"},
        {"0,0,0,0.3333333,0,0.3333333,0,0,0,0.6666667,0,0,0,0,0.6666667,0,0,0,0.6666667,0,0,0,0,0,0", "Circle With Ring"},
        {"0,0,0,0,0.6666667,0,0,0,0.3333333,0.3333333,0,0.3333333,0.3333333,0,0,0,0,0,0.6666667,0,0,0,0,0,0", "Palm Tree Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Ladle"},
        {"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
        //{"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,1,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
    };

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }
    //Convert.ToSingle(Math.Round((decimal)affordanceKey[1], 7))
    private static float[] RoundFloat(float[] v, int dec)
    {
        float[] answer = new float[v.Length];
        for(int i = 0; i < v.Length; i++)
        {
            answer[i] = Convert.ToSingle(Math.Round((decimal)v[i], dec));
        }
        return answer;
    }

    private static string toStringFloat(float[] f_arr)
    {
        String answer = "";
        float[] rounded = RoundFloat(f_arr, 7);
        for(int i = 0; i < f_arr.Length - 1; i++)
        {
            answer += rounded[i].ToString();
            answer += ",";
        }
        answer += rounded[f_arr.Length - 1].ToString();
        return answer;
    }

    private static T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

    public GestureDefinition GetGestureFromDB(Guid id)
    {
        try
        {
            using(var db = new LiteDatabase(_strConnection))
            {
                LiteCollection<SerializedGestureDefinition> gestureStrings = db.GetCollection<SerializedGestureDefinition>("gestures");
                SerializedGestureDefinition gesture = gestureStrings.FindById(id.ToString());
                if(gesture != null)
                {
                    return gesture.ToGestureDefinition();
                }
            }
        }
        catch(Exception e)
        {
            // Debug.Log("Exception occured while loading gesture from DB: " + e.Message);
        }

        return null;
    }

    public Dictionary<string, float[]> GetWordDictionary()
    {
        return WordVectorLookup;
    }

    //public static void Save()
    //{
    //    using (var db = new LiteDatabase(_strConnection))
    //    {
    //        // Save RTree content
    //        var spatialIndices = db.GetCollection<GestureCoordinate>("spatialData");
    //        Queue<ISpatialData> gQueue = new Queue<ISpatialData>();
    //        gQueue.Enqueue(rTree.root);
    //        RTree<GestureCoordinate>.Node curr = (RTree<GestureCoordinate>.Node) gQueue.Dequeue();
    //        while (curr.IsLeaf == false)
    //        {
    //            //TODO FIX: WON'T WORK ONCE IT HITS A LEAF NODE.
    //            foreach(GestureCoordinate gc in curr.Children)
    //            {
    //                gQueue.Enqueue(gc);
    //            }
    //          
    //            curr = (RTree<GestureCoordinate>.Node) gQueue.Dequeue();
    //        }
    //        while (gQueue.Count > 0)
    //        {
    //            spatialIndices.Insert((GestureCoordinate) gQueue.Dequeue());
    //            spatialIndices.EnsureIndex(x => x.Guid);
    //        }
    //
    //        // Save Episodic Memory
    //        var temporalIndecies = db.GetCollection<TemporalMemory.Node>("temporalData");
    //        var temp = tMemory.head;
    //        while(temp.next != null)
    //        {
    //            temporalIndecies.Insert(temp);
    //            temp = temp.next;
    //        }
    //    }
    //}
}
