﻿using System;
using System.Collections;
using System.Collections.Generic;

// This class will pick the most suitable response strategy for the partner robot to follow
internal class StrategySelectionController
{
    private CreativeArc CArc;
    private TimeSpan duration;
    private Combination combination;
    private Mimicry mimicry;
    private NoveltyGeneration novelGen;
    private SurpriseGeneration surpriseGen;
    private Transformation transformation;
    private ValueModulation valueMod;
    public ResponseStrategy[] Strategies { get; }
    private int index;
    private RTree<GestureCoordinate> rTree;
    private TemporalMemory TemporalMemory;

    public StrategySelectionController(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory): this(new CreativeArc(), TimeSpan.Zero, rTree, temporalMemory) {}
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="arc">The creative arc that the partner robot's actions should follow</param>
    /// <param name="sessionDuration">The duration of the partner robot's session</param>
    /// <param name="temporalMemory"></param>
    public StrategySelectionController(CreativeArc arc, TimeSpan sessionDuration, RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory)
    {
        this.CArc = arc;
        this.duration = sessionDuration;
        this.rTree = rTree;
        this.TemporalMemory = temporalMemory;

        combination = new Combination(rTree, temporalMemory);
        mimicry = new Mimicry(rTree, temporalMemory);
        novelGen = new NoveltyGeneration(rTree, temporalMemory);
        surpriseGen = new SurpriseGeneration(rTree, temporalMemory);
        transformation = new Transformation(rTree, temporalMemory);
        valueMod = new ValueModulation(rTree, temporalMemory);

        Strategies = new ResponseStrategy[] {mimicry, combination, transformation};//TODO: Add Random Generation!

        //Random rand = new Random();
        //index = rand.Next(Strategies.Length);
        index = 0;
    }

    /// <summary>
    /// Picks a response strategy that will bring the current location closest to the target location within the creative space
    /// </summary>
    /// <param name="lastGesture">The last gesture the robot partner performed</param>
    /// <returns>The most suitable response strategy</returns>
    public ResponseStrategy SelectResponseStrategy(GestureDefinition lastGesture)
    {
        if (lastGesture.IsGestureTooShort && index == 0)
        {
            return Strategies[1];
        }
        else
        {
            return Strategies[index];
        }
    }

    /// <summary>
    /// Gives an array with all of the response strategies
    /// </summary>
    /// <returns>an array of response strategies</returns>
    public ResponseStrategy[] SelectResponseStrategyArray()
    {
        return Strategies;
    }

    /// <summary>
    /// Sets the current strategy to the one at index i within the Strategies list
    /// </summary>
    /// <param name="i">The index of the desired response strategy</param>
    public void SetStrategy(int i)
    {
        index = i;
        UnityEngine.Debug.Log("The strategy is now " + Strategies[index]);
    }

}
