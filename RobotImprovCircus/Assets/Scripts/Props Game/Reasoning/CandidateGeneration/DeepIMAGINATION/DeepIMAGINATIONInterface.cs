﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DeepIMAGINATIONInterface : ModelInterface
{
    public bool isInit = false;
    //private bool generate = false;
    private System.Random random = new System.Random();

    public int nLatents = 2;

    //private AgentPlayer playback;
    //private PropManager propManager;
    private Executive executive;

    public Dictionary<string, float[]> propNameAffordanceLookup = new Dictionary<string, float[]> {
        {"Squid w Tentacles Partless", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Lolliop w Cube Ends Partless", new float[]{0.666666F,0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant U w Flat Base Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0.3333333F,0,0,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Big Curved Axe Partless", new float[]{0,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Sunflower Partless", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0,0,0.666666F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0.3333333F,0,0,0,0,0}},
        {"Air Horn", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0}},
        {"Corn Dog", new float[]{0,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Giant Electric Plug", new float[]{0.3333333F,0.666666F,0,0,0,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0.3333333F,0.3333333F,0.3333333F,0,0,0,0,0.3333333F,0,0,0,0,0}},
        {"Ring w Tail End", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0,0.666666F,0,0,0,0.3333333F,0,0.666666F,0,0,0.3333333F,0,0,0,0,0.3333333F,0,0}},
        {"Ring w Inward Spokes", new float[]{0,0.3333333F,0,0,0,0.666666F,0,0.3333333F,0.666666F,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0}},
        {"Large Ring w Outward Spokes", new float[]{0,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0}},
        {"Tube w Cones", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0,0,0.3333333F,0,0}},
        {"Thin Stick w Hammer End", new float[]{0,0.666666F,0,0,0,0,0,0.666666F,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Horseshoe", new float[]{0,0.3333333F,0,0,0.3333333F,0,0,0,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,0,0,0,0,0,0,0}},
        {"Helix", new float[]{0,0,0,0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0,0,0,0,0,0,0,0,0,0.3333333F,0,0}},
        {"Giant Golf T", new float[]{0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Circle With Ring", new float[]{0,0,0,0.3333333F,0,0.3333333F,0,0,0,0.666666F,0,0,0,0,0.666666F,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Palm Tree Partless", new float[]{0,0,0,0,0.666666F,0,0,0,0.3333333F,0.3333333F,0,0.3333333F,0.3333333F,0,0,0,0,0,0.666666F,0,0,0,0,0,0}},
        {"Ladle", new float[]{0,0.3333333F,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0.3333333F,0,0.3333333F,0,0.3333333F,0,0.666666F,0,0,0,0,0,0}},
        {"Plunger", new float[]{0,0.666666F,0,0,0.3333333F,0,0,0,0.3333333F,0.3333333F,0,0,0,0,0,1,0,0,0.666666F,0,0,0,0,0,0}}
    };
    private Dictionary<string, float[]> propDefaultNameAffordanceLookup = new Dictionary<string, float[]> {
        {"NULL", new float[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}}
    };
    float[] initialRandomVariables;

    /// <summary>
    /// Start this instance.
    /// </summary>
    public void Start()
    {
        initialRandomVariables = new float[nLatents];

        switch (Executive.dataType)
        {
            case DataEnum.Data27000:
                {
                    nInputs = 27000;
                    break;
                }

            case DataEnum.Data16000:
                {
                    nInputs = 16000;
                    break;
                }
        }

        //playback = gameObject.GetComponent<AgentPlayer>();
        //propManager = gameObject.GetComponent<PropManager>();
        executive = gameObject.GetComponent<Executive>();

        if (Executive.dataType == DataEnum.Data16000)
        {
            GraphAsset = Resources.Load("affordance-cvae-16000-2-final.ckpt") as TextAsset;
        }
        else if (Executive.dataType == DataEnum.Data27000)
        {
            GraphAsset = Resources.Load("affordance-cvae-27000-2-final.ckpt") as TextAsset;
        }
        InitModel();
    }

    override protected void InitModel()
    {
        float[,] param = new float[1, initialRandomVariables.Length + propDefaultNameAffordanceLookup["NULL"].Length];
        float[] values = Concatenate1DArrays<float>(initialRandomVariables, propDefaultNameAffordanceLookup["NULL"]);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }
        Dictionary<string, dynamic> initInputs = new Dictionary<string, dynamic>() {
            { "concat_1", param},
            { "keep_prob", 1.0F }
        };
        string outputNodeName = "decoder/Reshape_1";
        isInit = Model.InitializeModel(GraphAsset.bytes, initInputs, outputNodeName);
        if (isInit)
        {
            // Debug.Log("Model initialized");
        }
        else
        {
            // Debug.Log("Model not initialized");
        }
    }

    /// <summary>
    /// Update this instance.
    /// </summary>
    public void Update()
    {
        //if(isInit)
        //{
        //    HandleInput();
        //}

        //if(isInit && generate)
        //{
        //    GenerateAction();
        //}
    }

    private string Rank2ArrayToString<T>(T[,] array, string separator)
    {
        string result = "[";
        foreach (T cell in array)
        {
            result += "" + cell.ToString() + separator;
        }
        return result.Substring(0, result.Length - separator.Length) + "]";
    }

    /// <summary>
    /// Handle input.
    /// </summary>
    private void HandleInput()
    {
        //if (Input.GetKeyDown (KeyCode.Return))
        //{
        //	generate = true;
        //}
    }

    /// <summary>
    /// Concatenates the 1D arrays.
    /// </summary>
    /// <returns>The concatenated 1D array.</returns>
    /// <param name="a">The first 1D array.</param>
    /// <param name="b">The second 1D array.</param>
    private T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

    /// <summary>
    /// Gets a random value from dictionary.
    /// </summary>
    /// <returns>The random value from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TValue GetRandomValueFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TValue> values = Enumerable.ToList(dictionary.Values);
        int size = dictionary.Count;
        return values[random.Next(size)];
    }

    /// <summary>
    /// Gets a random key from dictionary.
    /// </summary>
    /// <returns>The random key from dictionary.</returns>
    /// <param name="dictionary">Dictionary.</param>
    /// <typeparam name="TKey">The key's type parameter.</typeparam>
    /// <typeparam name="TValue">The value's type parameter.</typeparam>
    public TKey GetRandomKeyFromDictionary<TKey, TValue>(IDictionary<TKey, TValue> dictionary)
    {
        List<TKey> keys = Enumerable.ToList(dictionary.Keys);
        int size = dictionary.Count;
        return keys[random.Next(size)];
    }

    //public void GenerateActionWithRandomProp()
    //{
    //    bool isGestureTooShort = true;
    //    while (isGestureTooShort == true)
    //    {
    //        float[] samples = new float[nLatents];
    //        for (int i = 0; i < nLatents; i++)
    //        {
    //            samples[i] = (float)random.NextDouble() * 6 - 3;
    //        }
    //        string propName = GetRandomKeyFromDictionary(propNameAffordanceLookup);
    //        // Debug.Log("Prop Name: " + propName);
    //        float[,] param = new float[1, samples.Length + propNameAffordanceLookup[propName].Length];
    //        float[] values = Concatenate1DArrays<float>(samples, propNameAffordanceLookup[propName]);
    //        for (int i = 0; i < values.Length; i++)
    //        {
    //            param[0, i] = values[i];
    //        }
    //        string input = Rank2ArrayToString<float>(param, ", ");
    //        Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
    //            { "concat_1", param },
    //            { "keep_prob", 1.0F }
    //        };
    //        string outputNodeName = "decoder/Reshape_1";
    //        float[,] output = model.GenerateOutput(generatorInputs, outputNodeName) as float[,];
    //        // Debug.Log("Result successfully obtained. Results in form: " + output.GetLength(0) + "x" + output.GetLength(1));

    //        playback.recording = new RecordedGesture(output, propName, true, 0, dataType);
    //        isGestureTooShort = playback.recording.isGestureTooShort;
    //        propManager.SetPropName(propName);

    //        // Debug.Log("Finished loading generated gesture");
    //    }

    //    generate = false;
    //}

    //public void GenerateAction()
    //{
    //    bool isGestureTooShort = true;
    //    while (isGestureTooShort == true)
    //    {
    //        float[] samples = new float[nLatents];
    //        for (int i = 0; i < nLatents; i++)
    //        {
    //            samples[i] = (float)random.NextDouble() * 8 - 3;
    //        }
    //        string propName = propManager.GetCurrentPropName();
    //        // Debug.Log("Prop Name: " + propName);
    //        playback.recording = new RecordedGesture(FindAction(samples, propName));
    //        isGestureTooShort = playback.recording.isGestureTooShort;
    //        if(isGestureTooShort)
    //        {
    //            continue;
    //        }

    //        Guid guid = Executive.GenerateGuid(new List<float>(samples), System.DateTime.Now);
    //        playback.recording.GetGesture().gestureID = guid.ToString();
    //        playback.recording.GetGesture().coordinateIndex = new GestureCoordinate(new Envelope(samples[0], samples[1]), guid);

    //        string pan = executive.KNNLabel(playback.recording.GetGesture().coordinateIndex, executive.kForLabeling, executive.weightedDistanceLabeling, true);
    //        string pon = executive.KNNLabel(playback.recording.GetGesture().coordinateIndex, executive.kForLabeling, executive.weightedDistanceLabeling, false);

    //        playback.recording.GetGesture().pretendActionName = pan;
    //        playback.recording.GetGesture().pretendObjectName = pon;
    //        playback.recording.GetGesture().coordinateIndex.pretendActionLabel = pan;
    //        playback.recording.GetGesture().coordinateIndex.pretendObjectLabel = pon;
    //        playback.recording.GetGesture().coordinateIndex.propName = propManager.GetCurrentPropName();

    //        // Debug.Log("Finished loading generated gesture");
    //    }

    //    // Debug.Log("Finished loading generated gesture");
    //    // Debug.Log("pretendActionName: " + pan + " PretendObjectName: " + pon);
    //    generate = false;
    //}

    /// <summary>
    /// Generates the latent space coordinate of a given gesture.
    /// </summary>
    /// <param name="gesture">The gesture to be evaluated</param>
    /// <returns>List of coordinate values</returns>
    public List<float> GenerateLatentSpaceCoordinate(GestureDefinition gesture)
    {
        GestureVector vector = null;

        switch (Executive.dataType)
        {
            case DataEnum.Data27000:
                {
                    //vector = new GestureVector(gesture, 0, true, true, nInputs, 0, Executive.dataType);
                    vector = new GestureVector(gesture, Executive.dataType);
                    break;
                }

            case DataEnum.Data16000:
                {
                    //vector = new GestureVector(gesture, 0, true, true, nInputs, 0, Executive.dataType);
                    vector = new GestureVector(gesture, Executive.dataType);
                    break;
                }
        }

        float[] inputTimeSeries = vector.To1DFloat();
        float[] affordance = propNameAffordanceLookup[gesture.propObject.name];

        float[,] param = new float[1, inputTimeSeries.Length + affordance.Length];
        float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);
        for (int i = 0; i < values.Length; i++)
        {
            param[0, i] = values[i];
        }
        Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
            { "concat", param },
            { "keep_prob", 1.0F }
        };

        // Generating output inference
        float[,] outputMeans = Model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
        //Debug.Log("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));

        // Finding the input's latent space value
        List<float> spaceCoordinate = new List<float>();
        foreach (float val in outputMeans)
        {
            //Debug.Log("" + val);
            spaceCoordinate.Add(val);
        }
        spaceCoordinate.AddRange(affordance);
        //Debug.Log("Finished calculating latent space coordinate");

        return spaceCoordinate;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="samples"></param>
    /// <returns></returns>
    public GestureDefinition FindAction(float[] samples, string propName)
    {
        try
        {
            float[,] param = new float[1, samples.Length + propNameAffordanceLookup[propName].Length];
            float[] values = Concatenate1DArrays<float>(samples, propNameAffordanceLookup[propName]);
            for (int i = 0; i < values.Length; i++)
            {
                param[0, i] = values[i];
            }

            Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>() {
            { "concat_1", param },
            { "keep_prob", 1.0F }
        };
            string outputNodeName = "decoder/Reshape_1";
            float[,] output = Model.GenerateOutput(generatorInputs, outputNodeName) as float[,];
            //Debug.Log("Result successfully obtained. Results in form: " + output.GetLength(0) + "x" + output.GetLength(1));
            GestureDefinition newGesture = new GestureDefinition(Executive.dataType);
            newGesture.LoadStates(output, propName, true, 0);
            Guid guid = Executive.GenerateGuid(new List<float>(values), System.DateTime.Now);
            newGesture.gestureID = guid.ToString();

            // saving the coordinates
            Envelope env = new Envelope(samples[0], samples[1]);
            newGesture.coordinateIndex = new GestureCoordinate(env, guid, "", "", propName);

            // labeling generated action
            string pan = executive.KNNLabel(newGesture.coordinateIndex, executive.kForLabeling, executive.weightedDistanceLabeling, true);
            string pon = executive.KNNLabel(newGesture.coordinateIndex, executive.kForLabeling, executive.weightedDistanceLabeling, false);

            newGesture.coordinateIndex.pretendActionLabel = newGesture.pretendActionName = pan;
            newGesture.coordinateIndex.pretendObjectLabel = newGesture.pretendObjectName = pon;

            return newGesture;
        }
        catch (Exception e)
        {
            throw e;
        }
    }
}
