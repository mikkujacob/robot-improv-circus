﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System;
using System.Linq;
using System.Globalization;

// Represents the decision cycle for the robot partner.
public class Executive : MonoBehaviour
{
    public static DataEnum dataType = DataEnum.Data16000;
    public static bool IsNegotiating = true;

    private StrategySelectionController Selector;
    private int CurrentStrategy;

    private BlockingCollection<GestureDefinition> InputQueue;
    public BlockingCollection<GestureDefinition> OutputQueue { get; set; }

    private DeepIMAGINATIONInterface DeepIMAGINATION;
    private GesturalDimRed GesturalDR;
    private SemanticDimRed SemanticDR;
    private RTree<GestureCoordinate> rTree { get; set; }
    private TemporalMemory temporalMemory { get; set; }
    private RTree<ReducedGesture> GestureDRTree { get; set; }
    private RTree<ReducedWord> ActionDRTree { get; set; }
    private RTree<ReducedWord> ObjectDRTree { get; set; }
    private LoadGestures loadGestures;

    private CreativeArc currentCurve;
    public string currentCurveType;

    private Novelty NoveltyEval;
    private Surprise SurpriseEval;
    private Value ValueEval;

    public int kForLabeling = 3;
    public bool weightedDistanceLabeling = false;

    public float EMASmoothingAlpha = 1f;

    private System.Random random;

    private bool busy;

    /// set true to run decision cycle
    public bool IsRunning { get; set; }

    // Use this for initialization
    void Start()
    {
        InputQueue = new BlockingCollection<GestureDefinition>();
        OutputQueue = new BlockingCollection<GestureDefinition>();

        SetCurrentCurve();

        // Instantiate DeepIMAGINATIONInterface here
        //DeepIMAGINATION = GameObject.Find("MImEGlobalObject").GetComponent<DeepIMAGINATIONInterface>();
        DeepIMAGINATION = GameObject.FindObjectOfType<DeepIMAGINATIONInterface>();
        GesturalDR = GameObject.FindObjectOfType<GesturalDimRed>();
        SemanticDR = GameObject.FindObjectOfType<SemanticDimRed>();
        loadGestures = GameObject.FindObjectOfType<LoadGestures>();

        random = new System.Random();

        //rTree = new RTree<GestureCoordinate>();

        Restart();

        loadGestures.InitiateLoader(rTree, temporalMemory, DeepIMAGINATION.GetModel(), GestureDRTree, ActionDRTree, ObjectDRTree, GesturalDR, SemanticDR);

        Invoke("LoadGestures", 2.0f);
    }

    public void LoadGestures()
    {
        loadGestures.LoadGesturesFromDisk();
        Debug.Log("Loaded Gestures.");
    }

    // Update is called once per frame
    void Update()
    {
        InputHandler();
    }

    void FixedUpdate()
    {
        if (!busy && IsRunning)
        //if (IsRunning)
        {
            _ = Task.Run(() => TryDequeueInput());
        }
    }

    /// <summary>
    /// Adds a new gesture into the input queue for processing
    /// </summary>
    /// <param name="input">The input gesture</param>
    public void AddNewGesture(GestureDefinition input)
    {
        //GestureDefinition newGesture = input.DeepCopy();
        //InputQueue.Add(input);
        GestureDefinition clone = input.DeepCopy();
        InputQueue.Add(clone);
        // Debug.Log("Added a gesture with ID: " + clone.gestureID);
    }

    public async Task AddRandomGestureToOutputQueue(string propName)
    {
        // Debug.Log("Started process to generate random gesture and add it to output queue");
        //Task<GestureDefinition> generatorTask = GenerateRandomGesture(propName);
        GestureDefinition gesture = await GenerateRandomGesture(propName);

        //OutputQueue.Add(generatorTask.Result);
        OutputQueue.Add(gesture);
    }

    public void AddSpecificGestureToOutPutQueue(GestureDefinition gesture)
    {
        OutputQueue.Add(gesture);
    }

    public async Task AddRandomGestureToInputQueue(string propName)
    {
        // Debug.Log("Started process to generate random gesture and add it to input queue");
        //Task<GestureDefinition> generatorTask = GenerateRandomGesture(propName);
        GestureDefinition gesture = await GenerateRandomGesture(propName);

        //InputQueue.Add(generatorTask.Result);
        InputQueue.Add(gesture);
    }

    private async Task<GestureDefinition> GenerateRandomGesture(string propName)
    {
        bool isTooShort = true;
        GestureDefinition gesture = null;
        int tryGenerateCount = 0;
        int maxTryCount = 3;
        while (isTooShort && tryGenerateCount < maxTryCount)
        {
            // Debug.Log("Generating random output gesture");
            gesture = await Task.Run(() => LoadRandomFromMemoryOrGenerateRandom(propName, true));
            // Debug.Log("Setting duration");
            gesture.SetDuration();
            isTooShort = gesture.IsGestureTooShort;
            tryGenerateCount++;
        }
        // Debug.Log("Maxed try count: " + (tryGenerateCount >= maxTryCount));
        // Debug.Log("Generating labels");
        gesture.pretendActionName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, true);
        gesture.pretendObjectName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, false);
        //Debug.Log("Generated Random - Pretend Action Name: " + gesture.pretendActionName + " Pretend Object Name: " + gesture.pretendObjectName);
        //float[] creativity = FindCreativeSpaceLocation(gesture);

        //await Task.Delay(1);

        return gesture;
        // Debug.Log("Added the generated random gesture with ID " + gesture.gestureID + " to output queue");
    }

    private GestureDefinition LoadRandomFromMemoryOrGenerateRandom(string propName, bool tryUseDataset) // TODO: LARGER QUESTION! Should this be a response strategy? If so, how does this entire random gesture generation and injection work in the decision cycle?
    {
        // Debug.Log("Loading random from memory or generating random");
        int temporalIndex = -1;
        // Best case try one of the dataset gestures
        if (tryUseDataset && loadGestures.GesturesLoaded > 0)
        {
            // Debug.Log("Loading random from dataset in memory");
            temporalIndex = random.Next(loadGestures.GesturesLoaded);
        }
        else // Otherwise try anything from memory
        {
            // Debug.Log("Loading random from memory");
            int max = temporalMemory.GetLastTemporalIndex();
            if (max >= 0)
            {
                // Debug.Log("There were gestures already");
                temporalIndex = random.Next(max);
            }
        }

        bool retrievalSucceeded = false;
        Guid generatedGuid = Guid.NewGuid();
        if (temporalIndex > 0) // If memory retrieval is possible
        {
            // Debug.Log("Can do a retrieve");
            TemporalMemory.Node retrievedNode = temporalMemory.GetNode(temporalIndex);
            // Debug.Log("Attempted retrieval");
            if (retrievedNode != null) // Retrieval worked
            {
                // Debug.Log("Retrieved success");
                generatedGuid = retrievedNode.id;
                retrievalSucceeded = true;
            }
        }

        //Find coordinates of retrieved gesture OR generate random samples.
        int coordSize = DeepIMAGINATION.nLatents;
        float[] coordinates = new float[coordSize];
        if (retrievalSucceeded) // If a gesture guid was retrieved
        {
            // Debug.Log("Retrieved success and generating coordinates of the retrieved gesture");
            GestureDefinition gesture = loadGestures.GetGestureFromDB(generatedGuid);
            if (gesture != null)
            {
                // Debug.Log("Retrieved success and getting coordinates of the retrieved gesture");
                Envelope envelope = gesture.coordinateIndex.Envelope;
                coordinates = new float[] { (float)envelope.CenterX, (float)envelope.CenterY }; // WITH ENVELOPE FIXED TO X AND Y, IT'S NO BETTER THAN PREVIOUS APPROACH. BUT MORE GENERALIZABLE
            }
            else
            {
                // Debug.Log("Retrieved failed and regenerating coordinates of the retrieved gesture from guid");
                coordinates = FindCoordinates(generatedGuid); // TODO: FAILS FOR NLATENTS != 2. THIS NEEDS TO BE GENERALIZED AND FIXED! // FIRST ATTEMPT AT USING DB TO RESOLVE THIS ABOVE!
            }
        }
        else // If nothing else, generate random samples
        {
            // Debug.Log("last resort, generate random sampled gesture");
            for (int i = 0; i < coordSize; i++)
            {
                coordinates[i] = (float)(random.NextDouble() * 6.0 - 3.0);
            }
        }

        // Debug.Log("Generating gesture with coordinates " + coordinates[0] + ", " + coordinates[1]);
        GestureDefinition output = DeepIMAGINATION.FindAction(coordinates, propName); //Generate from some set of latent space coordinates

        output.EMASmootheGesture(EMASmoothingAlpha);

        return output;
    }

    /// <summary>
    /// Removes the item at the front of the queue. If queue is empty, then it waits.
    /// </summary>
    /// <returns>A <c>Task</c> with the gesture input from the human</returns>
    private async Task TryDequeueInput()
    {
        try
        {
            GestureDefinition input;
            if (InputQueue.TryTake(out input, TimeSpan.FromMilliseconds(1)))
            {
                busy = true;
                //Task cycle = 
                await StartDecisionCycle(input);
                // Debug.Log("Started decision cycle for input gesture with ID: " + input.gestureID);
            }
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
        }
    }

    /// <summary>
    /// Starts robot partner's decision cycle
    /// </summary>
    /// <param name="playerGesture">The collaborator's most recent gesture</param>
    private async Task StartDecisionCycle(GestureDefinition playerGesture)
    {
        //Debug.Log("START: StartDecisionCycle - uses player gesture");

        // trim the gesture if it is longer than the model can receive as an input or add the last frame to the end as padding if too short
        GestureDefinition trimmedGesture = TrimGesture(playerGesture, dataType);

        float[] scores = await AddPerceptToMemory(trimmedGesture);
        //Debug.Log("Added percept to memory for input gesture with ID: " + trimmedGesture.gestureID);
        GestureDefinition response = await FormResponse(trimmedGesture, scores);
        // Debug.Log("Got a response for input gesture with ID: " + trimmedGesture.gestureID);
        PartnerPerform(response);
        //Debug.Log("DONE: StartDecisionCycle - player decision cycle complete");
    }

    /// <summary>
    /// Adds new percept to the agent's memory
    /// <param name="gesture"> The human player's gesture. </param>
    /// </summary>
    private async Task<float[]> AddPerceptToMemory(GestureDefinition playerGesture)
    {
        try
        {
            //Debug.Log("START: AddPerceptToMemory - first method called in StartDecisionCycle");
            List<float> coords = await Task.Run(() => ClassifyPercept(playerGesture));

            //var t2 = Task.Run(() => FindCreativeSpaceLocation(playerGesture));

            //await Task.WhenAll(t1, t2);

            //Debug.Log("Classified percept and found percept creative space location: Latent Space Coordinates: " + coords[0] + ", " + coords[1]);

            Envelope envelope = new Envelope(coords[0], coords[1]);
            DateTime timestamp = StringToDateTime(playerGesture.timestamp);
            Guid guid = GenerateGuid(coords, timestamp);
            temporalMemory.InsertAtEnd(guid);

            //Debug.Log("Generated a GUID for percept: " + guid.ToString() + " at time: " + timestamp.ToString());
            playerGesture.coordinateIndex = new GestureCoordinate(envelope, guid, "", "", playerGesture.propObject.name);

            playerGesture.pretendActionName = playerGesture.coordinateIndex.pretendActionLabel = KNNLabel(playerGesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, true);
            playerGesture.pretendObjectName = playerGesture.coordinateIndex.pretendObjectLabel = KNNLabel(playerGesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, false);

            float[] scores = new float[3];
            if(IsNegotiating)
            {
                scores = await Task.Run(() => FindCreativeSpaceLocation(playerGesture));
                //Debug.Log("Player gesture scores for negotiating - N: " + scores[0] + ", S: " + scores[1] + ", V: " + scores[2]);
            }

            // add to memory r tree
            rTree.Insert(playerGesture.coordinateIndex);

            PostLocalizationUpdate(playerGesture);

            //Debug.Log("Added player gesture coordinates to semantic memory");
            //Debug.Log("DONE: AddPerceptToMemory");

            return scores;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not Add Percept to Nemory");
            Debug.Log(e.Message);

            return new float[3];
        }
    }

    /// <summary>
    /// Decides the partner robot's next action
    /// </summary>
    private async Task<GestureDefinition> FormResponse(GestureDefinition gesture, float[] playerScores)
    {
        try
        {
            //Debug.Log("START: FormResponse - 2nd method called in StartDecisionCycle");
            //Get the array of strategies
            var strategies = Selector.SelectResponseStrategyArray();

            //Make list of GestureDefinitions using each strategy
            var gestures = await GenerateActions(strategies, gesture);

            GestureDefinition response = PickAction(gestures, playerScores);

            if (String.IsNullOrEmpty(response.pretendActionName) || String.IsNullOrEmpty(response.pretendObjectName) || response.pretendActionName == " " || response.pretendObjectName == " ")
            {
                response.pretendActionName = KNNLabel(response.coordinateIndex, kForLabeling, weightedDistanceLabeling, true);
                response.pretendObjectName = KNNLabel(response.coordinateIndex, kForLabeling, weightedDistanceLabeling, false);
            }

            response.EMASmootheGesture(EMASmoothingAlpha);

            //Debug.Log("Finished generating a gesture.");
            //Debug.Log("Generated Response - Pretend Action Name: " + response.pretendActionName + " Pretend Object Name: " + response.pretendObjectName);

            rTree.Insert(response.coordinateIndex);
            PostLocalizationUpdate(response);

            //Debug.Log("DONE: FormResponse");
            return response;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not Form Response");
            Debug.Log(e.Message);
            return gesture;
        }
    }

    /// <summary>
    /// Identifies percept with similarities from previously
    /// stored percepts within the agent's memory
    /// <param name="playerGesture"> The human player's gesture. </param>
    /// </summary>
    private List<float> ClassifyPercept(GestureDefinition playerGesture)
    {
        try
        {
            // classify the percept into latent space coordinates
            // Debug.Log("Classifying percept.");

            // trim the gesture if it is longer than the model can receive as an input or add the last frame to the end as padding if too short
            //GestureDefinition trimmedGesture = TrimPlayerGesture(playerGesture, dataType);

            // find latent space coordinate
            //List<float> coordList = DeepIMAGINATION.GenerateLatentSpaceCoordinate(trimmedGesture);
            List<float> coordList = DeepIMAGINATION.GenerateLatentSpaceCoordinate(playerGesture);
            return coordList;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not classify percept");
            Debug.Log(e.Message);
            List<float> coords = new List<float>();
            coords.Add(0);
            coords.Add(0);
            return coords;
        }
    }

    public GestureDefinition TrimGesture(GestureDefinition playerGesture, DataEnum dataType)
    {
        try
        {
            //GestureDefinition trimmedGesture = playerGesture;
            GestureDefinition trimmedGesture = playerGesture.DeepCopy();
            switch (dataType)
            {
                case DataEnum.Data27000:
                    {
                        int totalFrameLength = trimmedGesture.playerStates.Count;
                        int totalInputLength = totalFrameLength * 30;
                        int modelInputLength = 27000;
                        int maxFrameLength = 900;

                        if (totalInputLength == modelInputLength && totalFrameLength == maxFrameLength)
                        {
                            return trimmedGesture;
                        }
                        else if (totalInputLength < modelInputLength && totalFrameLength < maxFrameLength)
                        {
                            PlayerStateDefinition lastPose = trimmedGesture.playerStates[trimmedGesture.playerStates.Count - 1];
                            for (int i = 0; i < maxFrameLength - totalFrameLength; i++)
                            {
                                trimmedGesture.AddState(lastPose);
                            }

                            trimmedGesture.SetDuration();
                        }
                        else
                        {
                            //Too long, add trimming to this case.
                            trimmedGesture.playerStates.RemoveRange(maxFrameLength, totalFrameLength - maxFrameLength);
                        }

                        break;
                    }

                case DataEnum.Data16000:
                    {
                        int totalFrameLength = trimmedGesture.playerStates.Count;
                        int totalInputLength = totalFrameLength * 35;
                        int modelInputLength = 16000;
                        int paddingLength = 250;
                        int adjustedTotalmodelLength = (modelInputLength - paddingLength) * 2;
                        int maxFrameLength = 450 * 2;

                        if (totalInputLength == adjustedTotalmodelLength && totalFrameLength == maxFrameLength)
                        {
                            //Debug.Log("Length was exact. No need to trim");
                            return trimmedGesture;
                        }
                        else if (totalInputLength < adjustedTotalmodelLength && totalFrameLength < maxFrameLength)
                        {
                            //Debug.Log("Too short. Added last frame for padding.");
                            PlayerStateDefinition lastPose = trimmedGesture.playerStates[trimmedGesture.playerStates.Count - 1];
                            for (int i = 0; i < maxFrameLength - totalFrameLength; i++)
                            {
                                trimmedGesture.AddState(lastPose);
                            }

                            trimmedGesture.SetDuration();
                        }
                        else
                        {
                            //Debug.Log("Too long. Trim this.");
                            trimmedGesture.playerStates.RemoveRange(maxFrameLength, totalFrameLength - maxFrameLength);
                        }

                        break;
                    }
            }

            return trimmedGesture;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return playerGesture;
        }
    }

    /// <summary>
    /// Evaluates a gesture's novelty, surprise, and value
    /// to locate where the action would exist within the 
    /// 3D creative space
    /// <param name="gesture"> Action to be identified within the 3D creative space. </param>
    /// </summary>
    public float[] FindCreativeSpaceLocation(GestureDefinition gesture)
    {
        // Evaluation code
        // Debug.Log("Evaluating NSV of percept.");

        // 0 = novelty
        // 1 = surprise
        // 2 = value
        float[] scores = new float[3];

        try
        {
            //Debug.Log("START: FindCreativeSpaceLocation");
            gesture.pretendActionName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, true);
            gesture.pretendObjectName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, false);

            try
            {
                //Divide by 4? Need to normalize Novelty score to between 0 and 1
                scores[0] = (NoveltyEval.CalculateScore(gesture, 0.5f));
                //Debug.Log("The novelty score of the gesture with action " + gesture.pretendActionName + " and object " + gesture.pretendObjectName + " is " + scores[0]);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR: Can't calculate novelty score");
                Debug.Log(e.Message);
                scores[0] = 0;
            }

            try
            {
                //CHECK
                scores[1] = SurpriseEval.CalculateScore(gesture);
                //Debug.Log("The surprise score of the gesture with action " + gesture.pretendActionName + " and object " + gesture.pretendObjectName + " and prop name " + gesture.propObject.name + " is " + scores[1]);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR: Can't calculate surprise score");
                Debug.Log(e.Message);
                scores[1] = 0;
            }

            try
            {
                //CHECK
                scores[2] = ValueEval.CalculateScore(gesture);
                //Debug.Log("The value score of the gesture with action " + gesture.pretendActionName + " and object " + gesture.pretendObjectName + " and prop name " + gesture.propObject.name + " is " + scores[2]);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR: Can't calculate value score");
                Debug.Log(e.Message);
                scores[2] = 0;
            }
            //Debug.Log("DONE: FindCreativeSpaceLocation");
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not find creative space location");
            Debug.Log(e.Message);
        }

        return scores;
    }

    public void PostLocalizationUpdate(GestureDefinition gesture)
    {
        try
        {
            //Debug.Log("START: PostLocalizationUpdate()");

            float[] aScore = SemanticDR.Reduce(loadGestures.GetWordDictionary()[gesture.pretendActionName]);
            ReducedWord redActionWord = new ReducedWord(new Envelope(aScore[0], aScore[1]), gesture.pretendActionName);
            ActionDRTree.Insert(redActionWord);
            float[] oScore = SemanticDR.Reduce(loadGestures.GetWordDictionary()[gesture.pretendObjectName]);
            ReducedWord redObjectWord = new ReducedWord(new Envelope(oScore[0], oScore[1]), gesture.pretendObjectName);
            ObjectDRTree.Insert(redObjectWord);
            float[] gScore = GesturalDR.Reduce(gesture);
            ReducedGesture redGesture = new ReducedGesture(new Envelope(gScore[0], gScore[1]), new Guid(gesture.gestureID));
            GestureDRTree.Insert(redGesture);

            try
            {
                //CHECK
                SurpriseEval.UpdateDistribution(gesture);
            }
            catch (Exception e)
            {
                Debug.Log("ERROR: Could not calculate surprise eval in post localization update");
                Debug.Log(e.Message);
            }

           // Debug.Log("DONE: PostLocalizationUpdate()");
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not update Post Localization");
            Debug.Log(e.Message);
        }
    }

    /// <summary>
    /// Creates candidate actions for the agent to perform.
    /// <returns> A list of suitable candidate actions. </returns>
    /// </summary>
    private async Task<List<GestureDefinition>> GenerateActions(ResponseStrategy[] strategies, GestureDefinition playerGesture)
    {
        try
        {
            //Debug.Log("START: GenerateActions - called in FormResponse");
            List<GestureDefinition> responses = new List<GestureDefinition>();

            // will generate actions using DeepIMAGINATION
            // Debug.Log("Generate actions.");
            foreach (ResponseStrategy strategy in strategies)
            {
                var response = Task.Run(() => strategy.GenerateGestures(playerGesture));

                responses.AddRange(await response);
            }

            foreach (GestureDefinition gesture in responses)
            {
                gesture.coordinateIndex.propName = gesture.propObject.name;
                gesture.coordinateIndex.pretendActionLabel = gesture.pretendActionName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, true);
                gesture.coordinateIndex.pretendObjectLabel = gesture.pretendObjectName = KNNLabel(gesture.coordinateIndex, kForLabeling, weightedDistanceLabeling, false);
            }
            //Debug.Log("DONE: GenerateActions");

            return responses;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not Generate Actions");
            Debug.Log(e.Message);
            return new List<GestureDefinition>();
        }
    }

    /// <summary>
    /// Selects the action whose creative space location best 
    /// matches with the target location along the creative arc
    /// <param name="gestures"> Suitable candidate actions </param>
    /// <returns> The action that's closest to the target location within the creative arc </returns>
    /// </summary>
    private GestureDefinition PickAction(List<GestureDefinition> gestures, float[] playerScores)
    {
        try
        {
            //Debug.Log("START: PickAction - called in FormResponse");
            float[] currentScores;
            //float[] closestScores = { float.MaxValue, float.MaxValue, float.MaxValue };
            Vector3 closestScoresVector = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            GestureDefinition closestGesture = null;
            //float[] currentCurvePoints = currentCurve.GetCurrentPoint();

            //Debug.Log("Novelty Curve: " + currentCurvePoints[0]);
            //Debug.Log("Surprise Curve: " + currentCurvePoints[1]);
            //Debug.Log("Quality Curve: " + currentCurvePoints[2]);

            Vector3 currentCurvePointsVector = new Vector3(currentCurve.GetCurrentPointN(), currentCurve.GetCurrentPointS(), currentCurve.GetCurrentPointV());

            // will evaluate gestures within creative space
            // Debug.Log("Evaluate generated actions and pick one.");
            foreach (GestureDefinition gesture in gestures)
            {
                GestureDefinition trimmedGesture = TrimGesture(gesture, dataType);
                currentScores = FindCreativeSpaceLocation(trimmedGesture);

                Vector3 currentScoresVector = new Vector3(currentScores[0], currentScores[1], currentScores[2]);

                if(IsNegotiating)
                {
                    //Debug.Log("current scores before negotiating - N: " + currentScoresVector.x + ", S: " + currentScoresVector.y + ", V: " + currentScoresVector.z);
                    currentScoresVector = Vector3.Lerp(currentScoresVector, new Vector3(playerScores[0], playerScores[1], playerScores[2]), 0.5f);
                    //Debug.Log("current scores after negotiating - N: " + currentScoresVector.x + ", S: " + currentScoresVector.y + ", V: " + currentScoresVector.z);
                }

                //Vector3 closestScoresVector = new Vector3(closestScores[0], closestScores[1], closestScores[2]);

                //Find euclidian distance between all 3 points
                //Gives all three simulataneously
                if (Vector3.Distance(currentScoresVector, currentCurvePointsVector) < Vector3.Distance(closestScoresVector, currentCurvePointsVector))
                {
                    closestScoresVector = currentScoresVector;
                    closestGesture = gesture;
                }
            }

            currentCurve.SetNextPointIndex();

            //Debug.Log("Closest gesture scores - N: " + closestScoresVector.x + ", S: " + closestScoresVector.y + ", V: " + closestScoresVector.z);

            //Debug.Log("Distance from closest gesture scores to current curve point: " + Vector3.Distance(closestScoresVector, currentCurvePointsVector));

            //Debug.Log("DONE: PickAction");
            return closestGesture;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not pick an action");
            Debug.Log(e.Message);
            return new GestureDefinition(dataType);
        }
    }

    /// <summary>
    /// Sends gesture to action output queue.
    /// </summary>
    /// <param name="response">The gesture to be sent</param>
    private void PartnerPerform(GestureDefinition response)
    {
        try
        {
            //Debug.Log("START: PartnerPerform - 3rd method called in StartDecisionCycle");
            // push into queue
            // Debug.Log("Adding generated response to output queue.");
            OutputQueue.Add(response);
            // Debug.Log("Finished decision cycle and returned gesture with ID: " + response.gestureID);
            busy = false;
            //Debug.Log("DONE: PartnerPerform - Selected ction added to output queue");
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not add response to output queue");
            Debug.Log(e.Message);
        }
    }

    /// <summary>
    /// Returns the next gesture within the queue. Returns NULL if queue is empty.
    /// </summary>
    /// <returns>The next gesture to perform</returns>
    public GestureDefinition ReadFromOutputQueue()
    {
        // Debug.Log("Attempted to read from output queue.");
        GestureDefinition gesture;
        if (OutputQueue.TryTake(out gesture, TimeSpan.FromMilliseconds(1)))
        {
            // Debug.Log("Returned a gesture with ID: " + gesture.gestureID);
            return gesture;
        }
        else
        {
            // Debug.Log("Output queue didn't have a gesture.");
            return null;
        }
    }

    public string KNNLabel(ISpatialData point, int k, bool weightedDistance = false, bool action = false)
    {
        try
        {
            List<ISpatialData> nearestNeighbors = rTree.KNearestNeighbor(point, k);
            string answer = "";
            if (weightedDistance == true)
            {
                var map = new Dictionary<string, double>();
                for (int i = 0; i < nearestNeighbors.Count; i++)
                {
                    GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                    var label = gc.pretendObjectLabel;
                    if (action)
                    {
                        label = gc.pretendActionLabel;
                    }
                    if (map.ContainsKey(label))
                    {
                        map[label] += 1 / (0.00000001 + Envelope.BoxDistance(point.Envelope, gc.Envelope));
                    }
                    else
                    {
                        map.Add(label, 1 / (0.00000001 + Envelope.BoxDistance(point.Envelope, gc.Envelope)));
                    }
                    var mapList = map.ToList();
                    mapList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                    answer = mapList[0].Key;
                }
            }
            else
            {
                List<string> labels = new List<string>();
                for (int i = 0; i < nearestNeighbors.Count; i++)
                {
                    GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                    if (action)
                    {
                        labels.Add(gc.pretendActionLabel);
                    }
                    else
                    {
                        labels.Add(gc.pretendObjectLabel);
                    }
                }
                var labelGroup = labels.GroupBy(x => x);
                var maxCount = labelGroup.Max(g => g.Count());
                var label = labelGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();
                answer = label[0];
            }
            return answer;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: KNNLabeling error");
            Debug.Log(e.Message);
            return "UNKNOWN LABEL";
        }
    }

    /// <summary>
    /// Sets the initial values for the Decision Cycle
    /// </summary>
    public void Restart()
    {
        InputQueue.Dispose();
        OutputQueue.Dispose();
        InputQueue = new BlockingCollection<GestureDefinition>();
        OutputQueue = new BlockingCollection<GestureDefinition>();

        temporalMemory = new TemporalMemory();
        GestureDRTree = new RTree<ReducedGesture>();
        ActionDRTree = new RTree<ReducedWord>();
        ObjectDRTree = new RTree<ReducedWord>();
        rTree = new RTree<GestureCoordinate>();

        Selector = new StrategySelectionController(rTree, temporalMemory);
        CurrentStrategy = 0;

        NoveltyEval = new Novelty(GestureDRTree, ActionDRTree, ObjectDRTree, GesturalDR, SemanticDR, loadGestures.GetWordDictionary());
        SurpriseEval = new Surprise(rTree, GestureDRTree, GesturalDR, ObjectDRTree, ActionDRTree, SemanticDR, loadGestures.GetWordDictionary());
        ValueEval = new Value(rTree);

        busy = false;
        IsRunning = true;
    }

    /// <summary>
    /// Handles the inputs from keyboard
    /// </summary>
    private void InputHandler()
    {
        if (Input.GetKeyDown("r"))
        {
            SwitchStrategies();
        }
        //else if (Input.GetKeyDown("e"))
        //{
        //    TestSetUp();
        //}
        //else if (Input.GetKeyDown("w"))
        //{
        //    TestSetUp2();
        //}
        //else if (Input.GetKeyDown("q"))
        //{
        //    Restart();
        //}
    }

    /// <summary>
    /// Changes the current response strategy to the next one in the list
    /// </summary>
    private void SwitchStrategies()
    {
        CurrentStrategy = (CurrentStrategy + 1) % Selector.Strategies.Length;
        Selector.SetStrategy(CurrentStrategy);
    }

    /// <summary>
    /// Generates a new GUID from system calls.
    /// </summary>
    /// <returns>New Guid</returns>
    private Guid GenerateGuid()
    {
        return Guid.NewGuid();
    }

    /// <summary>
    /// Generates a GUID based on the RecordedGesture's latent space coordinates and timestamp
    /// </summary>
    /// <param name="coordinate">The latent space coordinate</param>
    /// <param name="time">The timestamp of the gesture</param>
    /// <returns>A new GUID</returns>
    public static Guid GenerateGuid(List<float> coordinate, DateTime time)
    {
        try
        {
            byte[] means = FloatToByte(coordinate.GetRange(0, 2).ToArray());
            var timeBytes = BitConverter.GetBytes(time.Ticks);
            byte[] guid = new byte[16];

            for (int i = 0; i < means.Length; i++)
            {
                guid[i] = means[i];
            }

            for (int i = means.Length; i < guid.Length; i++)
            {
                guid[i] = timeBytes[i - means.Length];
            }

            return new Guid(guid);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: Could not GenerateGUID");
            Debug.Log(e.Message);
            return new Guid();
        }
    }

    /// <summary>
    /// Converts a string representation of a date into a DateTime object.
    /// The format of the string must match "yyyy-MM-dd-HH-mm-ss-fff"
    /// </summary>
    /// <param name="timestamp">The string with the proper date/time format</param>
    /// <returns>A DateTime object form of the string</returns>
    private DateTime StringToDateTime(string timestamp)
    {
        string format = "yyyy-MM-dd-HH-mm-ss-fff";
        DateTime time = DateTime.ParseExact(timestamp, format, CultureInfo.InvariantCulture);
        return time;
    }

    /// <summary>
    /// Changes a float array into a byte array
    /// </summary>
    /// <param name="floatArr">The array to change to bytes</param>
    /// <returns>An array of bytes</returns>
    private static byte[] FloatToByte(float[] floatArr)
    {
        byte[] byteArr = new byte[floatArr.Length * 4];
        for (int i = 0; i < floatArr.Length; i++)
        {
            byte[] tempBytes = BitConverter.GetBytes(floatArr[i]);
            byteArr[i * 4 + 0] = tempBytes[0];
            byteArr[i * 4 + 1] = tempBytes[1];
            byteArr[i * 4 + 2] = tempBytes[2];
            byteArr[i * 4 + 3] = tempBytes[3];
        }

        return byteArr;
    }

    /// <summary>
    /// Returns the coordinates represented by the guid
    /// </summary>
    /// <param name="guid">The guid of a GestureCoordinate</param>
    /// <returns>Two floats representing the gesture's latent space coordinates</returns>
    private float[] FindCoordinates(Guid guid)
    {
        byte[] bytes = guid.ToByteArray();
        float m1 = BitConverter.ToSingle(bytes, 0);
        float m2 = BitConverter.ToSingle(bytes, 4);

        return new float[] { m1, m2 };
    }

    private string SetCreativeArc()
    {
        string[] arcTypes = { "rising", "falling", "flat" };
        System.Random random = new System.Random();

        string randomArc = arcTypes[random.Next(arcTypes.Length)];
        currentCurveType = randomArc;
        return currentCurveType;
    }

    public void SetCurrentCurve()
    {
        currentCurve = new CreativeArc(5, SetCreativeArc());
    }

    public void ChangeCurentCurve(string curveType)
    {
        currentCurveType = curveType;
        currentCurve = new CreativeArc(5, curveType);
    }
}
