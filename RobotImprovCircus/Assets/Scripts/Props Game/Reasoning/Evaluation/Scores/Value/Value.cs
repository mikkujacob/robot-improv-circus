﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A numerical representation of an object's value or meaningfulness
public class Value : Score
{
    private RTree<GestureCoordinate> gestureLSDRRTree;

    public double d_s = 0.0;
    public double d_r_po = 0.0;
    public double d_r_pa = 0.0;
    public double d_r = 0.0;
    public double d = 0.0;

    private AdaptiveScaling scalingS;
    private AdaptiveScaling scalingR;

    //SECTION: Parameters

    //Window sizes
    private int windowSizeLocal = 3;
    private int windowSizeRegional = 15;
    private int windowSizeGlobal = 45;
    //Whether average pooling or max pooling (average pooling default)
    public bool isAvgPooling = true;
    //K for nearest neighbors
    private int k_r = 15;//TODO: Set empirically from experimentation.
    //Max values of smoothness and recognizabiltiy scores
    private double maxSmoothnessScore = 20;
    private double maxRecognizabilityScore = 20;
    //Weights for Smoothness and Recognizability score components
    private float weightS = 1.0f;
    private float weightR = 1.0f;

    public Value(RTree<GestureCoordinate> gestureLSDRRTree)
    {
        this.gestureLSDRRTree = gestureLSDRRTree;

        scalingS = new AdaptiveScaling();
        scalingR = new AdaptiveScaling();
    }


    public override float CalculateScore(GestureDefinition gesture)
    {
        try
        {
            float smoothnessScore = CalculateSmoothnessScore(gesture);
            float recognizabilityScore = CalculateRecognizabilityScore(gesture);

            d = (smoothnessScore + recognizabilityScore) / 2.0;

            return (scalingS.ScaleValue(smoothnessScore * weightS) + scalingR.ScaleValue(recognizabilityScore * weightR)) / 2.0f;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return 0;
        }
    }


    public float CalculateSmoothnessScore(GestureDefinition gesture)
    {
        try
        {
            //Calculate jerk over window with small size
            double[][] jerksLocal = CalculateGestureJerks(gesture, windowSizeLocal, isAvgPooling);
            //Calculate jerk over window with medium size
            double[][] jerksRegional = CalculateGestureJerks(gesture, windowSizeRegional, isAvgPooling);
            //Calculate jerk over window with large size
            double[][] jerksGlobal = CalculateGestureJerks(gesture, windowSizeGlobal, isAvgPooling);

            //Aggregate jerk values for three window sizes
            double localAvg = CalculateAbsAvg(jerksLocal);
            double regionalAvg = CalculateAbsAvg(jerksRegional);
            double globalAvg = CalculateAbsAvg(jerksGlobal);
            //double localSum = CalculateAbsSum(jerksLocal);
            //double regionalSum = CalculateAbsSum(jerksRegional);
            //double globalSum = CalculateAbsSum(jerksGlobal);

            //Debug.Log("Local Avg: " + localAvg + ", Regional Avg: " + regionalAvg + ", Global Avg: " + globalAvg);
            //Debug.Log("Local Sum: " + localSum + ", Regional Sum: " + regionalSum + ", Global Sum: " + globalSum);

            //Average aggregate jerk values across three window sizes
            double scaleAvgJerkAvgs = (localAvg + regionalAvg + globalAvg) / 3.0;
            //double scaleAvgJerkSums = (localSum + regionalSum + globalSum) / 3.0;
            //double scaleSumJerkSums = localSum + regionalSum + globalSum;

            //Debug.Log("Scale Average: " + scaleAvgJerk);
            //Debug.Log("Scale Sum: " + scaleSumJerk);

            //Inverse function to use for sum: y = |maxValue/(x + 1)|
            double smoothnessScore = Math.Abs(maxSmoothnessScore / (scaleAvgJerkAvgs + 1));
            //double smoothnessScore = Math.Abs(maxSmoothnessScore / (scaleSumJerk + 1));

            d_s = smoothnessScore;

            //Debug.Log("D_S_G: " + smoothnessScore);

            return (float)smoothnessScore;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return 0;
        }
    }

    private double CalculateAbsAvg(double[][] arrayArrays)
    {
        try
        {
            int totalCount = arrayArrays.GetLength(0) * arrayArrays[0].GetLength(0);
            return CalculateAbsSum(arrayArrays) / totalCount;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return 0;
        }
    }

    private double CalculateAbsSum(double[][] arrayArrays)
    {
        double sum = 0.0;
        foreach (double[] array in arrayArrays)
        {
            foreach (double value in array)
            {
                sum += Math.Abs(value);
            }
        }
        return sum;
    }

    private double[][] CalculateGestureJerks(GestureDefinition gesture, int windowSize, bool isAvgPool)
    {
        try
        {
            GestureVector gv = new GestureVector(gesture, gesture.dataType);
            double[][] jointArrays = GetJointArrays(gv);
            //double[][] pooledArrays = new double[jointArrays.GetLength(0)][];
            double[][] jerksArrays = new double[jointArrays.GetLength(0)][];
            for (int i = 0; i < jointArrays.GetLength(0); i++)
            {
                //pooledArrays[i] = AbsMaxPoolArray(jointArrays[i], windowSize);
                //jerksArrays[i] = CalculateArrayJerks(pooledArrays[i]);
                if (!isAvgPool)
                {
                    jerksArrays[i] = CalculateArrayJerks(AbsMaxPoolArray(jointArrays[i], windowSize));
                }
                else
                {
                    jerksArrays[i] = CalculateArrayJerks(AvgPoolArray(jointArrays[i], windowSize));
                }
            }

            return jerksArrays;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return new double[35][];
        }
    }

    private double[][] GetJointArrays(GestureVector gv)
    {
        try
        {
            int paddingIndex = gv.list.Count - gv.paddingCount - 1;
            for (; paddingIndex >= 0; paddingIndex -= gv.frameSize)
            {
                if (gv.list[paddingIndex] > 0)
                {
                    break;
                }
            }
            int jointArrayLength = (int)Math.Ceiling((double)paddingIndex / (double)gv.frameSize + 1.0);
            double[][] jointArrays = new double[gv.frameSize][];
            for (int i = 0; i < gv.frameSize; i++)
            {
                jointArrays[i] = new double[jointArrayLength];
            }

            for (int arrayIndex = 0; arrayIndex < jointArrayLength; arrayIndex++)
            {
                for (int frameIndex = 0; frameIndex < gv.frameSize; frameIndex++)
                {
                    int vectorIndex = arrayIndex * gv.frameSize + frameIndex;
                    jointArrays[frameIndex][arrayIndex] = gv.list[vectorIndex];
                }
            }

            return jointArrays;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            double[][] jointArrays = new double[gv.frameSize][];
            for (int i = 0; i < gv.frameSize; i++)
            {
                jointArrays[i] = new double[450];
            }
            return jointArrays;
        }
    }

    private double[] AbsMaxPoolArray(double[] array, int windowSize)
    {
        double[] source = (double[])array.Clone();
        double[] target = new double[(int)Math.Ceiling((double)source.Length / (double)windowSize)];

        for(int targetIndex = 0; targetIndex < target.Length; targetIndex++)
        {
            //Start by considering first element as max;
            int absMaxIndex = targetIndex * windowSize + 0;
            for(int windowIndex = 1; windowIndex < windowSize; windowIndex++)
            {
                int sourceIndex = targetIndex * windowSize + windowIndex;
                if(sourceIndex >= source.Length)
                {
                    break;
                }
                absMaxIndex = (Math.Abs(source[absMaxIndex]) < Math.Abs(source[sourceIndex])) ? sourceIndex : absMaxIndex;
            }
            target[targetIndex] = source[absMaxIndex];
        }

        return target;
    }

    private double[] AvgPoolArray(double[] array, int windowSize)
    {
        try
        {
            double[] source = (double[])array.Clone();
            double[] target = new double[(int)Math.Ceiling((double)source.Length / (double)windowSize)];

            for (int targetIndex = 0; targetIndex < target.Length; targetIndex++)
            {
                //Start by considering first element as max;
                //int absMaxIndex = targetIndex * windowSize + 0;
                double sum = source[targetIndex * windowSize + 0];
                for (int windowIndex = 1; windowIndex < windowSize; windowIndex++)
                {
                    int sourceIndex = targetIndex * windowSize + windowIndex;
                    if (sourceIndex >= source.Length)
                    {
                        break;
                    }
                    //absMaxIndex = (Math.Abs(source[absMaxIndex]) < Math.Abs(source[sourceIndex])) ? sourceIndex : absMaxIndex;
                    sum += source[sourceIndex];
                }
                target[targetIndex] = sum / windowSize;
            }

            return target;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return new double[(int)Math.Ceiling((double)array.Length / (double)windowSize)];
        }
    }

    private double[] CalculateArrayJerks(double[] array)
    {
        return CalculateArrayDerivatives(array, 3);
    }

    private double[] CalculateArrayDerivatives(double[] array, int nthDerivative)
    {
        try
        {
            //Debug.Log("Array Derivatives: original array length: " + array.Length);
            double[] derivativeArraySource = (double[])array.Clone();
            //Debug.Log("Cloned source");
            double[] derivativeArrayTarget = new double[derivativeArraySource.Length - 1];
            //Debug.Log("Init target");
            for (int ithderivative = 0; ithderivative < nthDerivative; ithderivative++)
            {
                //Debug.Log("derivativeArraySource length: " + derivativeArraySource.Length + " derivative #: " + ithderivative);
                for (int i = 0; i < derivativeArraySource.Length - 1; i++)
                {
                    //if ((i + 1) == derivativeArraySource.Length || i == derivativeArraySource.Length)
                    //{
                    //    Debug.Log("ERROR: i: " + i + "i-1: " + (i - 1) + "i+1: " + (i + 1));
                    //}
                    derivativeArrayTarget[i] = derivativeArraySource[i + 1] - derivativeArraySource[i];
                }
                derivativeArraySource = (double[])derivativeArrayTarget.Clone();
                //Debug.Log("Cloned target");
                derivativeArrayTarget = new double[derivativeArraySource.Length - 1];
                //Debug.Log("derivativeArrayTarget length: " + derivativeArrayTarget.Length);
            }
            return derivativeArraySource;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return (double[])array.Clone();
        }
    }

    //
    public float CalculateRecognizabilityScore(GestureDefinition gesture)
    {
        try
        {
            //Find ratio of average distance between point with gestures having same semantic labels and between point with gestures hacing different semantic labels (ratio of average intra vs. inter class distance)
            List<KeyValuePair<string[], double>> nearest = GetKNNLabelOptions(gesture.coordinateIndex, k_r);

            double sameObjectDistanceSum = 0;
            double sameActionDistanceSum = 0;
            int sameObjectCount = 0;
            int sameActionCount = 0;
            double otherObjectDistanceSum = 0;
            double otherActionDistanceSum = 0;
            int otherObjectCount = 0;
            int otherActionCount = 0;

            for (int i = 0; i < nearest.Count; i++)
            {
                if (gesture.pretendObjectName == nearest[i].Key[0])
                {
                    sameObjectDistanceSum += nearest[i].Value;
                    sameObjectCount++;
                }
                else
                {
                    otherObjectDistanceSum += nearest[i].Value;
                    otherObjectCount++;
                }

                if (gesture.pretendActionName == nearest[i].Key[1])
                {
                    sameActionDistanceSum += nearest[i].Value;
                    sameActionCount++;
                }
                else
                {
                    otherActionDistanceSum += nearest[i].Value;
                    otherActionCount++;
                }
            }

            double objectAvgIntraClassDistance = (sameObjectCount > 0) ? sameObjectDistanceSum / sameObjectCount : double.MaxValue;
            double objectAvgInterClassDistance = (otherObjectCount > 0) ? otherObjectDistanceSum / otherObjectCount : double.MaxValue;

            double actionAvgIntraClassDistance = (sameActionCount > 0) ? sameActionDistanceSum / sameActionCount : double.MaxValue;
            double actionAvgInterClassDistance = (otherActionCount > 0) ? otherActionDistanceSum / otherActionCount : double.MaxValue;

            double objectRecognizabilityInverse = Math.Abs(objectAvgIntraClassDistance / (objectAvgInterClassDistance + 1));
            double actionRecognizabilityInverse = Math.Abs(actionAvgIntraClassDistance / (actionAvgInterClassDistance + 1));

            double objectRecognizabilityScore = maxRecognizabilityScore / (objectRecognizabilityInverse + 1);
            double actionRecognizabilityScore = maxRecognizabilityScore / (actionRecognizabilityInverse + 1);

            d_r_po = objectRecognizabilityScore;
            d_r_pa = actionRecognizabilityScore;

            d_r = objectRecognizabilityScore + actionRecognizabilityScore;

            //Debug.Log("D_R_O: " + objectRecognizabilityScore + ", D_R_A: " + actionRecognizabilityScore);

            return (float)(objectRecognizabilityScore + actionRecognizabilityScore);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return 0;
        }
    }

    public List<KeyValuePair<string[], double>> GetKNNLabelOptions(ISpatialData point, int k)
    {
        try
        {
            List<ISpatialData> nearestNeighborsO = gestureLSDRRTree.KNearestNeighbor(point, k);
            var map = new Dictionary<string[], double>();
            List<KeyValuePair<string[], double>> resultList = new List<KeyValuePair<string[], double>>();
            for (int i = 0; i < nearestNeighborsO.Count; i++)
            {
                GestureCoordinate gc = (GestureCoordinate)nearestNeighborsO[i];
                string[] label = new string[] { gc.pretendObjectLabel, gc.pretendActionLabel, gc.propName };
                double distance = Envelope.BoxDistance(point.Envelope, gc.Envelope);
                resultList.Add(new KeyValuePair<string[], double>(label, distance));
            }

            resultList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
            return resultList;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            return new List<KeyValuePair<string[], double>>();
        }
    }
}
