﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Representation of the gesture after it has had its dimensionality reduced
/// </summary>
public class ReducedGesture : ISpatialData
{
    /// <summary>
    /// The reduced dimensionality of the gesture
    /// </summary>
    public Envelope Envelope { get; set; }

    /// <summary>
    /// The unique ID of the gesture. This Guid should correspond to the actual gesture's Guid
    /// </summary>
    public Guid Guid;

    public ReducedGesture() : this(new Envelope()) { }

    public ReducedGesture(Envelope env) : this(env, Guid.NewGuid()) { }

    public ReducedGesture(Envelope env, Guid g)
    {
        this.Envelope = env;
        this.Guid = g;
    }
}
