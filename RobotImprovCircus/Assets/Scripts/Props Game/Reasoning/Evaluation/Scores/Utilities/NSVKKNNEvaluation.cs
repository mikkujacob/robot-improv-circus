﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NSVKKNNEvaluation : MonoBehaviour
{
    public bool isInit = false;
    private static bool KeepRunning = true;
    private static bool RunDebugMessageQueue = true;
    private static TextWriter DebugMessageWriter = null;

    private RTree<GestureCoordinate> GestureLSDRTree;
    private RTree<ReducedGesture> GestureDRTree;
    private RTree<ReducedWord> ActionDRTree;
    private RTree<ReducedWord> ObjectDRTree;
    private GesturalDimRed GesturalDR;
    private SemanticDimRed SemanticDR;
    private FrozenGraphLoader model;
    public int GesturesLoaded { get; private set; } = 0;

    private static BlockingCollection<string> DebugMessageQueue = new BlockingCollection<string>();

    private Novelty NoveltyEval;
    private Surprise SurpriseEval;
    private Value ValueEval;

    private static Dictionary<string, float[]> WordVectorLookup = new Dictionary<string, float[]>();
    private string WordVectorFileName = "/word_dict_cc.txt";

    private List<GestureDefinition> gestures;

    private List<Dictionary<string, double>> noveltyScores;
    private List<Dictionary<string, double>> surpriseScores;
    private List<Dictionary<string, double>> valueScores;
    private List<Dictionary<string, double>> KNNLabelingScores;
    private Dictionary<int, double[]> KGScores;
    private Dictionary<int, double[]> KSPOScores;
    private Dictionary<int, double[]> KSPAScores;

    private int DerivativeOrder = 4;

    private static string streamingAssetsPath;
    private static string debugWriterFile = "/NSVKKNNEvaluation/DebugOutput-" + (DateTime.Now.ToString() + ".log").Replace("/", "-").Replace(":", "-").Replace(" ", "-");
    private string resultsNFile = "/NSVKKNNEvaluation/NoveltyResults.csv";
    private string resultsSFile = "/NSVKKNNEvaluation/SurpriseResults.csv";
    private string resultsVFile = "/NSVKKNNEvaluation/ValueResults.csv";
    private string resultsKGFile = "/NSVKKNNEvaluation/KGestureResults.csv";
    private string resultsKSPOFile = "/NSVKKNNEvaluation/KSemanticPretendObjectsResults.csv";
    private string resultsKSPAFile = "/NSVKKNNEvaluation/KSemanticPretendActionsResults.csv";
    private string resultsKNNFile = "/NSVKKNNEvaluation/KNNLabelingResults.csv";

    // Use this for initialization
    public void Start()
    {
        streamingAssetsPath = Application.streamingAssetsPath;

        StartCoroutine(WriteDebugMessageToFile());

        StartCoroutine(LoadNSVKKNNEvaluation());
    }

    private static IEnumerator WriteDebugMessageToFile()
    {
        if(DebugMessageWriter == null)
        {
            DebugMessageWriter = new StreamWriter(streamingAssetsPath + debugWriterFile);
        }
        while (RunDebugMessageQueue)
        {
            string message;
            bool success = DebugMessageQueue.TryTake(out message);
            if(success)
            {
                DebugMessageWriter.WriteLine(DateTime.Now.ToString() + ":" + message);
                Debug.Log(DateTime.Now.ToString() + ":" + message);
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public IEnumerator LoadNSVKKNNEvaluation()
    {
        //Giving DeepIMAGINATIONInterface and other Model Interfaces time load and initialize. 
        yield return new WaitForSeconds(2f);

        model = FindObjectOfType<DeepIMAGINATIONInterface>().GetModel();
        GesturalDR = FindObjectOfType<GesturalDimRed>();
        SemanticDR = FindObjectOfType<SemanticDimRed>();

        GestureDRTree = new RTree<ReducedGesture>();
        ActionDRTree = new RTree<ReducedWord>();
        ObjectDRTree = new RTree<ReducedWord>();
        GestureLSDRTree = new RTree<GestureCoordinate>();

        LoadWordVectorDict();

        gestures = new List<GestureDefinition>();

        noveltyScores = new List<Dictionary<string, double>>();
        surpriseScores = new List<Dictionary<string, double>>();
        valueScores = new List<Dictionary<string, double>>();
        KNNLabelingScores = new List<Dictionary<string, double>>();
        KGScores = new Dictionary<int, double[]>();
        KSPOScores = new Dictionary<int, double[]>();
        KSPAScores = new Dictionary<int, double[]>();

        // Look in StreamingAssets for
        if (Directory.Exists(streamingAssetsPath))
        {
            // timeseries .timeseries file
            // affordance .labels file
            // pretend action .labels file
            // pretend object .labels file
            if (Directory.GetFiles(streamingAssetsPath, "*.labels").Length >= 3 && Directory.GetFiles(streamingAssetsPath, "*.timeseries").Length >= 1)
            {
                // Load gestures from timeseries and labels file
                StreamReader timeseriesReader = null;
                StreamReader labelsReader = null;
                StreamReader pretendObjectReader = null;
                StreamReader pretendActionReader = null;

                if (Executive.dataType == DataEnum.Data27000)
                {
                    timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-27000.timeseries");
                    labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
                    pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
                    pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");
                }
                else if (Executive.dataType == DataEnum.Data16000)
                {
                    timeseriesReader = new StreamReader(streamingAssetsPath + "/timeseries-16000.timeseries");
                    labelsReader = new StreamReader(streamingAssetsPath + "/affordance-labels.labels");
                    pretendObjectReader = new StreamReader(streamingAssetsPath + "/pretend-object-labels.labels");
                    pretendActionReader = new StreamReader(streamingAssetsPath + "/pretend-action-labels.labels");
                }

                float[,] param;
                float[] affordance;
                List<float> vals;

                int filesloaded = 0; //TODO: REMOVE AFTER TESTING!
                int filestoload = 973; //Swap with next commented line during final work 
                //int filestoload = 50; //TODO: Comment when done testing
                string pretendObjectLabel;
                while ((pretendObjectLabel = pretendObjectReader.ReadLine()) != null && filesloaded < filestoload) //TODO: REMOVE " && filesloaded < filestoload" CONDITION AFTER TESTING!
                {
                    float[] inputTimeSeries = Array.ConvertAll<string, float>(timeseriesReader.ReadLine().Split(','), StringToFloat);
                    affordance = Array.ConvertAll<string, float>(labelsReader.ReadLine().Split(','), StringToFloat);

                    //string pretendObjectLabel = pretendObjectReader.ReadLine();
                    string pretendActionLabel = pretendActionReader.ReadLine();
                    //pretendActionLabel = pretendActionLabel.Substring(0, pretendActionLabel.IndexOf("."));

                    param = new float[1, inputTimeSeries.Length + affordance.Length];
                    float[] values = Concatenate1DArrays<float>(inputTimeSeries, affordance);

                    for (int i = 0; i < values.Length; i++)
                    {
                        param[0, i] = values[i];
                    }

                    Dictionary<string, dynamic> generatorInputs = new Dictionary<string, dynamic>()
                            {
                                { "concat", param },
                                { "keep_prob", 1.0f }
                            };

                    // Generating output inference
                    float[,] outputMeans = model.GenerateOutput(generatorInputs, "encoder/dense/BiasAdd") as float[,];
                    //DebugMessageQueue.Add("Result successfully obtained. Results in form: " + outputMeans.GetLength(0) + "x" + outputMeans.GetLength(1));
                    float[,] outputSTDevs = model.GenerateOutput(generatorInputs, "encoder/dense_1/BiasAdd") as float[,];
                    //DebugMessageQueue.Add("Result successfully obtained. Results in form: " + outputSTDevs.GetLength(0) + "x" + outputSTDevs.GetLength(1));

                    // Finding the input's latent space value
                    vals = new List<float>();
                    foreach (float val in outputMeans)
                    {
                        vals.Add(val);
                    }
                    foreach (float val in outputSTDevs)
                    {
                        vals.Add(val);
                    }
                    vals.AddRange(affordance);

                    var affordanceKey = affordance;
                    //DebugMessageQueue.Add("KEY:  " + toStringFloat(affordanceKey));
                    float[,] paramjointdata = new float[1, inputTimeSeries.Length];

                    for (int i = 0; i < inputTimeSeries.Length; i++)
                    {
                        paramjointdata[0, i] = inputTimeSeries[i];
                    }

                    string propName = propAffordanceNameLookup[toStringFloat(affordanceKey)];
                    if (propName.Equals("Squid w Tentacles Partless OR Horseshoe"))
                    {
                        if (UnityEngine.Random.value <= 0.5F)
                        {
                            propName = "Squid w Tentacles Partless";
                        }
                        else
                        {
                            propName = "Horseshoe";
                        }
                    }
                    GestureDefinition gesture = new GestureDefinition(paramjointdata, propName, true, 0, Executive.dataType);
                    //gesture.GetGesture().objectName = propAffordanceNameLookup[toStringFloat(affordanceKey)];
                    //gesture.GetGesture().objectSize = propAffordanceNameLookup[toStringFloat(affordanceKey)];

                    gesture.pretendActionName = pretendActionLabel;
                    gesture.pretendObjectName = pretendObjectLabel;

                    //var guid = System.Guid.NewGuid();
                    var guid = Executive.GenerateGuid(vals, System.DateTime.Now);
                    gesture.gestureID = guid.ToString();

                    GestureCoordinate gestureCoordinate = new GestureCoordinate(new Envelope(vals[0], vals[1]), guid, pretendActionLabel, pretendObjectLabel, propName);

                    gesture.coordinateIndex = gestureCoordinate;

                    // insert into RTree
                    GestureLSDRTree.Insert(gestureCoordinate);

                    // insert into Reduced Gestural Dimensionality RTree
                    float[] gestureDRVector = GesturalDR.Reduce(inputTimeSeries);
                    ReducedGesture redGesture = new ReducedGesture(new Envelope(gestureDRVector[0], gestureDRVector[1]), guid);
                    GestureDRTree.Insert(redGesture);

                    // insert into Reduced Object Dimensionality RTree
                    if (WordVectorLookup.ContainsKey(pretendObjectLabel))
                    {
                        float[] objectDRVector = SemanticDR.Reduce(WordVectorLookup[pretendObjectLabel]);
                        ReducedWord redObjectWord = new ReducedWord(new Envelope(objectDRVector[0], objectDRVector[1]), pretendObjectLabel);
                        ObjectDRTree.Insert(redObjectWord);
                    }

                    // insert into Reduced Action Dimensionality RTree
                    if (WordVectorLookup.ContainsKey(pretendActionLabel))
                    {
                        float[] actionDRVector = SemanticDR.Reduce(WordVectorLookup[pretendActionLabel]);
                        ReducedWord redActionWord = new ReducedWord(new Envelope(actionDRVector[0], actionDRVector[1]), pretendActionLabel);
                        ActionDRTree.Insert(redActionWord);
                    }

                    gestures.Add(gesture);

                    GesturesLoaded += 1;

                    filesloaded++; //TODO: REMOVE AFTER TESTING!

                    if(filesloaded % 20 == 0)
                    {
                        DebugMessageQueue.Add("Gestures Loaded: " + (((float)filesloaded / (float)filestoload) * 100f) + "%");
                    }

                    yield return null;
                }
            }
            else
            {
                throw new Exception("No gestures files exists in StreamingAssets Folder");
            }

            DebugMessageQueue.Add("Gestures Loaded: 100%");
        }

        NoveltyEval = new Novelty(GestureDRTree, ActionDRTree, ObjectDRTree, GesturalDR, SemanticDR, WordVectorLookup);
        SurpriseEval = new Surprise(GestureLSDRTree, GestureDRTree, GesturalDR, ObjectDRTree, ActionDRTree, SemanticDR, WordVectorLookup);
        ValueEval = new Value(GestureLSDRTree);

        isInit = true;

        DebugMessageQueue.Add("Loaded NSVKKNN Evaluation Script.");
    }

    // Update is called once per frame
#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
    public async void Update()
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
    {
        if (isInit && Input.GetKeyDown(KeyCode.Return))
        {
            DebugMessageQueue.Add("Starting NSV Evaluation.");
            isInit = false;
            await EvaluateNSVKKNN();
            isInit = true;
        }
    }

    private async Task EvaluateNSVKKNN()
    {
        DebugMessageQueue.Add("Evaluating KNN");
        Task KNN = EvaluateKNNLabeling();

        DebugMessageQueue.Add("Evaluating K");
        Task K = EvaluateK();

        DebugMessageQueue.Add("Evaluating NSV");
        Task NSV = EvaluateNSV();

        await Task.WhenAll(new Task[] { KNN, K, NSV });
    }

    private async Task EvaluateNSV()
    {
        List<Task> tasks = new List<Task>();

        DebugMessageQueue.Add("Round One With Updation.");
        foreach (GestureDefinition gesture in gestures)
        {
            if(!KeepRunning)
            {
                break;
            }
            tasks.Add(Task.Run(() => EvaluateN(gesture)));
            tasks.Add(Task.Run(() => EvaluateS(gesture)));
            tasks.Add(Task.Run(() => EvaluateV(gesture)));

            await Task.WhenAll(tasks);

            UpdateAll(gesture);

            tasks.Clear();
        }

        DebugMessageQueue.Add("Round Two Without Updation.");
        foreach (GestureDefinition gesture in gestures)
        {
            if (!KeepRunning)
            {
                break;
            }
            tasks.Add(Task.Run(() => EvaluateN(gesture)));
            tasks.Add(Task.Run(() => EvaluateS(gesture)));
            tasks.Add(Task.Run(() => EvaluateV(gesture)));

            await Task.WhenAll(tasks);

            tasks.Clear();
        }

        DebugMessageQueue.Add("Writing NSV Results To File.");
        RecordResults(resultsNFile, gestures, noveltyScores);
        RecordResults(resultsSFile, gestures, surpriseScores);
        RecordResults(resultsVFile, gestures, valueScores);
    }

    private void EvaluateN(GestureDefinition gesture)
    {
        try
        {
            //DebugMessageQueue.Add("Evaluating N");
            float noveltyScore = NoveltyEval.CalculateScore(gesture);

            Dictionary<string, double> nScores = new Dictionary<string, double>();
            nScores.Add("NScore", noveltyScore);
            nScores.Add("D_G", NoveltyEval.d_g);
            nScores.Add("D_S", NoveltyEval.d_s);
            nScores.Add("D_S_PO", NoveltyEval.d_s_po);
            nScores.Add("D_S_PA", NoveltyEval.d_s_pa);
            nScores.Add("D", NoveltyEval.d);

            noveltyScores.Add(nScores);
        }
        catch(Exception e)
        {
            DebugMessageQueue.Add("Exception in EvaluateN(): " + e.Message);
            //Handle exception.

            Dictionary<string, double> nScores = new Dictionary<string, double>();
            nScores.Add("NScore", Double.NaN);
            nScores.Add("D_G", Double.NaN);
            nScores.Add("D_S", Double.NaN);
            nScores.Add("D_S_PO", Double.NaN);
            nScores.Add("D_S_PA", Double.NaN);
            nScores.Add("D", Double.NaN);

            noveltyScores.Add(nScores);
        }
    }

    private void EvaluateS(GestureDefinition gesture)
    {
        try
        {
            //DebugMessageQueue.Add("Evaluating S");
            float surpriseScore = SurpriseEval.CalculateScore(gesture);

            Dictionary<string, double> sScores = new Dictionary<string, double>();
            sScores.Add("SScore", surpriseScore);
            sScores.Add("D_DFE_G", SurpriseEval.d_dfe_g);
            sScores.Add("D_DFE_PO", SurpriseEval.d_dfe_po);
            sScores.Add("D_DFE_PA", SurpriseEval.d_dfe_pa);
            sScores.Add("D_DFE", SurpriseEval.d_dfe);
            sScores.Add("D_BS_G", SurpriseEval.d_bs_g);
            sScores.Add("D_BS_PO", SurpriseEval.d_bs_po);
            sScores.Add("D_BS_PA", SurpriseEval.d_bs_pa);
            sScores.Add("D_BS", SurpriseEval.d_bs);
            sScores.Add("D", SurpriseEval.d);

            surpriseScores.Add(sScores);
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in EvaluateS(): " + e.Message);
            //Handle exception.

            Dictionary<string, double> sScores = new Dictionary<string, double>();
            sScores.Add("SScore", Double.NaN);
            sScores.Add("D_DFE_G", Double.NaN);
            sScores.Add("D_DFE_PO", Double.NaN);
            sScores.Add("D_DFE_PA", Double.NaN);
            sScores.Add("D_DFE", Double.NaN);
            sScores.Add("D_BS_G", Double.NaN);
            sScores.Add("D_BS_PO", Double.NaN);
            sScores.Add("D_BS_PA", Double.NaN);
            sScores.Add("D_BS", Double.NaN);
            sScores.Add("D", Double.NaN);

            surpriseScores.Add(sScores);
        }
    }

    private void EvaluateV(GestureDefinition gesture)
    {
        try
        {
            //DebugMessageQueue.Add("Evaluating V");
            float valueScore = ValueEval.CalculateScore(gesture);

            Dictionary<string, double> vScores = new Dictionary<string, double>();
            vScores.Add("VScore", valueScore);
            vScores.Add("D_S", ValueEval.d_s);
            vScores.Add("D_R_PO", ValueEval.d_r_po);
            vScores.Add("D_R_PA", ValueEval.d_r_pa);
            vScores.Add("D_R", ValueEval.d_r);
            vScores.Add("D", ValueEval.d);

            valueScores.Add(vScores);
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in EvaluateV(): " + e.Message);
            //Handle exception.

            Dictionary<string, double> vScores = new Dictionary<string, double>();
            vScores.Add("VScore", Double.NaN);
            vScores.Add("D_S", Double.NaN);
            vScores.Add("D_R_PO", Double.NaN);
            vScores.Add("D_R_PA", Double.NaN);
            vScores.Add("D_R", Double.NaN);
            vScores.Add("D", Double.NaN);

            valueScores.Add(vScores);
        }
    }

    private async Task EvaluateK()
    {
        DebugMessageQueue.Add("Evaluating KG");
        var KG = EvaluateKG();

        DebugMessageQueue.Add("Evaluating KSPO");
        var KSPO = EvaluateKSPO();

        DebugMessageQueue.Add("Evaluating KSPA");
        var KSPA = EvaluateKSPA();

        await Task.WhenAll(new Task[] { KG, KSPO, KSPA });
    }

    private async Task EvaluateKG()
    {
        await FindMeansOfMeansForK(KGScores, 1);
        FindVelocity(KGScores);
        FindAcceleration(KGScores);
        FindJerk(KGScores);
        DebugMessageQueue.Add("Writing KG Results To File.");
        RecordResults(resultsKGFile, KGScores);
    }

    private async Task EvaluateKSPO()
    {
        await FindMeansOfMeansForK(KSPOScores, 2);
        FindVelocity(KSPOScores);
        FindAcceleration(KSPOScores);
        FindJerk(KSPOScores);
        DebugMessageQueue.Add("Writing KSPO Results To File.");
        RecordResults(resultsKSPOFile, KSPOScores);
    }

    private async Task EvaluateKSPA()
    {
        await FindMeansOfMeansForK(KSPAScores, 3);
        FindVelocity(KSPAScores);
        FindAcceleration(KSPAScores);
        FindJerk(KSPAScores);
        DebugMessageQueue.Add("Writing KSPA Results To File.");
        RecordResults(resultsKSPAFile, KSPAScores);
    }

    private async Task EvaluateKNNLabeling()
    {
        for (int k = 2; k < gestures.Count / 2; k++)
        {
            if (!KeepRunning)
            {
                break;
            }
            await Task.Run(() => EvaluateKNNLabelingForK(k));
        }

        DebugMessageQueue.Add("Writing KNN Results To File.");
        RecordResults(resultsKNNFile, KNNLabelingScores);
    }

    private void EvaluateKNNLabelingForK(int k)
    {
        try
        {
            DebugMessageQueue.Add("Finding KNN Accuracy For K: " + k);
            double avgAccuracyKOU = 0.0;
            double avgAccuracyKOW = 0.0;
            double avgAccuracyKAU = 0.0;
            double avgAccuracyKAW = 0.0;

            foreach (GestureDefinition gesture in gestures)
            {
                if (!KeepRunning)
                {
                    break;
                }
                string pretendObjectUnweighted = KNNLabel(gesture.coordinateIndex, k, false, false);
                avgAccuracyKOU += (pretendObjectUnweighted == gesture.pretendObjectName) ? 1 : 0;
                string pretendObjectWeighted = KNNLabel(gesture.coordinateIndex, k, true, false);
                avgAccuracyKOW += (pretendObjectWeighted == gesture.pretendObjectName) ? 1 : 0;
                string pretendActionUnweighted = KNNLabel(gesture.coordinateIndex, k, false, true);
                avgAccuracyKAU += (pretendActionUnweighted == gesture.pretendActionName) ? 1 : 0;
                string pretendActionWeighted = KNNLabel(gesture.coordinateIndex, k, true, true);
                avgAccuracyKAW += (pretendActionWeighted == gesture.pretendActionName) ? 1 : 0;
            }

            avgAccuracyKOU /= gestures.Count;
            avgAccuracyKOW /= gestures.Count;
            avgAccuracyKAU /= gestures.Count;
            avgAccuracyKAW /= gestures.Count;

            Dictionary<string, double> KNNScores = new Dictionary<string, double>();
            KNNScores.Add("avgAccuracyKOU", avgAccuracyKOU);
            KNNScores.Add("avgAccuracyKOW", avgAccuracyKOW);
            KNNScores.Add("avgAccuracyKAU", avgAccuracyKAU);
            KNNScores.Add("avgAccuracyKAW", avgAccuracyKAW);

            KNNLabelingScores.Add(KNNScores);
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in EvaluateKNNLabelingForK(int k): " + e.Message);
            //Handle exception.

            Dictionary<string, double> KNNScores = new Dictionary<string, double>();
            KNNScores.Add("avgAccuracyKOU", Double.NaN);
            KNNScores.Add("avgAccuracyKOW", Double.NaN);
            KNNScores.Add("avgAccuracyKAU", Double.NaN);
            KNNScores.Add("avgAccuracyKAW", Double.NaN);

            KNNLabelingScores.Add(KNNScores);
        }
    }

    private List<ISpatialData> GetGestureDRTreeContents()
    {
        return GestureDRTree.KNearestNeighbor(GestureDRTree.root, GestureDRTree.Count);
    }

    private List<ISpatialData> GetSemanticPODRTreeContents()
    {
        return ObjectDRTree.KNearestNeighbor(ObjectDRTree.root, ObjectDRTree.Count);
    }

    private List<ISpatialData> GetSemanticPADRTreeContents()
    {
        return ActionDRTree.KNearestNeighbor(ActionDRTree.root, ActionDRTree.Count);
    }

    private List<ISpatialData> GetGestureDRTreeKNN(ISpatialData point, int k)
    {
        return GestureDRTree.KNearestNeighbor(point, k);
    }

    private List<ISpatialData> GetSemanticPODRTreeKNN(ISpatialData point, int k)
    {
        return ObjectDRTree.KNearestNeighbor(point, k);
    }

    private List<ISpatialData> GetSemanticPADRTreeKNN(ISpatialData point, int k)
    {
        return ActionDRTree.KNearestNeighbor(point, k);
    }

    private async Task FindMeansOfMeansForK(Dictionary<int, double[]> KScores, int mode)
    {
        List<ISpatialData> data = null;
        switch(mode)
        {
            case 1:
                {
                    data = GetGestureDRTreeContents();
                    break;
                }

            case 2:
                {
                    data = GetSemanticPODRTreeContents();
                    break;
                }
            case 3:
                {
                    data = GetSemanticPADRTreeContents();
                    break;
                }
        }

        for (int k = 2; k < data.Count / 2; k++)
        {
            if (!KeepRunning)
            {
                break;
            }
            double meanForK = await FindMeansOfMeans(k, data, mode);
            // x: k
            // y: means of means
            double[] derivatives = new double[DerivativeOrder];
            derivatives[0] = meanForK;
            KScores.Add(k, derivatives);
            DebugMessageQueue.Add("Found means of means for " + k + ": " + meanForK);
        }

        //DebugMessageQueue.Add("Done calculating means of means for k!");
    }

    private async Task<double> FindMeansOfMeans(int k, List<ISpatialData> data, int mode)
    {
        try
        {
            double sumOfMeans = 0D;
            for (int i = 0; i < data.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                List<ISpatialData> neighbors = null;
                switch (mode)
                {
                    case 1:
                        {
                            neighbors = GetGestureDRTreeKNN(data[i], k);
                            break;
                        }

                    case 2:
                        {
                            neighbors = GetSemanticPODRTreeKNN(data[i], k);
                            break;
                        }
                    case 3:
                        {
                            neighbors = GetSemanticPADRTreeKNN(data[i], k);
                            break;
                        }
                }
                double mean = await Task.Run(() => CalculateAvgDistanceToNeighbors(data[i], neighbors));
                sumOfMeans += mean;
            }
            double meansOfMeans = sumOfMeans / data.Count;

            return meansOfMeans;
        }
        catch(Exception e)
        {
            DebugMessageQueue.Add("Exception in FindMeansOfMeans(int k, List<ISpatialData> data, int mode): " + e.Message);
            //Handle exception.

            return Double.NaN;
        }
    }

    /// <summary>
    /// Finds the estimated derivative for each point. It approximates the slope of the 
    /// </summary>
    private void FindVelocity(Dictionary<int, double[]> KScores)
    {
        try
        {
            for (int k = 3; k <= KScores.Count; k++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                double velocity = EstimateSlope(k, 1, KScores);
                KScores[k][1] = Math.Abs(velocity);
            }
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in FindVelocity(Dictionary<int, double[]> KScores): " + e.Message);
            //Handle exception.

            for (int k = 3; k <= KScores.Count; k++)
            {
                KScores[k][1] = Double.NaN;
            }
        }
    }

    /// <summary>
    /// Estimates the 2nd derivative for datapoints listed in the KScores dictionary
    /// </summary>
    private void FindAcceleration(Dictionary<int, double[]> KScores)
    {
        try
        {
            for (int k = 4; k <= KScores.Count - 1; k++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                double accel = EstimateSlope(k, 2, KScores);
                KScores[k][2] = Math.Abs(accel);
            }
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in FindAcceleration(Dictionary<int, double[]> KScores): " + e.Message);
            //Handle exception.

            for (int k = 4; k <= KScores.Count - 1; k++)
            {
                KScores[k][2] = Double.NaN;
            }
        }
    }

    /// <summary>
    /// Estimates the 3rd derivative for datapoints listed in the KScores dictionary
    /// </summary>
    private void FindJerk(Dictionary<int, double[]> KScores)
    {
        try
        {
            for (int k = 5; k <= KScores.Count - 2; k++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                double jerk = EstimateSlope(k, 3, KScores);
                KScores[k][3] = Math.Abs(jerk);
            }
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in FindJerk(Dictionary<int, double[]> KScores): " + e.Message);
            //Handle exception.

            for (int k = 5; k <= KScores.Count - 2; k++)
            {
                KScores[k][3] = Double.NaN;
            }
        }
    }

    private double CalculateAvgDistanceToNeighbors(ISpatialData node, List<ISpatialData> neighbors)
    {
        try
        {
            double x1 = node.Envelope.CenterX;
            double y1 = node.Envelope.CenterY;
            double x2 = 0.0D;
            double y2 = 0.0D;
            double sum = 0.0;
            foreach (ISpatialData neighbor in neighbors)
            {
                if(!KeepRunning)
                {
                    break;
                }
                x2 = neighbor.Envelope.CenterX;
                y2 = neighbor.Envelope.CenterY;
                sum += CalculateDistance(x1, y1, x2, y2);
            }
            return sum / neighbors.Count;
        }
        catch (Exception e)
        {
            DebugMessageQueue.Add("Exception in CalculateAvgDistanceToNeighbors(ISpatialData node, List<ISpatialData> neighbors): " + e.Message);
            //Handle exception.

            return Double.NaN;
        }
    }

    private double CalculateDistance(double x1, double y1, double x2, double y2)
    {
        return System.Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }

    private double EstimateSlope(int k, int order, Dictionary<int, double[]> KScores)
    {
        double deltaX = 2; // (k+1) - (k-1) = 2
        double deltaY = KScores[k + 1][order - 1] - KScores[k - 1][order - 1];
        double slope = deltaY / deltaX;
        return slope;
    }

    public string KNNLabel(ISpatialData point, int k, bool weightedDistance = false, bool action = false)
    {
        List<ISpatialData> nearestNeighbors = GestureLSDRTree.KNearestNeighbor(point, k);
        string answer = "";
        if (weightedDistance == true)
        {
            var map = new Dictionary<string, double>();
            for (int i = 0; i < nearestNeighbors.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                var label = gc.pretendObjectLabel;
                if (action)
                {
                    label = gc.pretendActionLabel;
                }

                if (map.ContainsKey(label))
                {
                    map[label] += 1 / (0.00000001 + Envelope.BoxDistance(point.Envelope, gc.Envelope));
                }
                else
                {
                    map.Add(label, 1 / (0.00000001 + Envelope.BoxDistance(point.Envelope, gc.Envelope)));
                }
                var mapList = map.ToList();
                mapList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                mapList.Reverse();
                answer = mapList[0].Key;
            }
        }
        else
        {
            List<string> labels = new List<string>();
            for (int i = 0; i < nearestNeighbors.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                if (action)
                {
                    labels.Add(gc.pretendActionLabel);
                }
                else
                {
                    labels.Add(gc.pretendObjectLabel);
                }
            }
            var labelGroup = labels.GroupBy(x => x);
            var maxCount = labelGroup.Max(g => g.Count());
            var label = labelGroup.Where(x => x.Count() == maxCount).Select(x => x.Key).ToArray();
            answer = label[0];
        }

        return answer;
    }

    /// <summary>
    /// Writes the results to a text file.
    /// </summary>
    private void RecordResults(string fileName, List<GestureDefinition> gestureList, List<Dictionary<string, double>> scores)
    {
        using (TextWriter resultsWriter = new StreamWriter(streamingAssetsPath + fileName))
        {
            for (int i = 0; i < gestureList.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                string resultLine = "GestureIndex:" + (i) + ",PropName:" + gestureList[i].propObject.name + ",PretendObject:" + gestureList[i].pretendObjectName + ",PretendAction:" + gestureList[i].pretendActionName + ",";
                foreach (KeyValuePair<string, double> pair in scores[i])
                {
                    if (!KeepRunning)
                    {
                        break;
                    }
                    resultLine += pair.Key + ":" + pair.Value + ",";
                }
                resultLine = resultLine.Substring(0, resultLine.Length - 1);
                resultsWriter.WriteLine(resultLine);
            }

            for (int i = gestureList.Count; i < 2 * gestureList.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                string resultLine = "GestureIndex:" + (i) + ",PropName:" + gestureList[i - gestureList.Count].propObject.name + ",PretendObject:" + gestureList[i - gestureList.Count].pretendObjectName + ",PretendAction:" + gestureList[i].pretendActionName + ",";
                foreach (KeyValuePair<string, double> pair in scores[i])
                {
                    if (!KeepRunning)
                    {
                        break;
                    }
                    resultLine += pair.Key + ":" + pair.Value + ",";
                }
                resultLine = resultLine.Substring(0, resultLine.Length - 1);
                resultsWriter.WriteLine(resultLine);
            }
        }
        DebugMessageQueue.Add("Wrote Results To " + fileName);
    }

    /// <summary>
    /// Writes the results to a text file.
    /// </summary>
    private void RecordResults(string fileName, List<Dictionary<string, double>> KNNScores)
    {
        using (TextWriter resultsWriter = new StreamWriter(streamingAssetsPath + fileName))
        {
            for (int i = 0; i < KNNScores.Count; i++)
            {
                if (!KeepRunning)
                {
                    break;
                }
                string resultLine = "K:" + (i+2) + ",";
                foreach (KeyValuePair<string, double> pair in KNNScores[i])
                {
                    if (!KeepRunning)
                    {
                        break;
                    }
                    resultLine += pair.Key + ":" + pair.Value + ",";
                }
                resultLine = resultLine.Substring(0, resultLine.Length - 1);
                resultsWriter.WriteLine(resultLine);
            }
        }
        DebugMessageQueue.Add("Wrote Results To " + fileName);
    }

    /// <summary>
    /// Writes the results to a text file.
    /// </summary>
    private void RecordResults(string fileName, Dictionary<int, double[]> kScoreMap)
    {
        using (TextWriter resultsWriter = new StreamWriter(streamingAssetsPath + fileName))
        {
            foreach (KeyValuePair<int, double[]> pair in kScoreMap)
            {
                if (!KeepRunning)
                {
                    break;
                }
                string resultLine = "K:" + pair.Key + ",";
                resultLine += "KMeansofMeans:" + pair.Value[0] + ",";
                resultLine += "KVelocity:" + pair.Value[1] + ",";
                resultLine += "KAcceleration:" + pair.Value[2] + ",";
                resultLine += "KJerk:" + pair.Value[3] + ",";
                resultsWriter.WriteLine(resultLine);
            }
        }
        DebugMessageQueue.Add("Wrote Results To " + fileName);
    }

    private void UpdateAll(GestureDefinition gesture)
    {
        float[] aScore = SemanticDR.Reduce(WordVectorLookup[gesture.pretendActionName]);
        ReducedWord redActionWord = new ReducedWord(new Envelope(aScore[0], aScore[1]), gesture.pretendActionName);
        ActionDRTree.Insert(redActionWord);
        float[] oScore = SemanticDR.Reduce(WordVectorLookup[gesture.pretendObjectName]);
        ReducedWord redObjectWord = new ReducedWord(new Envelope(oScore[0], oScore[1]), gesture.pretendObjectName);
        ObjectDRTree.Insert(redObjectWord);
        float[] gScore = GesturalDR.Reduce(gesture);
        ReducedGesture redGesture = new ReducedGesture(new Envelope(gScore[0], gScore[1]), new Guid(gesture.gestureID));
        GestureDRTree.Insert(redGesture);

        SurpriseEval.UpdateDistribution(gesture);
    }

    private void LoadWordVectorDict()
    {
        if (Directory.Exists(streamingAssetsPath))
        {
            StreamReader wordReader = new StreamReader(streamingAssetsPath + WordVectorFileName);
            string line = wordReader.ReadLine();
            while (line != null)
            {
                string[] lineSplit = line.Split(',');
                string word = lineSplit[0];
                float[] vector = new float[lineSplit.Length - 1];
                for (int i = 1; i < lineSplit.Length; i++)
                {
                    vector[i - 1] = StringToFloat(lineSplit[i]);
                }

                WordVectorLookup.Add(word, vector);
                line = wordReader.ReadLine();
            }
        }
    }

    private static Dictionary<string, string> propAffordanceNameLookup = new Dictionary<string, string> {
        {"0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0,0,0,0,0,0,0", "Squid w Tentacles Partless OR Horseshoe"},
        {"0.6666667,0,0,0,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Lolliop w Cube Ends Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0.3333333,0,0,0,0.6666667,0.3333333,0,0,0,0,0", "Giant U w Flat Base Partless"},
        {"0,0.3333333,0.3333333,0,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0", "Big Curved Axe Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0,0,0.6666667,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0.3333333,0,0,0,0,0", "Sunflower Partless"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0", "Air Horn"},
        {"0,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Corn Dog"},
        {"0.3333333,0.6666667,0,0,0,0,0,0.3333333,0.3333333,0.3333333,0,0,0.3333333,0.3333333,0.3333333,0,0,0,0,0.3333333,0,0,0,0,0", "Giant Electric Plug"},
        {"0,0.3333333,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0.3333333,0,0.6666667,0,0,0.3333333,0,0,0,0,0.3333333,0,0", "Ring w Tail End"},
        {"0,0.3333333,0,0,0,0.6666667,0,0.3333333,0.6666667,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0", "Ring w Inward Spokes"},
        {"0,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0", "Large Ring w Outward Spokes"},
        {"0,0.3333333,0,0,0.3333333,0,0,0.3333333,0,0.3333333,0,0,0.3333333,0,0,0,0,0,0,0.3333333,0,0,0.3333333,0,0", "Tube w Cones"},
        {"0,0.6666667,0,0,0,0,0,0.6666667,0,0,0,0.3333333,0,0.3333333,0,0,0,0,0.6666667,0,0,0,0,0,0", "Thin Stick w Hammer End"},
        {"0,0,0,0,0,0,0.3333333,0,0.3333333,0,0,0,0.3333333,0,0,0,0,0,0,0,0,0,0.3333333,0,0", "Helix"},
        {"0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Giant Golf T"},
        {"0,0,0,0.3333333,0,0.3333333,0,0,0,0.6666667,0,0,0,0,0.6666667,0,0,0,0.6666667,0,0,0,0,0,0", "Circle With Ring"},
        {"0,0,0,0,0.6666667,0,0,0,0.3333333,0.3333333,0,0.3333333,0.3333333,0,0,0,0,0,0.6666667,0,0,0,0,0,0", "Palm Tree Partless"},
        {"0,0.3333333,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0.3333333,0,0.3333333,0,0.3333333,0,0.6666667,0,0,0,0,0,0", "Ladle"},
        {"0,0.6666667,0,0,0.3333333,0,0,0,0.3333333,0.3333333,0,0,0,0,0,0.3333333,0,0,0.6666667,0,0,0,0,0,0", "Plunger"}
    };

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }

    private static float[] RoundFloat(float[] v, int dec)
    {
        float[] answer = new float[v.Length];
        for (int i = 0; i < v.Length; i++)
        {
            answer[i] = Convert.ToSingle(Math.Round((decimal)v[i], dec));
        }
        return answer;
    }

    private static string toStringFloat(float[] f_arr)
    {
        String answer = "";
        float[] rounded = RoundFloat(f_arr, 7);
        for (int i = 0; i < f_arr.Length - 1; i++)
        {
            answer += rounded[i].ToString();
            answer += ",";
        }
        answer += rounded[f_arr.Length - 1].ToString();
        return answer;
    }

    private static T[] Concatenate1DArrays<T>(T[] a, T[] b)
    {
        T[] result = new T[a.Length + b.Length];
        Array.Copy(a, 0, result, 0, a.Length);
        Array.Copy(b, 0, result, a.Length, b.Length);
        return result;
    }

    public void OnApplicationQuit()
    {
        RunDebugMessageQueue = false;
        StopCoroutine("WriteDebugMessageToFile");
        KeepRunning = false;
    }
}