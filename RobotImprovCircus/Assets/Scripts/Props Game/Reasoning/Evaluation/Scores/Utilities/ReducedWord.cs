﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Representation of the pretend action/object labels after they have had their dimensionality reduced
/// </summary>
public class ReducedWord : ISpatialData
{
    /// <summary>
    /// The reduced version of the label word's vector
    /// </summary>
    public Envelope Envelope { get; set; }

    /// <summary>
    /// The pretend action/object label
    /// </summary>
    public string Label;

    public ReducedWord() : this(new Envelope()) { }

    public ReducedWord(Envelope env): this(env, "") { }

    public ReducedWord(Envelope env, string lab) 
    {
        this.Envelope = env;
        this.Label = lab;
    }
}
