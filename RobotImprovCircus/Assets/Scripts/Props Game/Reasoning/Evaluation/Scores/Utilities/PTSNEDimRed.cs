﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base for any classes that will use a Parametric T-SNE model.
/// </summary>
public abstract class PTSNEDimRed : ModelInterface
{
    protected string InputNodeName;
    protected string OutputNodeName;
    
    public int TargetDimension;
    
    public bool DebugMode = false;
    public bool isInit;

    override protected void InitModel()
    {
        // Only testing to make sure model works
        // Passing in a dummy vector full of zeros to test
        float[,] inputVector = new float[1, nInputs];
        Dictionary<string, dynamic> initInputs = new Dictionary<string, dynamic>() {
            { this.InputNodeName, inputVector}
        };
        isInit = Model.InitializeModel(GraphAsset.bytes, initInputs, OutputNodeName);
        if (isInit && DebugMode)
        {
            Debug.Log("Model initialized");
        }
        else if (DebugMode)
        {
            Debug.Log("Model not initialized");
        }
    }

    /// <summary>
    /// Given the float vector representation of a gesture,
    /// will return a lower dimensionality vector
    /// </summary>
    /// <param name="vector">A vector that represents the action component of a gesture</param>
    /// <returns>A reduced version of the vector given. Currently returns a 2D coordinate</returns>
    public float[] Reduce(float[] vector)
    {
        try
        {
            float[] reducedDimension = new float[TargetDimension];
            float[,] inputVector = new float[1, vector.Length];
            for (int i = 0; i < vector.Length; i++)
            {
                inputVector[0, i] = vector[i];
            }
            Dictionary<string, dynamic> inputs = new Dictionary<string, dynamic>() {
                { InputNodeName, inputVector}
            };
            float[,] output = Model.GenerateOutput(inputs, OutputNodeName) as float[,];
            for (int i = 0; i < TargetDimension; i++)
            {
                reducedDimension[i] = output[0, i];
            }

            if (DebugMode)
            {
                Debug.Log("Result successfully obtained. Results in form: " + output.GetLength(0) + "x" + output.GetLength(1));
            }

            return reducedDimension;
        } catch (Exception e)
        {
            Debug.Log("ERROR: PRSNEDimRed Reduce error");
            Debug.Log(e.Message);
            return new float[2];
        }
    }
}
