﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// The interactive interface for the Gestural Dimensionality Reduction Model
/// </summary>
public class GesturalDimRed : PTSNEDimRed
{
    // Only have a frozen model for 16000 input/datatype

    // Start is called before the first frame update
    void Start()
    {
        this.InputNodeName = "dense_1_input"; ;
        this.OutputNodeName = "dense_5/BiasAdd";
        this.TargetDimension = 2;

        switch (Executive.dataType)
        {
            case DataEnum.Data27000:
                {
                    this.nInputs = 27000;
                    break;
                }

            case DataEnum.Data16000:
                {
                    this.nInputs = 16000;
                    this.GraphAsset = Resources.Load("tsne_gestural_16000") as TextAsset;
                    break;
                }
        }
        InitModel();
    }

    /// <summary>
    /// Reduces the dimensions for a gesture
    /// </summary>
    /// <param name="gesture">The gesture object</param>
    /// <returns>An array of the gesture's reduced </returns>
    public float[] Reduce(GestureDefinition gesture)
    {
        try
        {
            GestureVector vector = null;
            //float[] inputTimeSeries = new float[27000];
            float[] inputTimeSeries = null;
            switch (gesture.dataType)
            {
                case DataEnum.Data27000:
                    {   // Uncomment below line once we get 27000 model
                        //vector = new GestureVector(gesture, 0, true, true, nInputs, 0, DataType);

                        //vector = new GestureVector(gesture, 0, true, true, nInputs, 0, Executive.dataType);
                        vector = new GestureVector(gesture, Executive.dataType);

                        break;
                    }
                case DataEnum.Data16000:
                    {
                        //vector = new GestureVector(gesture, 0, true, true, nInputs, 0, Executive.dataType);
                        vector = new GestureVector(gesture, Executive.dataType);
                        // Comment below line once we get 27000 model
                        //inputTimeSeries = vector.To1DFloat();
                        break;
                    }
            }
            // Uncomment below line once we get the 27000 model
            inputTimeSeries = vector.To1DFloat();

            return Reduce(inputTimeSeries);
        } catch (Exception e)
        {
            Debug.Log("ERROR: GesturalDimRed Reduce error");
            Debug.Log(e.Message);
            return new float[1];
        }
    }
}
