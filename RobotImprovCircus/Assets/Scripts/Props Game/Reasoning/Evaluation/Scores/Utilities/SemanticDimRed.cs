﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The interactive interface for the Semantic Dimensionality Reduction Model
/// </summary>
public class SemanticDimRed : PTSNEDimRed
{
    private string ModelFileName;

    void Start()
    {
        this.InputNodeName = "dense_1_input"; // Replace with the model's input node as needed
        this.OutputNodeName = "dense_5/BiasAdd"; // Replace with the model's output node as needed
        this.ModelFileName = "tsne_semantic_300"; // The frozen model file located in the Resources folder
        this.TargetDimension = 2;
        this.nInputs = 300;
        this.GraphAsset = Resources.Load(ModelFileName) as TextAsset;
        InitModel();
    }
}
