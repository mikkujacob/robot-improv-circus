﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

/// <summary>
/// Used for experimentation to find the K for our K nearest neighbor searches.
/// </summary>
public class KFinder : MonoBehaviour
{
    private string TimeSeriesFile = "/timeseries-16000.timeseries";
    private string DRProgressFile = "/DRPoints.txt";
    private string KMeansMeansFile = "/KMeansMeansAbsoluteValues.txt";
    private string StreamingAssetsPath;
    private RTree<ReducedGesture> GestureDRTree;
    private GesturalDimRed GestureDR;
    private Dictionary<int, double[]> EstimatedDeriv;
    // the double array is in order of position, velocity, acceleration
    public bool debug;

    // Start is called before the first frame update
    void Start()
    {
        StreamingAssetsPath = Application.streamingAssetsPath;
        GestureDRTree = new RTree<ReducedGesture>();
        GestureDR = FindObjectOfType<GesturalDimRed>();
        EstimatedDeriv = new Dictionary<int, double[]>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            EvaluateDatasetForDR();
            FindMeansOfMeansForK();
            FindVelocity();
            FindAcceleration();
            RecordDerivatives();
        }
    }

    /// <summary>
    /// Calculate the reduced dimensionality of all the gestures in the dataset
    /// Currently only works on 16000 dataset (will change once 27000 model is made)
    /// </summary>
    private void EvaluateDatasetForDR()
    {
        if (Directory.Exists(StreamingAssetsPath))
        {
            //DataTable dt = new DataTable();
            //dt.Columns.Add("X_Value", typeof(double));
            //dt.Columns.Add("Y_Value", typeof(double));

            // Finding the dimensionality reduced form of the gestures
            StreamReader timeseriesReader = new StreamReader(StreamingAssetsPath + TimeSeriesFile);

            int gestures = 0;
            string line = timeseriesReader.ReadLine();
            using (TextWriter DRWriter = new StreamWriter(StreamingAssetsPath + DRProgressFile))
            {
                while (line != null)
                {
                    float[] inputTimeSeries = Array.ConvertAll<string, float>(line.Split(','), StringToFloat);

                    float[] gestureDRVector = GestureDR.Reduce(inputTimeSeries);
                    ReducedGesture redGesture = new ReducedGesture(new Envelope(gestureDRVector[0], gestureDRVector[1]), new Guid());
                    GestureDRTree.Insert(redGesture);

                    DRWriter.WriteLine(gestureDRVector[0] + "," + gestureDRVector[1]);
                    //dt.Rows.Add(gestureDRVector[0], gestureDRVector[1]);

                    line = timeseriesReader.ReadLine();
                    gestures++;
                }

                if (debug)
                {
                    Debug.Log("Total gestures loaded:" + gestures);
                }
            }
            //        chart1.DataSource = dt;
            //        chart1.Series["Series1"].XValueMember = "X_Value";
            //        chart1.Series["Series1"].YValueMembers = "Y_Value";
            //        chart1.Series["Series1"].ChartType = SeriesChartType.Line;
            //        chart1.ChartAreas[0].AxisY.LabelStyle.Format = "";
        }
    }

    /// <summary>
    /// Calculates the average of the average distances for k nearest neighbors for all datapoints in an RTree
    /// </summary>
    private void FindMeansOfMeansForK()
    {
        List<ISpatialData> Gestures = GestureDRTree.KNearestNeighbor(GestureDRTree.root, GestureDRTree.Count);
        //using (TextWriter KMeansMeansWriter = new StreamWriter(StreamingAssetsPath + KMeansMeansFile))
        //{
            for (int k = 2; k < 974; k++)
            {
                double meanForK = FindMeansOfMeans(k, Gestures);
                // x: k
                // y: means of means
                double[] derivatives = new double[3];
                derivatives[0] = meanForK;
                EstimatedDeriv.Add(k, derivatives);
                Debug.Log(k + "," + meanForK);
            }
        //}
        //Debug.Log("Done calculating means of means for k!");
    }

    /// <summary>
    /// Find the average of the average distances between a point and its neighbors
    /// </summary>
    /// <param name="k">The amount of neighbors to compare any given point to</param>
    /// <returns>The mean of the average distance between a point and a specified amount of neighbors</returns>
    private double FindMeansOfMeans(int k, List<ISpatialData> Gestures)
    {
        double sumOfMeans = 0D;
        for(int i = 0; i < Gestures.Count; i++)
        {
            List<ISpatialData> neighbors = GestureDRTree.KNearestNeighbor(Gestures[i], k);
            double mean = CalculateAvgDistanceToNeighbors(Gestures[i], neighbors);
            sumOfMeans += mean;
        }
        double meansOfMeans = sumOfMeans / Gestures.Count;

        return meansOfMeans;
    }
    
    /// <summary>
    /// Estimates the 2nd derivative for datapoints listed in the EstimatedDeriv dictionary
    /// </summary>
    private void FindAcceleration()
    {
        for (int k = 4; k <= EstimatedDeriv.Count - 1; k++)
        {
            double accel = EstimateSlope(k, 2);
            EstimatedDeriv[k][2] = Math.Abs(accel);
        }
    }

    /// <summary>
    /// Finds the estimated derivative for each point. It approximates the slope of the 
    /// </summary>
    private void FindVelocity()
    {
        for (int k = 3; k <= EstimatedDeriv.Count; k++)
        {
            double velocity = EstimateSlope(k, 1);
            EstimatedDeriv[k][1] = Math.Abs(velocity);
        }
    }

    /// <summary>
    /// Calculates the average distance between a specified point and its neighbors
    /// </summary>
    /// <param name="node">The node that will be the common point</param>
    /// <param name="neighbors">All other nodes that will be compared to the common node</param>
    /// <returns>The average distance</returns>
    private double CalculateAvgDistanceToNeighbors(ISpatialData node, List<ISpatialData> neighbors)
    {
        var x1 = node.Envelope.CenterX;
        var y1 = node.Envelope.CenterY;
        var x2 = 0.0D;
        var y2 = 0.0D;
        var sum = 0.0D;
        foreach (ISpatialData neighbor in neighbors)
        {
            x2 = neighbor.Envelope.CenterX;
            y2 = neighbor.Envelope.CenterY;
            sum += CalculateDistance(x1, y1, x2, y2);
        }
        return sum / neighbors.Count;
    }

    private double CalculateDistance(double x1, double y1, double x2, double y2)
    {
        return System.Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }

    /// <summary>
    /// Estimates the slope of a point's tangent
    /// </summary>
    /// <param name="k"></param>
    /// <param name="order">whether you are looking for the 1st or 2nd derivative estimate</param>
    /// <returns>the estimated slope</returns>
    private double EstimateSlope(int k, int order)
    {
        double deltaX = 2; // (k+1) - (k-1) = 2
        double deltaY = EstimatedDeriv[k+1][order - 1] - EstimatedDeriv[k-1][order - 1];
        double slope = deltaY / deltaX;
        return slope;
    }

    private static float StringToFloat(String s)
    {
        return float.Parse(s);
    }

    /// <summary>
    /// Writes the derivative (velocity and acceleration) to a txt file
    /// </summary>
    private void RecordDerivatives()
    {
        using (TextWriter KMeansMeansWriter = new StreamWriter(StreamingAssetsPath + KMeansMeansFile))
        {
            foreach(KeyValuePair<int, double[]> estimate in EstimatedDeriv)
            {
                string str = DoubleArrayToString(estimate.Value);
                KMeansMeansWriter.WriteLine(estimate.Key + str);
            }
        }
        Debug.Log("Loaded and set!!");
    }

    private string DoubleArrayToString(double[] array)
    {
        string returnValue = "";

        foreach(double num in array)
        {
            returnValue += "," + num.ToString();
        }

        return returnValue;
    }
}
