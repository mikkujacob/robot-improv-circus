﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptiveScaling
{
    public float targetMin;
    public float targetMax;

    private float currentMin;
    private float currentMax;

    public AdaptiveScaling() : this(0, 1) { }

    public AdaptiveScaling(float targetMin, float targetMax)
    {
        this.targetMin = targetMin;
        this.targetMax = targetMax;
        this.currentMin = this.targetMin;
        this.currentMax = this.targetMax;
    }

    public float ScaleValue(float value)
    {
        if(value < currentMin)
        {
            currentMin = value;
        }
        else if(value > currentMax)
        {
            currentMax = value;
        }

        float scaledValue = targetMin + (value - currentMin) * (targetMax - targetMin) / (currentMax - currentMin);

        //Debug.Log("CurrentMin: " + currentMin + ", CurrentMax: " + currentMax + ", Value: " + value + ", ScaledValue: " + scaledValue);

        return scaledValue;
    }
}
