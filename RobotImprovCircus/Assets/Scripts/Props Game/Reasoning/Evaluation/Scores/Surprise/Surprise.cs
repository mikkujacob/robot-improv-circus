﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathNet.Numerics.Distributions;

// A numerical representation of how surprising or unexpected is an object
public class Surprise : Score
{
    private RTree<GestureCoordinate> gestureLSDRRTree;
    private RTree<ReducedWord> posDRRTree;
    private RTree<ReducedWord> pasDRRTree;
    private SemanticDimRed sDR;
    private RTree<ReducedGesture> gDRRTree;
    private GesturalDimRed gDR;
    private Dictionary<string, float[]> wordDict;
    private int kSemanticBS = 15; // TODO: Need to set empirically
    private int kGesturalBS = 15; // TODO: Need to set empirically
    private int kDFE = 5; // TODO: Need to set empirically

    public double d_dfe_g = 0.0;
    public double d_dfe_po = 0.0;
    public double d_dfe_pa = 0.0;
    public double d_dfe = 0.0;
    public double d_bs_g = 0.0;
    public double d_bs_po = 0.0;
    public double d_bs_pa = 0.0;
    public double d_bs = 0.0;
    public double d = 0.0;

    private AdaptiveScaling scalingBS;
    private AdaptiveScaling scalingDFE;

    private Dictionary<string, int> objectToIndexDict;
    private Dictionary<string, int> actionToIndexDict;
    private Dictionary<string, int> propToIndexDict;
    private Dictionary<string, int> gestureCoordinateToIndexDict;

    private System.Random rand;
    //private int[,,] frequencies;
    private int[,] frequenciesOA;
    private int[,] frequenciesAA;
    private int[,] frequenciesGA;
    private int[,] frequenciesOATemp;
    private int[,] frequenciesAATemp;
    private int[,] frequenciesGATemp;
    //private int total = 0;
    private int totalOA = 0;
    private int totalAA = 0;
    private int totalGA = 0;
    private int totalOATemp = 0;
    private int totalAATemp = 0;
    private int totalGATemp = 0;
    public int maxReps = 5;
    public double scaleFactorSBS = 10;
    public double scaleFactorGBS = 10;

    public double weightBS = 1.0;
    public double weightDFE = 1.0;

    private readonly string objectFrequenciesFilePath = "/pretend_objects_frequency.txt";
    private readonly string actionFrequenciesFilePath = "/pretend_actions_frequency.txt";


    public Surprise(RTree<GestureCoordinate> gLSDRRTree, RTree<ReducedGesture> gDRRTree, GesturalDimRed gDR, RTree<ReducedWord> posDRRTree, RTree<ReducedWord> pasDRRTree, SemanticDimRed sDR, Dictionary<string, float[]> wordDict)
    {
        gestureLSDRRTree = gLSDRRTree;

        this.wordDict = wordDict;
        this.posDRRTree = posDRRTree;
        this.pasDRRTree = pasDRRTree;
        this.sDR = sDR;
        this.gDRRTree = gDRRTree;
        this.gDR = gDR;

        objectToIndexDict = new Dictionary<string, int>();
        actionToIndexDict = new Dictionary<string, int>();
        propToIndexDict = new Dictionary<string, int>
        {
            {"Squid w Tentacles Partless", 0},
            {"Lolliop w Cube Ends Partless", 1},
            {"Giant U w Flat Base Partless", 2},
            {"Big Curved Axe Partless", 3},
            {"Sunflower Partless", 4},
            {"Air Horn", 5},
            {"Corn Dog", 6},
            {"Giant Electric Plug", 7},
            {"Ring w Tail End", 8},
            {"Ring w Inward Spokes", 9},
            {"Large Ring w Outward Spokes", 10},
            {"Tube w Cones", 11},
            {"Thin Stick w Hammer End", 12},
            {"Horseshoe", 13},
            {"Helix", 14},
            {"Giant Golf T", 15},
            {"Circle With Ring", 16},
            {"Palm Tree Partless", 17},
            {"Ladle", 18},
            {"Plunger", 19}
        };
        gestureCoordinateToIndexDict = new Dictionary<string, int>();

        rand = new System.Random();

        scalingBS = new AdaptiveScaling();
        scalingDFE = new AdaptiveScaling();

        if (Directory.Exists(Application.streamingAssetsPath))
        {
            StreamReader pretendObjectReader = new StreamReader(Application.streamingAssetsPath + objectFrequenciesFilePath);
            StreamReader pretendActionReader = new StreamReader(Application.streamingAssetsPath + actionFrequenciesFilePath);

            string line = "";
            int lineIndex = 0;
            while ((line = pretendObjectReader.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                string[] terms = line.Split(':');

                objectToIndexDict[terms[0]] = lineIndex++;
            }

            lineIndex = 0;
            while ((line = pretendActionReader.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }
                string[] terms = line.Split(':');

                actionToIndexDict[terms[0]] = lineIndex++;
            }

            for(int i = 0; i < 199; i++)
            {
                string y = (i < 10) ? "00" + i : (i < 100) ? "0" + i : "" + i;
                for (int j = 0; j < 199; j++)
                {
                    string x = (j < 10) ? "00" + j : (j < 100) ? "0" + j : "" + j;
                    gestureCoordinateToIndexDict["" + x + y] = i * 199 + j;
                }
            }

            //frequencies = new int[objectToIndexDict.Count, actionToIndexDict.Count, propToIndexDict.Count];
            frequenciesOA = new int[objectToIndexDict.Count, propToIndexDict.Count];
            for (int i = 0; i < frequenciesOA.GetLength(0); i++)
            {
                for (int j = 0; j < frequenciesOA.GetLength(1); j++)
                {
                    frequenciesOA[i, j] = 1;
                    ++totalOA;
                }
            }

            frequenciesAA = new int[actionToIndexDict.Count, propToIndexDict.Count];
            for (int i = 0; i < frequenciesAA.GetLength(0); i++)
            {
                for (int j = 0; j < frequenciesAA.GetLength(1); j++)
                {
                    frequenciesAA[i, j] = 1;
                    ++totalAA;
                }
            }
            frequenciesOATemp = new int[objectToIndexDict.Count, propToIndexDict.Count];
            frequenciesAATemp = new int[actionToIndexDict.Count, propToIndexDict.Count];

            frequenciesGA = new int[gestureCoordinateToIndexDict.Count, propToIndexDict.Count];
            for (int i = 0; i < frequenciesGA.GetLength(0); i++)
            {
                for (int j = 0; j < frequenciesGA.GetLength(1); j++)
                {
                    frequenciesGA[i, j] = 1;
                    ++totalGA;
                }
            }
            frequenciesGATemp = new int[gestureCoordinateToIndexDict.Count, propToIndexDict.Count];
        }
    }

    public override float CalculateScore(GestureDefinition gesture)
    {
        try
        {
            if (string.IsNullOrEmpty(gesture.pretendObjectName) || string.IsNullOrEmpty(gesture.pretendActionName) || string.IsNullOrEmpty(gesture.propObject.name))
            {
                Debug.Log("ERROR: Input for surprise calculation had null or empty value(s).");
                Debug.Log("DEBUG OUTPUT: Object: " + gesture.pretendObjectName + ", Action: " + gesture.pretendActionName + ", Prop: " + gesture.propObject.name);
                return -1;
            }

            double bayesianSurpriseScore = CalculateSemanticBayesianSurprise(gesture) * scaleFactorSBS + CalculateGesturalBayesianSurprise(gesture) * scaleFactorGBS;
            double distanceFromExpectationScore = CalculateDistanceFromExpectationSurprise(gesture);

            d_bs = bayesianSurpriseScore;

            d_dfe = distanceFromExpectationScore;

            d = (d_bs + d_dfe) / 2.0;

            return (scalingBS.ScaleValue((float)(bayesianSurpriseScore * weightBS)) + scalingDFE.ScaleValue((float)(distanceFromExpectationScore * weightDFE))) / 2.0f;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0f;
        }
    }

    private double CalculateDistanceFromExpectationSurprise(GestureDefinition gesture)
    {
        try
        {
            //Debug.Log("Starting DFE Surprise");
            //Reduce input object, action, and gesture.
            float[] poV = sDR.Reduce(wordDict[gesture.pretendObjectName]);
            ReducedWord po = new ReducedWord(new Envelope(poV[0], poV[1]), gesture.pretendObjectName);

            //Debug.Log("Calculated PO reduced");

            float[] paV = sDR.Reduce(wordDict[gesture.pretendActionName]);
            ReducedWord pa = new ReducedWord(new Envelope(paV[0], paV[1]), gesture.pretendActionName);

            //Debug.Log("Calculated PA reduced");

            float[] gV = gDR.Reduce(gesture);
            ReducedGesture g = new ReducedGesture(new Envelope(gV[0], gV[1]), new Guid(gesture.gestureID));

            //Debug.Log("Calculated G reduced");

            //Get list of reduced most expected objects, actions, and gestures.
            List<ISpatialData> kNObjects = ReduceSemanticList(GetKMostExpected(kDFE, 0, 1, propToIndexDict[gesture.propObject.name], objectToIndexDict, propToIndexDict, frequenciesOA, totalOA));

            //Debug.Log("Got K Most Expected O");

            List<ISpatialData> kNActions = ReduceSemanticList(GetKMostExpected(kDFE, 0, 1, propToIndexDict[gesture.propObject.name], actionToIndexDict, propToIndexDict, frequenciesAA, totalAA));

            //Debug.Log("Got K Most Expected A");

            List<ISpatialData> kNGestures = ReduceGesturalList(GetKMostExpected(kDFE, 0, 1, propToIndexDict[gesture.propObject.name], gestureCoordinateToIndexDict, propToIndexDict, frequenciesGA, totalGA));

            //Debug.Log("Got K Most Expected G");

            //Calculate distances to most expected objects, actions, and gestures.
            double D_PO = CalculateAvgDistance(po, kNObjects);
            double D_PA = CalculateAvgDistance(pa, kNActions);
            double D_G = CalculateAvgDistance(g, kNGestures);

            d_dfe_po = D_PO;
            d_dfe_pa = D_PA;
            d_dfe_g = D_G;

            //Debug.Log("D_DFE_PO: " + D_PO + ", D_DFE_PA: " + D_PA + ", D_DFE_G: " + D_G);

            return (D_PO + D_PA + D_G) / 3.0;//TODO: Scale, weight, or normalize.
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0;
        }
    }

    private double CalculateAvgDistance(ISpatialData point, List<ISpatialData> neighbors)
    {
        try
        {
            double distanceSum = 0.0;
            foreach (ISpatialData neighbor in neighbors)
            {
                distanceSum += Envelope.BoxDistance(point.Envelope, neighbor.Envelope);
                //Debug.Log("Distance Sum: " + distanceSum);
            }

            int neighborCountGuarded = (neighbors.Count > 0) ? neighbors.Count : 1;

            return distanceSum / neighborCountGuarded;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0;
        }
    }

    private List<ISpatialData> ReduceSemanticList(List<KeyValuePair<string, double>> list)
    {
        try
        {
            List<ISpatialData> result = new List<ISpatialData>();

            foreach (KeyValuePair<string, double> wordEntry in list)
            {
                float[] v = sDR.Reduce(wordDict[wordEntry.Key]);
                ReducedWord w = new ReducedWord(new Envelope(v[0], v[1]), wordEntry.Key);
                result.Add(w);
            }

            return result;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            List<ISpatialData> result = new List<ISpatialData>();
            result.Add(new ReducedWord(new Envelope(0.0f, 0.0f), "ERROR_KEY"));

            return result;
        }
    }

    private List<ISpatialData> ReduceGesturalList(List<KeyValuePair<string, double>> list)
    {
        try
        {
            List<ISpatialData> result = new List<ISpatialData>();

            foreach (KeyValuePair<string, double> wordEntry in list)
            {
                //Decode gesture coordinate string;
                string xStr = wordEntry.Key.Substring(0, 3);
                int x = int.Parse(xStr);
                float xFl = (x > 99) ? x - 100 : -x;
                xFl /= 10.0f;
                string yStr = wordEntry.Key.Substring(3, 3);
                int y = int.Parse(yStr);
                float yFl = (y > 99) ? y - 100 : -y;
                yFl /= 10.0f;

                ReducedGesture g = new ReducedGesture(new Envelope(xFl, yFl), Guid.NewGuid());
                result.Add(g);
            }

            return result;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            List<ISpatialData> result = new List<ISpatialData>();
            result.Add(new ReducedGesture(new Envelope(0.0f, 0.0f), Guid.NewGuid()));
            return result;
        }
    }

    private List<KeyValuePair<string, double>> GetKMostExpected(int k, int targetDim, int conditionDim, int conditionIndex, Dictionary<string, int> firstDict, Dictionary<string, int> secondDict, int[,] frequencies, int total)
    {
        try
        {
            List<KeyValuePair<string, double>> resultList = new List<KeyValuePair<string, double>>();

            double[] PTGivenC = ConditionalP2D(targetDim, conditionDim, conditionIndex, firstDict.Values, secondDict.Values, frequencies, total);
            List<string> keys = new List<string>(firstDict.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                resultList.Add(new KeyValuePair<string, double>(keys[i], PTGivenC[i]));
            }

            resultList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            int removeTotal = resultList.Count - k;
            for (int i = 0; i < removeTotal; i++)
            {
                resultList.RemoveAt(0);
            }

            return resultList;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            List<KeyValuePair<string, double>> resultList = new List<KeyValuePair<string, double>>();
            resultList.Add(new KeyValuePair<string, double>("ERROR_KEY", 0.0));
            return resultList;
        }
    }

    private double CalculateGesturalBayesianSurprise(GestureDefinition gesture)
    {
        try
        {
            double[] PG = LinearP2D(0, gestureCoordinateToIndexDict.Values, propToIndexDict.Values, frequenciesGA, totalGA);

            frequenciesGATemp = (int[,])frequenciesGA.Clone();
            totalGATemp = totalGA;

            AddObservation(gesture.coordinateIndex, gesture.propObject.name);

            List<KeyValuePair<GestureCoordinate, double>> options = GetKNNOptions(gesture.coordinateIndex, kGesturalBS);

            foreach (KeyValuePair<GestureCoordinate, double> pair in options)
            {
                int reps = (int)Math.Round(maxReps / (pair.Value + 1)); // Trying to do an inverse weight to distance.
                reps = (reps > maxReps) ? maxReps : ((reps < 1) ? 1 : reps); // Clamp output between 1 and maxReps
                for (int i = 0; i < reps; i++)
                {
                    AddObservation(pair.Key, gesture.propObject.name);
                }
            }

            double[] PGGivenA = ConditionalP2D(0, 1, propToIndexDict[gesture.propObject.name], gestureCoordinateToIndexDict.Values, propToIndexDict.Values, frequenciesGATemp, totalGATemp);

            double differenceGGivenA = CalculateDistributionDifference(PGGivenA, PG);

            d_bs_g = differenceGGivenA;

            //Debug.Log("D_KL(P(G|A), P(G)): " + differenceGGivenA);

            return differenceGGivenA;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0;
        }
    }

    private double CalculateSemanticBayesianSurprise(GestureDefinition gesture)
    {
        try
        {
            //Debug.Log("Starting BS Calculation");

            double[] PO = LinearP2D(0, objectToIndexDict.Values, propToIndexDict.Values, frequenciesOA, totalOA);
            double[] PA = LinearP2D(0, actionToIndexDict.Values, propToIndexDict.Values, frequenciesAA, totalAA);

            //Debug.Log("LinearP2Ds done");

            frequenciesOATemp = (int[,])frequenciesOA.Clone();
            totalOATemp = totalOA;
            frequenciesAATemp = (int[,])frequenciesAA.Clone();
            totalAATemp = totalAA;

            //Debug.Log("Temps initted.");

            AddObservation(gesture.pretendObjectName, gesture.pretendActionName, gesture.propObject.name);

            //Debug.Log("Added gesture to observation");

            List<KeyValuePair<string[], double>> options = GetKNNLabelOptions(gesture.coordinateIndex, kSemanticBS);//TODO: Debug why this might be failing here!

            //Debug.Log("Got neighbors.");

            foreach (KeyValuePair<string[], double> pair in options)
            {
                int reps = (int)Math.Round(maxReps / (pair.Value + 1)); // Trying to do an inverse weight to distance.
                reps = (reps > maxReps) ? maxReps : ((reps < 1) ? 1 : reps); // Clamp output between 1 and maxReps
                for (int i = 0; i < reps; i++)
                {
                    AddObservation(pair.Key[0], pair.Key[1], gesture.propObject.name);
                }

                //Debug.Log("Added observation: " + pair.Key[0] + ", " + pair.Key[1] + ", " + gesture.propObject.name);
            }

            //Debug.Log("Added all observations.");

            double[] POGivenA = ConditionalP2D(0, 1, propToIndexDict[gesture.propObject.name], objectToIndexDict.Values, propToIndexDict.Values, frequenciesOATemp, totalOATemp);
            double[] PAGivenA = ConditionalP2D(0, 1, propToIndexDict[gesture.propObject.name], actionToIndexDict.Values, propToIndexDict.Values, frequenciesAATemp, totalAATemp);

            //Debug.Log("Conditional P2Ds calculated.");

            double differenceOGivenA = CalculateDistributionDifference(POGivenA, PO);
            //Debug.Log("D_KL(P(O|A), P(O)): " + differenceOGivenA);
            double differenceAGivenA = CalculateDistributionDifference(PAGivenA, PA);
            //Debug.Log("D_KL(P(A|A), P(A)): " + differenceAGivenA);

            //Debug.Log("Differences calculated.");

            d_bs_po = differenceOGivenA;
            d_bs_pa = differenceAGivenA;

            //Debug.Log("D_KL(P(O|A), P(O)): " + differenceOGivenA + ", D_KL(P(A|A), P(A)): " + differenceAGivenA);

            return differenceOGivenA + differenceAGivenA; //TODO: Weight or normalize
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0;
        }
    }

    private double CalculateDistributionDifference(double[] d1, double[] d2)
    {
        try
        {
            //D_KL = | P(M_i | D) * log(P(M_i | D) / P(M_i)) / log(2) |

            double d_kl = 0.0;
            for (int i = 0; i < d1.Length; i++)
            {
                if ((d1[i] > -1 * double.Epsilon && d1[i] < double.Epsilon) && (d2[i] > -1 * double.Epsilon && d2[i] < double.Epsilon))
                {
                    d_kl += 0;
                }
                else
                {
                    double d1_i_guarded = (d1[i] > -1 * double.Epsilon && d1[i] < double.Epsilon) ? d1[i] + (1.0 / 1000.0) : d1[i];
                    double d2_i_guarded = (d2[i] > -1 * double.Epsilon && d2[i] < double.Epsilon) ? d2[i] + (1.0 / 1000.0) : d2[i];
                    d_kl += Math.Abs(d1[i] * Math.Log10(Math.Abs(d1[i]) / Math.Abs(d2_i_guarded)) / Math.Log10(2));
                }
            }

            return d_kl;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return -1.0;
        }
    }

    private void AddObservation(GestureCoordinate gc, string prop)
    {
        try
        {
            double cX = (gc.Envelope.CenterX < 10.0f) ? gc.Envelope.CenterX : gc.Envelope.CenterX - 10;
            double cY = (gc.Envelope.CenterY < 10.0f) ? gc.Envelope.CenterY : gc.Envelope.CenterY - 10;
            int a = (int)Math.Abs(Math.Round(cX * 10)) + ((cX > 0.0) ? 100 : 0);
            int b = (int)Math.Abs(Math.Round(cY * 10)) + ((cY > 0.0) ? 100 : 0);

            string x = (a < 10) ? "00" + a : (a < 100) ? "0" + a : "" + a;
            string y = (b < 10) ? "00" + b : (b < 100) ? "0" + b : "" + b;

            frequenciesGATemp[gestureCoordinateToIndexDict["" + x + y], propToIndexDict[prop]] += 1;
            totalGATemp++;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            Debug.Log("KEYS: G:" + gc + ", P: " + prop);
        }
    }

    private void AddObservations(List<GestureCoordinate> gcs, List<string> props)
    {
        try
        {
            for (int i = 0; i < gcs.Count; i++)
            {
                double cX = (gcs[i].Envelope.CenterX < 10.0f) ? gcs[i].Envelope.CenterX : gcs[i].Envelope.CenterX - 10;
                double cY = (gcs[i].Envelope.CenterY < 10.0f) ? gcs[i].Envelope.CenterY : gcs[i].Envelope.CenterY - 10;
                int a = (int)Math.Abs(Math.Round(cX * 10)) + ((cX > 0.0) ? 100 : 0);
                int b = (int)Math.Abs(Math.Round(cY * 10)) + ((cY > 0.0) ? 100 : 0);

                string x = (a < 10) ? "00" + a : (a < 100) ? "0" + a : "" + a;
                string y = (b < 10) ? "00" + b : (b < 100) ? "0" + b : "" + b;

                frequenciesGATemp[gestureCoordinateToIndexDict["" + x + y], propToIndexDict[props[i]]] += 1;
                totalGATemp++;
            }
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
        }
    }

    private void AddObservation(string obj, string act, string prop)
    {
        try
        {
            frequenciesOATemp[objectToIndexDict[obj], propToIndexDict[prop]] += 1;
            totalOATemp++;
            frequenciesAATemp[actionToIndexDict[act], propToIndexDict[prop]] += 1;
            totalAATemp++;
        }
        catch(Exception e)
        {
            Debug.Log("EXCEPTION In AddObservation(): " + e.Message);
            Debug.Log("KEYS: O:" + obj + ", A: " + act + ", P: " + prop);
        }
    }

    private void AddObservations(List<string> objs, List<string> acts, List<string> props)
    {
        try
        {
            for (int i = 0; i < objs.Count; i++)
            {
                frequenciesOATemp[objectToIndexDict[objs[i]], propToIndexDict[props[i]]] += 1;
                totalOATemp++;
                frequenciesAATemp[actionToIndexDict[acts[i]], propToIndexDict[props[i]]] += 1;
                totalAATemp++;
            }
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
        }
    }

    public void UpdateDistribution(GestureDefinition gesture)
    {
        try
        {
            if (string.IsNullOrEmpty(gesture.pretendObjectName) || string.IsNullOrEmpty(gesture.pretendActionName) || string.IsNullOrEmpty(gesture.propObject.name))
            {
                Debug.Log("ERROR: Input for surprise calculation had null or empty value(s).");
                Debug.Log("DEBUG OUTPUT: Object: " + gesture.pretendObjectName + ", Action: " + gesture.pretendActionName + ", Prop: " + gesture.propObject.name);
                return;
            }

            AddObservation(gesture.pretendObjectName, gesture.pretendActionName, gesture.propObject.name);

            List<KeyValuePair<string[], double>> options = GetKNNLabelOptions(gesture.coordinateIndex, kSemanticBS);

            foreach (KeyValuePair<string[], double> pair in options)
            {
                int reps = (int)Math.Round(maxReps / (1 + pair.Value)); // Trying to do an inverse weight to distance.
                reps = (reps > maxReps) ? maxReps : ((reps < 1) ? 1 : reps); // Clamp output between 1 and maxReps
                for (int i = 0; i < reps; i++)
                {
                    AddObservation(pair.Key[0], pair.Key[1], gesture.propObject.name);
                }
            }

            frequenciesOA = (int[,])frequenciesOATemp.Clone();
            totalOA = totalOATemp;
            frequenciesAA = (int[,])frequenciesAATemp.Clone();
            totalAA = totalAATemp;

            AddObservation(gesture.coordinateIndex, gesture.propObject.name);

            List<KeyValuePair<GestureCoordinate, double>> coordOptions = GetKNNOptions(gesture.coordinateIndex, kGesturalBS);

            foreach (KeyValuePair<GestureCoordinate, double> pair in coordOptions)
            {
                int reps = (int)Math.Round(maxReps / (pair.Value + 1)); // Trying to do an inverse weight to distance.
                reps = (reps > maxReps) ? maxReps : ((reps < 1) ? 1 : reps); // Clamp output between 1 and maxReps
                for (int i = 0; i < reps; i++)
                {
                    AddObservation(pair.Key, gesture.propObject.name);
                }
            }

            frequenciesGA = (int[,])frequenciesGATemp.Clone();
            totalGA = totalGATemp;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
        }
    }

    public List<KeyValuePair<string[], double>> GetKNNLabelOptions(ISpatialData point, int k)//TODO: Debug why it's failing here!
    {
        try
        {
            List<ISpatialData> nearestNeighborsO = gestureLSDRRTree.KNearestNeighbor(point, k);
            List<KeyValuePair<string[], double>> resultList = new List<KeyValuePair<string[], double>>();
            for (int i = 0; i < nearestNeighborsO.Count; i++)
            {
                GestureCoordinate gc = (GestureCoordinate)nearestNeighborsO[i];
                string[] label = new string[] { gc.pretendObjectLabel, gc.pretendActionLabel, gc.propName };
                double distance = Envelope.BoxDistance(point.Envelope, gc.Envelope);
                resultList.Add(new KeyValuePair<string[], double>(label, distance));
            }

            resultList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
            return resultList;
        }
        catch(Exception e)
        {
            Debug.Log("EXCEPTION IN GetKNNLabelOptions(): " + e.Message);
            List<KeyValuePair<string[], double>> resultList = new List<KeyValuePair<string[], double>>();
            resultList.Add(new KeyValuePair<string[], double>(new string[] { "ERROR_KEY", "ERROR_KEY", "ERROR_KEY" }, 0.0));
            return resultList;
        }
    }

    public List<KeyValuePair<GestureCoordinate, double>> GetKNNOptions(ISpatialData point, int k)
    {
        try
        {
            List<ISpatialData> nearestNeighbors = gestureLSDRRTree.KNearestNeighbor(point, k);
            List<KeyValuePair<GestureCoordinate, double>> resultList = new List<KeyValuePair<GestureCoordinate, double>>();
            for (int i = 0; i < nearestNeighbors.Count; i++)
            {
                GestureCoordinate gc = (GestureCoordinate)nearestNeighbors[i];
                double distance = Envelope.BoxDistance(point.Envelope, gc.Envelope);
                resultList.Add(new KeyValuePair<GestureCoordinate, double>(gc, distance));
            }

            resultList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
            return resultList;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);
            List<KeyValuePair<GestureCoordinate, double>> resultList = new List<KeyValuePair<GestureCoordinate, double>>();
            resultList.Add(new KeyValuePair<GestureCoordinate, double>(new GestureCoordinate(new Envelope(), Guid.NewGuid()), 0.0));
            return resultList;
        }
    }

    private int GetJointIndex(int index1, int index2, int count1, int count2)
    {
        return index1 + count1 * index2;
    }

    private int GetJointIndex(string obj, string act, string prop)
    {
        return GetJointIndex(objectToIndexDict[obj], actionToIndexDict[act], propToIndexDict[prop]);
    }

    private int GetJointIndex(int objIndex, int actIndex, int propIndex)
    {
        return objIndex + objectToIndexDict.Count * actIndex + objectToIndexDict.Count * actionToIndexDict.Count * propIndex;
    }

    public void PrintDistribution(string name, Dirichlet dist)
    {
        string text = name + ": ";
        for (int i = 0; i < dist.Alpha.Length; i++)
        {
            if (i < dist.Alpha.Length - 1)
            {
                text += dist.Alpha[i] + ",";
            }
            else
            {
                text += dist.Alpha[i] + "\n";
            }
        }

        Debug.Log(text);
    }

    public void PrintArray(string name, double[] data)
    {
        string text = name + ": ";
        for (int i = 0; i < data.Length; i++)
        {
            if (i < data.Length - 1)
            {
                text += data[i] + ",";
            }
            else
            {
                text += data[i] + "\n";
            }
        }

        Debug.Log(text);
    }

    private int GetFrequencySum2D(int firstIndex, int secondIndex, Dictionary<string, int> firstDict, Dictionary<string, int> secondDict, int[,] frequencies, int total)
    {
        try
        {
            if (total == 0)
            {
                return 0;
            }

            List<int> firstIndices = new List<int>();
            if (firstIndex == -1)
            {
                firstIndices.AddRange(firstDict.Values);
            }
            else
            {
                firstIndices.Add(firstIndex);
            }

            List<int> secondIndices = new List<int>();
            if (secondIndex == -1)
            {
                secondIndices.AddRange(secondDict.Values);
            }
            else
            {
                secondIndices.Add(secondIndex);
            }

            int frequencySum = 0;
            foreach (int i in firstIndices)
            {
                foreach (int j in secondIndices)
                {
                    frequencySum += frequencies[i, j];
                }
            }

            return frequencySum;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return 0;
        }
    }

    private double P2D(int firstIndex, int secondIndex, IEnumerable<int> firstDictValues, IEnumerable<int> secondDictValues, int[,] frequencies, int total)
    {
        try
        {
            if (total == 0)
            {
                return 0;
            }

            List<int> firstIndices = new List<int>();
            if (firstIndex == -1)
            {
                firstIndices.AddRange(firstDictValues);
            }
            else
            {
                firstIndices.Add(firstIndex);
            }

            List<int> secondIndices = new List<int>();
            if (secondIndex == -1)
            {
                secondIndices.AddRange(secondDictValues);
            }
            else
            {
                secondIndices.Add(secondIndex);
            }

            double pSum = 0;
            foreach (int i in firstIndices)
            {
                foreach (int j in secondIndices)
                {
                    pSum += ((double)frequencies[i, j] / (double)total);
                }
            }

            return pSum;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return 0.0;
        }
    }

    private double[] ConditionalP2D(int targetDim, int conditionDim, int conditionIndex, IEnumerable<int> firstDictValues, IEnumerable<int> secondDictValues, int[,] frequencies, int total)
    {
        try
        {
            double[] result = new double[frequencies.GetLength(targetDim)];
            if (total == 0)
            {
                return result;
            }
            for (int i = 0; i < frequencies.GetLength(targetDim); i++)
            {
                if (conditionDim == 0)
                {
                    result[i] = P2D(conditionIndex, i, firstDictValues, secondDictValues, frequencies, total);
                }
                else if (conditionDim == 1)
                {
                    result[i] = P2D(i, conditionIndex, firstDictValues, secondDictValues, frequencies, total);
                }
            }

            return result;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return new double[frequencies.GetLength(targetDim)];
        }
    }

    private double[] LinearP2D(int dim1, IEnumerable<int> firstDictValues, IEnumerable<int> secondDictValues, int[,] frequencies, int total)
    {
        try
        {
            double[] result = new double[frequencies.GetLength(dim1)];
            if (total == 0)
            {
                return result;
            }
            for (int i = 0; i < frequencies.GetLength(dim1); i++)
            {
                if (dim1 == 0)
                {
                    result[i] = P2D(i, -1, firstDictValues, secondDictValues, frequencies, total);
                }
                else if (dim1 == 1)
                {
                    result[i] = P2D(-1, i, firstDictValues, secondDictValues, frequencies, total);
                }
            }

            return result;
        }
        catch (Exception e)
        {
            Debug.Log("ERROR: " + e.Message);

            return new double[frequencies.GetLength(dim1)];
        }
    }

    /// <summary>
    /// Gets the frequency sum for a particular pretend object, pretend action, and prop name.
    /// Sums output across one of those dimensions.
    /// </summary>
    /// <returns>The frequency.</returns>
    /// <param name="obj">The desired pretend object. Pass '#' to sum along this dimension.</param>
    /// <param name="act">The desired pretend action. Pass '#' to sum along this dimension.</param>
    /// <param name="prop">The desired prop name. Pass '#' to sum along this dimension.</param>
    private int GetFrequencySum3D(string obj, string act, string prop, int[,,] frequencies, int total)
    {
        int objIndex = (obj == "#") ? -1 : objectToIndexDict[obj];
        int actIndex = (act == "#") ? -1 : actionToIndexDict[act];
        int propIndex = (prop == "#") ? -1 : propToIndexDict[prop];

        return GetFrequencySum3D(objIndex, actIndex, propIndex, frequencies, total);
    }

    /// <summary>
    /// Gets the frequency sum for a particular pretend object, pretend action, and prop name.
    /// Sums output across one of those dimensions.
    /// </summary>
    /// <returns>The frequency.</returns>
    /// <param name="objIndex">The desired pretend object index. Pass -1 to sum along this dimension.</param>
    /// <param name="actIndex">The desired pretend action index. Pass -1 to sum along this dimension.</param>
    /// <param name="propIndex">The desired prop index. Pass -1 to sum along this dimension.</param>
    private int GetFrequencySum3D(int objIndex, int actIndex, int propIndex, int[,,] frequencies, int total)
    {
        if (total == 0)
        {
            return 0;
        }

        List<int> objIndices = new List<int>();
        if (objIndex == -1)
        {
            objIndices.AddRange(objectToIndexDict.Values);
        }
        else
        {
            objIndices.Add(objIndex);
        }

        List<int> actIndices = new List<int>();
        if (actIndex == -1)
        {
            actIndices.AddRange(actionToIndexDict.Values);
        }
        else
        {
            actIndices.Add(actIndex);
        }

        List<int> propIndices = new List<int>();
        if (propIndex == -1)
        {
            propIndices.AddRange(propToIndexDict.Values);
        }
        else
        {
            propIndices.Add(propIndex);
        }

        int frequencySum = 0;
        foreach (int i in objIndices)
        {
            foreach (int j in actIndices)
            {
                foreach (int k in propIndices)
                {
                    frequencySum += frequencies[i, j, k];
                }
            }
        }

        return frequencySum;
    }

    /// <summary>
    /// Gets the (optionally marginalized) probability for a particular pretend object, pretend action, and prop name.
    /// </summary>
    /// <returns>The joint or marginal probability.</returns>
    /// <param name="obj">The desired pretend object. Pass '#' to sum along this dimension.</param>
    /// <param name="act">The desired pretend action. Pass '#' to sum along this dimension.</param>
    /// <param name="prop">The desired prop name. Pass '#' to sum along this dimension.</param>
    private double P3D(string obj, string act, string prop, int[,,] frequencies, int total)
    {
        int objIndex = (obj == "#") ? -1 : objectToIndexDict[obj];
        int actIndex = (act == "#") ? -1 : actionToIndexDict[act];
        int propIndex = (prop == "#") ? -1 : propToIndexDict[prop];

        return P3D(objIndex, actIndex, propIndex, frequencies, total);
    }

    /// <summary>
    /// Gets the (optionally marginalized) probability for a particular pretend object, pretend action, and prop name.
    /// </summary>
    /// <returns>The joint or marginal probability.</returns>
    /// <param name="objIndex">The desired pretend object index. Pass -1 to sum along this dimension.</param>
    /// <param name="actIndex">The desired pretend action index. Pass -1 to sum along this dimension.</param>
    /// <param name="propIndex">The desired prop index. Pass -1 to sum along this dimension.</param>
    private double P3D(int objIndex, int actIndex, int propIndex, int[,,] frequencies, int total)
    {
        if(total == 0)
        {
            return 0;
        }

        List<int> objIndices = new List<int>();
        if(objIndex == -1)
        {
            objIndices.AddRange(objectToIndexDict.Values);
        }
        else
        {
            objIndices.Add(objIndex);
        }

        List<int> actIndices = new List<int>();
        if (actIndex == -1)
        {
            actIndices.AddRange(actionToIndexDict.Values);
        }
        else
        {
            actIndices.Add(actIndex);
        }

        List<int> propIndices = new List<int>();
        if (propIndex == -1)
        {
            propIndices.AddRange(propToIndexDict.Values);
        }
        else
        {
            propIndices.Add(propIndex);
        }

        double pSum = 0;
        foreach(int i in objIndices)
        { 
            foreach(int j in actIndices)
            { 
                foreach(int k in propIndices)
                {
                    pSum += ((double)frequencies[i, j, k] / (double)total);
                }
            }
        }

        return pSum;
    }

    /// <summary>
    /// Returns a marginal probability distribution with a linear/1D shape.
    /// </summary>
    /// <returns>The marginal 1D probability.</returns>
    /// <param name="dim1">The sole dimension to preserve.</param>
    private double[] LinearP3D(int dim1, int[,,] frequencies, int total)
    {
        double[] result = new double[frequencies.GetLength(dim1)];
        for(int i = 0; i < frequencies.GetLength(dim1); i++)
        {
            if (dim1 == 0)
            {
                result[i] = P3D(i, -1, -1, frequencies, total);
            }
            else if(dim1 == 1)
            {
                result[i] = P3D(-1, i, -1, frequencies, total);
            }
            else
            {
                result[i] = P3D(-1, -1, i, frequencies, total);
            }
        }

        return result;
    }

    /// <summary>
    /// Returns a marginal probability distribution with a rectangular/2D shape.
    /// </summary>
    /// <returns>The marginal 2D probability.</returns>
    /// <param name="dim1">One of the dimensions to preserve.</param>
    /// <param name="dim2">One of the dimensions to preserve.</param>
    private double[,] RectangularP3D(int dim1, int dim2, int[,,] frequencies, int total)
    {
        double[,] result = new double[frequencies.GetLength(dim1), frequencies.GetLength(dim2)];
        for (int j = 0; j < frequencies.GetLength(dim2); j++)
        {
            for (int i = 0; i < frequencies.GetLength(dim1); i++)
            {
                if (dim1 == 0 && dim2 == 1)
                {
                    result[i, j] = P3D(i, j, -1, frequencies, total);
                }
                else if (dim1 == 0 && dim2 == 2)
                {
                    result[i, j] = P3D(i, -1, j, frequencies, total);
                }
                else if(dim1 == 1 && dim2 == 2)
                {
                    result[i, j] = P3D(-1, i, j, frequencies, total);
                }
                else if (dim1 == 1 && dim2 == 0)
                {
                    result[i, j] = P3D(j, i, -1, frequencies, total);
                }
                else if (dim1 == 2 && dim2 == 0)
                {
                    result[i, j] = P3D(j, -1, i, frequencies, total);
                }
                else
                {
                    result[i, j] = P3D(-1, j, i, frequencies, total);
                }
            }
        }

        return result;
    }
}
