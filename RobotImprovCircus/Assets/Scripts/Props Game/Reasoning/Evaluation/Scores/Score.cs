﻿using System.Collections;
using System.Collections.Generic;

// A numerical representation
public abstract class Score
{
    public abstract float CalculateScore(GestureDefinition gesture);
}
