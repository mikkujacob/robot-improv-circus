﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The numerical representation of an object's novelty
public class Novelty : Score
{
    private GesturalDimRed gesturalDimRed;
    private SemanticDimRed semanticDimRed;
    private RTree<ReducedGesture> GestureDRTree;
    private RTree<ReducedWord> ActionDRTree;
    private RTree<ReducedWord> ObjectDRTree;
    private int k = 200; // The value of 200 is based on the KFinder experiment
    private Dictionary<string, float[]> WordDict;

    private AdaptiveScaling scalingG;
    private AdaptiveScaling scalingS;

    public double d_g = 0.0;
    public double d_s = 0.0;
    public double d_s_po = 0.0;
    public double d_s_pa = 0.0;
    public double d = 0.0;

    /// <summary>
    /// Initializes a new instance of the <see cref="T:Novelty"/> class.
    /// </summary>
    /// <param name="gDRTree">Gestural DR Tree.</param>
    /// <param name="aDRTree">Action DR Tree.</param>
    /// <param name="oDRTree">Object DR Tree.</param>
    /// <param name="gestureDR">Gestural DR.</param>
    /// <param name="semanticDR">Semantic DR.</param>
    /// <param name="wordDict">Word dict.</param>
    public Novelty(RTree<ReducedGesture> gDRTree, RTree<ReducedWord> aDRTree, RTree<ReducedWord> oDRTree, GesturalDimRed gestureDR, SemanticDimRed semanticDR, Dictionary<string, float[]> wordDict)
    {
        GestureDRTree = gDRTree;
        ActionDRTree = aDRTree;
        ObjectDRTree = oDRTree;

        gesturalDimRed = gestureDR;
        semanticDimRed = semanticDR;
        WordDict = wordDict;

        scalingG = new AdaptiveScaling();
        scalingS = new AdaptiveScaling();
    }

    // Don't use this method for the research unless you don't need to change the weights
    public override float CalculateScore(GestureDefinition gesture)
    {
        return CalculateScore(gesture, 0.5f);
    }

    /// <summary>
    /// Calculates the novelty of a gesture based on the action and object labels and the gesture vectors
    /// If the datatype of the gesture is not 16000, then it will only evaluate the semantic score regardless of weight
    /// </summary>
    /// <param name="gesture">The gesture to be evaluated</param>
    /// <param name="weight">The weight [0,1] for calculating the score between gesture and semantics. 
    /// 0f means only gestural score. 1f means only semantic score</param>
    /// <returns>a numerical score of the gesture's novelty</returns>
    public float CalculateScore(GestureDefinition gesture, float weight)
    {
        if (gesture.pretendActionName == "" || gesture.pretendObjectName == "" || !WordDict.ContainsKey(gesture.pretendActionName) || !WordDict.ContainsKey(gesture.pretendObjectName))
        {
            weight = 0f;
        }

        //TODO: Increase k over time to adjust for increased number of observed actions, but do so to get a minimal feasible increase over time.
        //k = 200 + (int)Math.Round(Math.Log(GestureDRTree.Count)/Math.Log(2));

        float semanticScore = 0;
        float gesturalScore = 0;
        if (weight > 0f)
        {
            // Finding semantic
            // Action
            //Debug.Log("ACTION NAME: " + gesture.pretendActionName);
            float[] aScore = SemanticReduce(Word2Vec(gesture.pretendActionName));
            ReducedWord redActionWord = new ReducedWord(new Envelope(aScore[0], aScore[1]), gesture.pretendActionName);
            List<ISpatialData> actionKNN = ActionDRTree.KNearestNeighbor(redActionWord, k);
            float avgActionDist = (float)CalculateAvgDistanceToNeighbors(redActionWord, actionKNN);

            // Object
            //Debug.Log("OBJECT NAME: " + gesture.pretendObjectName);
            float[] oScore = SemanticReduce(Word2Vec(gesture.pretendObjectName));
            ReducedWord redObjectWord = new ReducedWord(new Envelope(oScore[0], oScore[1]), gesture.pretendObjectName);
            List<ISpatialData> objectKNN = ObjectDRTree.KNearestNeighbor(redObjectWord, k);
            float avgObjectDist = (float)CalculateAvgDistanceToNeighbors(redObjectWord, objectKNN);

            d_s_po = avgObjectDist;
            d_s_pa = avgActionDist;

            semanticScore = (avgActionDist + avgObjectDist) / 2; // semantic score

            d_s = semanticScore;

            ////Add Pretend Action and Pretend Object to RTrees //Done in Executive now.
            //ActionDRTree.Insert(redActionWord); //Done in Executive now.
            //ObjectDRTree.Insert(redObjectWord); //Done in Executive now.
        }

        //float novelty = semanticScore;

        if (weight < 1)
        {
            // Finding gesture
            float[] gScore = GesturalReduce(gesture);
            ReducedGesture redGesture = new ReducedGesture(new Envelope(gScore[0], gScore[1]), new Guid(gesture.gestureID));
            List<ISpatialData> gesturekNN = GestureDRTree.KNearestNeighbor(redGesture, k);
            float avgGestureDist = (float)CalculateAvgDistanceToNeighbors(redGesture, gesturekNN);

            d_g = avgGestureDist;

            gesturalScore = avgGestureDist;

            //GestureDRTree.Insert(redGesture); //Done in Executive now.
        }

        d = (semanticScore + gesturalScore) / 2.0;

        //Novelty = WeightedAverage(semanticScore, gesturalScore)
        float novelty = scalingS.ScaleValue(Clamp(weight, 1f, 0f) * semanticScore) + scalingG.ScaleValue(Clamp(1f - weight, 1f, 0f) * gesturalScore);

        return novelty / 2.0f;
    }

    /// <summary>
    /// Reduces the gesture's gestural vector to a smaller dimension
    /// </summary>
    /// <param name="gesture">Gesture that will be reduced to a smaller dimension</param>
    /// <returns>The new reduced coordinates</returns>
    private float[] GesturalReduce(GestureDefinition gesture)
    {
        return gesturalDimRed.Reduce(gesture);
    }

    /// <summary>
    /// Reduces the word vector to a smallere dimension
    /// </summary>
    /// <param name="vector">Vector that will be reduced to a smaller dimension</param>
    /// <returns>The new reduced coordinates</returns>
    private float[] SemanticReduce(float[] vector)
    {
        return semanticDimRed.Reduce(vector);
    }

    /// <summary>
    /// Finds the vector that represents a words 
    /// </summary>
    /// <param name="word">The pretend label</param>
    /// <returns>The numerical representation of the word</returns>
    private float[] Word2Vec(string word)
    {
        return WordDict[word];
    }

    /// <summary>
    /// Calculates the average distance between a specified point and its neighbors
    /// </summary>
    /// <param name="node">The node that will be the common point</param>
    /// <param name="neighbors">All other nodes that will be compared to the common node</param>
    /// <returns>The average distance</returns>
    private double CalculateAvgDistanceToNeighbors(ISpatialData node, List<ISpatialData> neighbors)
    {
        var x1 = node.Envelope.CenterX;
        var y1 = node.Envelope.CenterY;
        var x2 = 0.0D;
        var y2 = 0.0D;
        var sum = 0.0D;
        foreach (ISpatialData neighbor in neighbors)
        {
            x2 = neighbor.Envelope.CenterX;
            y2 = neighbor.Envelope.CenterY;
            sum += CalculateDistance(x1, y1, x2, y2);
        }
        return sum / neighbors.Count;
    }

    /// <summary>
    /// Finds the max distance
    /// </summary>
    /// <param name="projectedData"></param>
    /// <returns></returns>
    private double CalculateMaxDistance(double[][] projectedData)
    {
        var maxdist = 0.0D;
        double dist;
        for (int i = 0; i < projectedData.Length; i++)
        {
            for (int j = i + 1; j < projectedData.Length; j++)
            {
                dist = CalculateDistance(projectedData[i][0], projectedData[i][1],
                                         projectedData[j][0], projectedData[j][1]);
                if (dist > maxdist)
                {
                    maxdist = dist;
                }
            }
        }
        return maxdist;
    }

    /// <summary>
    /// Calculates the distance between 2 2D points
    /// </summary>
    /// <param name="x1">X value of the first point</param>
    /// <param name="y1">Y value of the first point</param>
    /// <param name="x2">X value of the second point</param>
    /// <param name="y2">Y value of the second point</param>
    /// <returns>The total distance between the 2 points</returns>
    private double CalculateDistance(double x1, double y1, double x2, double y2)
    {
        return System.Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
    }

    /// <summary>
    /// Generic clamping method. Ensures that a value exists within a defined range
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value">The value to be evaluated</param>
    /// <param name="max">The (inclusive) max of the range</param>
    /// <param name="min">The (inclusive) min of the range</param>
    /// <returns></returns>
    public T Clamp<T>(T value, T max, T min)
        where T : IComparable<T>
    {
        T result = value;
        if (value.CompareTo(max) > 0)
            result = max;
        if (value.CompareTo(min) < 0)
            result = min;
        return result;
    }
}