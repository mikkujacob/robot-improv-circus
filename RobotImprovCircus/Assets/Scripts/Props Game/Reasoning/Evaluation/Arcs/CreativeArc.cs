﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The trajectory within the creative space that the partner robot's actions will follow
public class CreativeArc : Arc {

    private Dictionary<string, float[,]> CurveBehaviours = new Dictionary<string, float[,]> {
        {"rising", new float[,] { {0, 0, 0.5f},
                                  {0.25f, 0.25f, 0.65f},
                                  {0.5f, 0.5f, 0.75f},
                                  {0.75f, 0.75f, 0.85f},
                                  {1, 1, 1}} },
        {"flat", new float[,] { {0.5f, 0.5f, 0.5f},
                                  {0.5f, 0.5f, 0.5f},
                                  {0.5f, 0.5f, 0.5f},
                                  {0.5f, 0.5f, 0.5f},
                                  {0.5f, 0.5f, 0.5f}} },
        {"falling", new float[,] { {1, 1, 1},
                                  {0.75f, 0.75f, 0.85f},
                                  {0.5f, 0.5f, 0.75f},
                                  {0.25f, 0.25f, 0.65f},
                                  {0, 0, 0.5f}} }};
    
    public CreativeArc(int length = 5): this(length, "flat") { }
    public CreativeArc(int length, string behavior): base(length)
    {
        this.Points = CurveBehaviours[behavior];
    }

	// Use this for initialization
	void Start () { 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
