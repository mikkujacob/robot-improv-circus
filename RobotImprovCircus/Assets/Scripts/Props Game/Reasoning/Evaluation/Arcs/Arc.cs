﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A trajectory within a 3D space
public class Arc
{
    protected float[,] Points;
    private int CurrentPointIndex;

    // Ideally you would want to make the length greater than or equal to the amount of turns per round
    // 1 robot action + 1 human action = 1 turn
    public Arc(int length = 5)
    {
        this.CurrentPointIndex = 0;
        Points = new float[length, 3];
    }

    public float[] GetNextPoint()
    {
        CurrentPointIndex++;
        float[] nextPoint = new float[] { Points[CurrentPointIndex, 0], Points[CurrentPointIndex, 1], Points[CurrentPointIndex, 2] };
        return nextPoint;
    }

    public float[] GetCurrentPoint()
    {
        float[] currentPoint = { Points[CurrentPointIndex, 0], Points[CurrentPointIndex, 1], Points[CurrentPointIndex, 2] };
        return currentPoint;
    }

    public float GetCurrentPointN()
    {
        return Points[CurrentPointIndex, 0];
    }

    public float GetCurrentPointS()
    {
        return Points[CurrentPointIndex, 1];
    }

    public float GetCurrentPointV()
    {
        return Points[CurrentPointIndex, 2];
    }

    public void SetNextPointIndex()
    {
        CurrentPointIndex++;
        if (CurrentPointIndex > 5)
        {
            CurrentPointIndex = 0;
        }
    }
}
