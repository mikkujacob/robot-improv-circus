﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells agent to generate gestures aimed toward increasing or decreasing value scores
internal class ValueModulation : ResponseStrategy
{
    public ValueModulation(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory) : base(rTree, temporalMemory)
    {
    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
}
