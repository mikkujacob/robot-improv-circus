﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

// Tells agent to generate the same gesture as the user
internal class Mimicry : ResponseStrategy
{
    public Mimicry(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory) : base(rTree, temporalMemory)
    {
        strategyName = "Mimicry";
    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        // Debug.Log("Generating gestures using " + strategyName + " strategy");
        // Debug.Log("RTree is null: " + (rTree == null));
        // Debug.Log("GestureCoordinate is null: " + gesture.coordinateIndex);
        // Debug.Log("Gesture Latent Space Coordinates: " + gesture.coordinateIndex.Envelope.CenterX + ", " + gesture.coordinateIndex.Envelope.CenterX);
        List<GestureCoordinate> gestureCoordinate = rTree.Search(gesture.coordinateIndex.Envelope).ToList<GestureCoordinate>();
        List<GestureDefinition> returnedGestures = new List<GestureDefinition>();
        string propName = gesture.propObject.name;

        // Debug.Log("Prop Name from gesture is " + propName);

        float[] coordinate = null;

        if(gestureCoordinate.Count != 0)
        {
            // Debug.Log("Found a nearest neighbor for the gesture");
            coordinate = new float[] { (float)gestureCoordinate[0].Envelope.CenterX, (float)gestureCoordinate[0].Envelope.CenterY };
        }
        else
        {
            // Debug.Log("Did not find a nearest neighbor for the gesture, using gesture coordinates itself");
            coordinate = new float[] { (float)gesture.coordinateIndex.Envelope.CenterX, (float)gesture.coordinateIndex.Envelope.CenterY };
        }
        
        // Debug.Log("The resulting gesture coordinate was ( " + coordinate[0] + ", " + coordinate[1] + ")");

        float[] samples = new float[nLatents];
        for(int i = 0; i < nLatents; i++)
        {
            samples[i] = coordinate[i];
        }

        // Debug.Log("The resulting gesture was generated from ( " + samples[0] + ", " + samples[1] + ")");
        GestureDefinition response = DeepIMAGINATION.FindAction(samples, propName);
        Guid guid = Executive.GenerateGuid(new List<float>(samples), System.DateTime.Now);
        response.gestureID = guid.ToString();
        Envelope env = new Envelope(samples[0], samples[1]);
        response.coordinateIndex = new GestureCoordinate(env, guid);
        returnedGestures.Add(response);
        // Debug.Log("The resulting gesture was added to returned gestures list");

        return returnedGestures;
    }
}
