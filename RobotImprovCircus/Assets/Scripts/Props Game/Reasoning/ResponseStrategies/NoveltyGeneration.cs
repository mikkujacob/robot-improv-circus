﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells agent to generate gestures from a different latency space than the current one
internal class NoveltyGeneration : ResponseStrategy
{
    public NoveltyGeneration(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory) : base(rTree, temporalMemory)
    {
    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
}
