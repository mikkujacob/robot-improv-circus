﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Tells agent to generate gestures that deviate from expected interactions with an object
internal class SurpriseGeneration : ResponseStrategy
{
    public SurpriseGeneration(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory) : base(rTree, temporalMemory)
    {
    }

    public override List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }
}
