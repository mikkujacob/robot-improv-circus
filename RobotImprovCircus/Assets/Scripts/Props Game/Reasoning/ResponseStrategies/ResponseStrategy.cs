﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

// Guides how the agent will navigate through the creative space
internal class ResponseStrategy
{
    protected DeepIMAGINATIONInterface DeepIMAGINATION;
    protected RTree<GestureCoordinate> rTree;
    protected TemporalMemory temporalMemory;
    protected FrozenGraphLoader model;
    protected int nLatents;
    protected PropManager propManager;
    protected string strategyName;
    protected System.Random random;

    public ResponseStrategy(RTree<GestureCoordinate> rTree, TemporalMemory temporalMemory)
    {
        DeepIMAGINATION = UnityEngine.Object.FindObjectOfType<DeepIMAGINATIONInterface>();
        this.rTree = rTree;
        this.temporalMemory = temporalMemory;
        model = DeepIMAGINATION.GetModel();
        nLatents = DeepIMAGINATION.nLatents;
        propManager = UnityEngine.Object.FindObjectOfType<PropManager>();
        random = new System.Random();
    }

    /// <summary>
    /// Generates candidate actions based off of a single input.
    /// </summary>
    /// <param name="gesture">The gesture to base actions off of</param>
    /// <returns>A list of potential response gestures</returns>
    public virtual List<GestureDefinition> GenerateGestures(GestureDefinition gesture)
    {
        return new List<GestureDefinition>();
    }

    public string GetStrategyName()
    {
        return strategyName;
    }

    protected List<float[]> ChangeToFloatList(List<GestureCoordinate> list)
    {
        List<float[]> returnList = new List<float[]>();

        foreach(GestureCoordinate item in list)
        {
            float[] coordinate = CoordinateToFloat(item);
            returnList.Add(coordinate);
        }
        return returnList;
    }

    protected float[] CoordinateToFloat(GestureCoordinate item)
    {
        float[] coordinate = new float[] {(float) item.Envelope.CenterX,
                (float) item.Envelope.CenterY};
        return coordinate;
    }
}
