﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteController : MonoBehaviour {
	private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
	private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;

	private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
	private SteamVR_TrackedObject trackedObj;

	private GameObject moveObj;
	private bool isGrabbed;

	// Use this for initialization
	void Start () {
		trackedObj = GetComponent<SteamVR_TrackedObject>();
		isGrabbed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (controller == null) {
			return;
		}

		if (controller.GetPressUp(triggerButton) && moveObj != null) {
			moveObj.transform.parent = null;
			moveObj.GetComponent<Rigidbody>().isKinematic = false;
		}

		if (controller.GetPressDown(triggerButton) && moveObj != null) {
			moveObj.transform.parent = this.transform;
			moveObj.GetComponent<Rigidbody>().isKinematic = true;
		}
	}

	private void OnTriggerEnter(Collider collider) {
		Debug.Log("Trigger enter");
		moveObj = collider.gameObject;
	}

	private void OnTriggerExit(Collider collider) {		
		Debug.Log("Trigger exit");
		moveObj = null;
	}
}
