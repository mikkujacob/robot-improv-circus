﻿using UnityEngine;

public class NVRObjectStateTrackerController : MonoBehaviour
{
	ObjectStateTracker objectStateTracker;

	GlobalTrackerManager trackerManager;

	public void Start()
	{
		trackerManager = GameObject.FindGameObjectWithTag("MImEGlobalObject").GetComponent<GlobalTrackerManager>();
	}

	public void Update()
	{
		
	}

	public void BeginInteraction()
	{
		if(objectStateTracker == null) 
		{
			objectStateTracker = gameObject.GetComponent<ObjectStateTracker>();
		}

		if(trackerManager.hasActionStarted)
		{
			Debug.Log("BeginInteraction()");
			objectStateTracker.Activate();
		}
	}
}

