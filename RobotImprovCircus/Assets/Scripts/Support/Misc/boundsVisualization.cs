﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Morphs the the gameObject to visualize bounds of another
/// </summary>
public class boundsVisualization : MonoBehaviour {

	/// <summary>
	/// Target game object
	/// </summary>
	public GameObject target;

	/// <summary>
	/// mesh of the target game object
	/// </summary>
	private Mesh mesh;

	/// <summary>
	/// Renderer of the target game object
	/// </summary>
	private Renderer renderer;

	// Use this for initialization
	void Start () {
		mesh = target.GetComponent<MeshFilter>().mesh;
		renderer = target.GetComponent<Renderer>();
		transform.position = renderer.bounds.center;
		//Debug.Log(transform.position + "<------------------------" + target.name);
	}

	// Update is called once per frame
	void Update () {
		transform.position = renderer.bounds.center; //target.transform.position;
		transform.localScale = Vector3.Scale(mesh.bounds.size, target.transform.lossyScale);
		//Debug.Log(target.name + ": local scale- " + transform.localScale);
		//Debug.Log(target.name + ": lossy scale- " + transform.lossyScale);
	}
}
