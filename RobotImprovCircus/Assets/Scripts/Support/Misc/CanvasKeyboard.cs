﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CanvasKeyboard : MonoBehaviour {
	public string currentTextBuffer = "";
	public Text bufferDisplay;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (bufferDisplay != null)
		{
			bufferDisplay.text = currentTextBuffer;
		}


	}


	public void AddCharacter(Text neWCharacter)
	{
		currentTextBuffer += neWCharacter.text;
	}

	public void RemoveCharacter()
	{
		if(!String.IsNullOrEmpty(currentTextBuffer))
		{
    	currentTextBuffer = currentTextBuffer.Substring(0,(currentTextBuffer.Length - 1));
    }
	}
}
