using UnityEngine;
using System.Collections.Generic;
//using UnityEditorInternal;
using System.Linq;

/// <summary>
/// Spatial relationships tracker
/// </summary>
public class SpatialTracker : ITracker
{

	/// <summary>
	/// The tracker.
	/// </summary>
	private ObjectStateTracker tracker;

	/// <summary>
	/// The game object.
	/// </summary>
	private GameObject gameObject;

	/// <summary>
	/// reference to the GlobalTrackerManager
	/// </summary>
	public GlobalTrackerManager globalTrackerManager;

	/// <summary>
	/// List of all the object State Trackers
	/// </summary>
	private List<ObjectStateTracker> objectStateTrackers;

	/// <summary>
	/// All the objects above
	/// </summary>
	private List<string> above;

	/// <summary>
	/// All the objects below
	/// </summary>
	private List<string> below;

	/// <summary>
	/// All the objects right of.
	/// </summary>
	public List<string> rightOf;

	/// <summary>
	/// All the objects left of.
	/// </summary>
	public List<string> leftOf;

	/// <summary>
	/// All the objects behind.
	/// </summary>
	public List<string> behind;

	/// <summary>
	/// All the objects ahead.
	/// </summary>
	public List<string> ahead;

	/// <summary>
	/// Everything that is far
	/// </summary>
	public List<string> far;

	/// <summary>
	/// Everything that is near
	/// </summary>
	public List<string> near;

	/// <summary>
	/// Distance to differentiate near and far
	/// </summary>
	private float nearThreshold;


	/// <summary>
	/// Initializes a new instance of the <see cref="TriggerContainmentTracker"/> class.
	/// </summary>
	/// <param name="tracker">Tracker.</param>
	public SpatialTracker (ObjectStateTracker tracker)
	{
		this.tracker = tracker;
		gameObject = tracker.gameObject;

	}

	/// <summary>
	/// starts the tracker
	/// </summary>
	public void StartTracker()
	{
		globalTrackerManager = tracker.globalTrackerManager;
		objectStateTrackers = globalTrackerManager.objectStateTrackers;
		//gameObjects = GameObject.FindGameObjectsWithTag("ObjectStateTracker");
		rightOf = new List<string>();
		leftOf = new List<string>();
		above = new List<string>();
		below = new List<string>();
		ahead = new List<string>();
		behind = new List<string>();
		near = new List<string>();
		far = new List<string>();

		nearThreshold = 0.95f; // incase there is no playerAvatar object
		GameObject playerAvatar = GameObject.FindWithTag("PlayerAvatar");
		if (playerAvatar != null)
		{
			Mesh mesh = new Mesh();
			playerAvatar.GetComponent<SkinnedMeshRenderer>().BakeMesh(mesh);
			nearThreshold = Vector3.Scale(mesh.bounds.size, playerAvatar.transform.lossyScale).y / 2;
			//Debug.Log("nearThreshold: " + nearThreshold);
		}


	}

	/// <summary>
	/// updates the tracker
	/// </summary>
	public void UpdateTracker()
	{
		objectStateTrackers = globalTrackerManager.objectStateTrackers;
		Vector3 position = gameObject.transform.position;
		string id = tracker.state.id;
		Vector3 otherPositon;
		string otherId;
		foreach (ObjectStateTracker otherTracker in objectStateTrackers)
		{

			otherPositon = otherTracker.gameObject.transform.position;
			otherId = otherTracker.state.id;


			if (position.x < otherPositon.x) // checks left / right: X-AXIS
			{
				// other is to our right
				if (!rightOf.Contains(otherId)) //add to rightof if its not there
				{
					rightOf.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("rightOf", tracker.state.rightOf.ToString(), rightOf.ToString()));
					tracker.state.changes.Add(new TrackedChange("rightOf", "", otherId));
					tracker.isChanged = true;
					tracker.state.rightOf.Add(otherId);

				}
				if (leftOf.Contains(otherId)) //remove from leftOf if its in it
				{
					leftOf.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("leftOf", tracker.state.leftOf.ToString(), leftOf.ToString()));
					tracker.state.changes.Add(new TrackedChange("leftOf", otherId, ""));
					tracker.isChanged = true;
					tracker.state.leftOf.Remove(otherId);
				}
			}
			else if (position.x > otherPositon.x)
			{
				// other is to our left
				if (rightOf.Contains(otherId)) //remove from rightOf if its in it
				{
					rightOf.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("rightOf", tracker.state.rightOf.ToString(), rightOf.ToString()));
					tracker.state.changes.Add(new TrackedChange("rightOf", otherId, ""));
					tracker.isChanged = true;
					tracker.state.rightOf.Remove(otherId);
				}
				if (!leftOf.Contains(otherId)) //add to leftOf if its not there
				{
					leftOf.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("leftOf", tracker.state.leftOf.ToString(), leftOf.ToString()));
					tracker.state.changes.Add(new TrackedChange("leftOf", "", otherId));
					tracker.isChanged = true;
					tracker.state.leftOf.Add(otherId);
				}
			}





			if (position.y < otherPositon.y) // checks above / below: Y-AXIS
			{
				// other is above us
				if (!above.Contains(otherId)) //add to above if its not there
				{
					above.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("above", tracker.state.above.ToString(), above.ToString()));
					tracker.state.changes.Add(new TrackedChange("above", "", otherId));
					tracker.isChanged = true;
					tracker.state.above.Add(otherId);
					//Debug.Log(gameObject.name + ": " + otherObject.name + " has been ADDED to above");

				}
				if (below.Contains(otherId)) //remove from below if its in it
				{
					below.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("below", tracker.state.below.ToString(), below.ToString()));
					tracker.state.changes.Add(new TrackedChange("below", otherId, ""));
					tracker.isChanged = true;
					tracker.state.below.Remove(otherId);
					//Debug.Log(gameObject.name + ": " + otherObject.name + " has been REMOVED from below");
				}
			}
			else if (position.y > otherPositon.y)
			{
				// other is below us
				if (above.Contains(otherId)) //remove from above if its in it
				{
					above.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("above", tracker.state.above.ToString(), above.ToString()));
					tracker.state.changes.Add(new TrackedChange("above", otherId, ""));
					tracker.isChanged = true;
					tracker.state.above.Remove(otherId);
					//Debug.Log(gameObject.name + ": " + otherObject.name + " has been REMOVED from above");
				}
				if (!below.Contains(otherId)) //add to below if its not there
				{
					below.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("below", tracker.state.below.ToString(), below.ToString()));
					tracker.state.changes.Add(new TrackedChange("below", "", otherId));
					tracker.isChanged = true;
					tracker.state.below.Add(otherId);
					//Debug.Log(gameObject.name + ": " + otherObject.name + " has been ADDED to below");
				}
			}





			if (position.z < otherPositon.z) // checks ahead / behind: Z-AXIS
			{
				//other is ahead of us
				if (!ahead.Contains(otherId)) //add to ahead if its not there
				{
					ahead.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("ahead", tracker.state.ahead.ToString(), ahead.ToString()));
					tracker.state.changes.Add(new TrackedChange("ahead", "", otherId));
					tracker.isChanged = true;
					tracker.state.ahead.Add(otherId);

				}
				if (behind.Contains(otherId)) //remove from behind if its in it
				{
					behind.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("behind", tracker.state.behind.ToString(), behind.ToString()));
					tracker.state.changes.Add(new TrackedChange("behind", otherId, ""));
					tracker.isChanged = true;
					tracker.state.behind.Remove(otherId);
				}
			}
			else if (position.z > otherPositon.z)
			{
				// other is behind us
				if (ahead.Contains(otherId)) //remove from ahead if its in it
				{
					ahead.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("ahead", tracker.state.ahead.ToString(), ahead.ToString()));
					tracker.state.changes.Add(new TrackedChange("ahead", otherId, ""));
					tracker.isChanged = true;
					tracker.state.ahead.Remove(otherId);
				}
				if (!behind.Contains(otherId)) //add to behind if its not there
				{
					behind.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("behind", tracker.state.behind.ToString(), behind.ToString()));
					tracker.state.changes.Add(new TrackedChange("behind", "", otherId));
					tracker.isChanged = true;
					tracker.state.behind.Add(otherId);
				}
			}






			if (Vector3.Distance(position,otherPositon) < nearThreshold) // checks near vs far
			{
				// other is near to us
				if (!near.Contains(otherId)) //add to near if its not there
				{
					near.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("near", tracker.state.near.ToString(), near.ToString()));
					tracker.state.changes.Add(new TrackedChange("near", "", otherId));
					tracker.isChanged = true;
					tracker.state.near.Add(otherId);

				}
				if (far.Contains(otherId)) //remove from far if its in it
				{
					far.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("far", tracker.state.far.ToString(), far.ToString()));
					tracker.state.changes.Add(new TrackedChange("far", otherId, ""));
					tracker.isChanged = true;
					tracker.state.far.Remove(otherId);
				}
			}
			else if (Vector3.Distance(position,otherPositon) > nearThreshold)
			{
				// other is far to us
				if (near.Contains(otherId)) //remove from near if its in it
				{
					near.Remove(otherId);
					//tracker.state.changes.Add(new TrackedChange("near", tracker.state.near.ToString(), near.ToString()));
					tracker.state.changes.Add(new TrackedChange("near", otherId, ""));
					tracker.isChanged = true;
					tracker.state.near.Remove(otherId);
				}
				if (!far.Contains(otherId)) //add to far if its not there
				{
					far.Add(otherId);
					//tracker.state.changes.Add(new TrackedChange("far", tracker.state.far.ToString(), far.ToString()));
					tracker.state.changes.Add(new TrackedChange("far", "", otherId));
					tracker.isChanged = true;
					tracker.state.far.Add(otherId);
				}
			}






		}
	}

	/// <summary>
	/// resets the tracker
	/// </summary>
	public void ResetTracker()
	{

	}


}
