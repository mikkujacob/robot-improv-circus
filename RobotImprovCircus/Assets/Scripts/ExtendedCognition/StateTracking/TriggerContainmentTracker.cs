using UnityEngine;
using System.Collections.Generic;
//using UnityEditorInternal;
using System.Linq;

/// <summary>
/// Containment Tracker
/// </summary>
public class TriggerContainmentTracker : ITracker
{

	/// <summary>
	/// The tracker.
	/// </summary>
	private ObjectStateTracker tracker;

	/// <summary>
	/// The game object.
	/// </summary>
	private GameObject gameObject;

	/// <summary>
	/// Reference to the OnTrigger component.
	/// </summary>
	private OnTrigger triggerDetector;

	/// <summary>
	/// Last frame's contains.
	/// </summary>
	private List<string> lastContains;

	/// <summary>
	/// Last frame's containing.
	/// </summary>
	private bool lastContaining;

	/// <summary>
	/// Last frame's lastContainers.
	/// </summary>
	private List<string> lastContainers;

	/// <summary>
	/// Last frame's contained.
	/// </summary>
	private bool lastContained;



	/// <summary>
	/// Initializes a new instance of the <see cref="TriggerContainmentTracker"/> class.
	/// </summary>
	/// <param name="tracker">Tracker.</param>
	public TriggerContainmentTracker(ObjectStateTracker tracker)
	{
		this.tracker = tracker;
		this.gameObject = tracker.gameObject;


	}

	/// <summary>
	/// Starts the tracker.
	/// </summary>
	public void StartTracker()
	{
		gameObject.AddComponent<OnTrigger>();
		triggerDetector = gameObject.GetComponent<OnTrigger>();
		triggerDetector.Setup(tracker.state.contains, tracker.state.containing, tracker.state.containers, tracker.state.contained, this);



		lastContains = new List<string>();
		lastContainers = new List<string>();
		lastContains.AddRange(tracker.state.contains);
		lastContainers.AddRange(tracker.state.containers);

		lastContaining = tracker.state.containing;
		lastContained = tracker.state.contained;

	}

	/// <summary>
	/// Updates the tracker.
	/// </summary>
	public void UpdateTracker()
	{

		//there has been a change in contains
		if (!tracker.state.contains.SequenceEqual(triggerDetector.contains))
		{
			tracker.isChanged = true;
			lastContains.Clear();
			lastContains.AddRange(tracker.state.contains);
			tracker.state.contains.Clear();
			tracker.state.contains.AddRange(triggerDetector.contains);
			tracker.state.changes.Add(new TrackedChange("contains", lastContains.ToString(), tracker.state.contains.ToString()));
			//Debug.Log("change in contains from " + lastContains.Count + " to " + tracker.state.contains.Count + ". occured in " + gameObject.name);

		}

		// there has been a change in the containing state
		if (triggerDetector.containing != tracker.state.containing)
		{
			tracker.isChanged = true;
			lastContaining = tracker.state.containing;
			tracker.state.containing = triggerDetector.containing;
			tracker.state.changes.Add(new TrackedChange("containing", lastContaining.ToString(), tracker.state.containing.ToString()));
			//Debug.Log("change in containing from " + lastContaining + " to " + tracker.state.containing + ". occured in " + gameObject.name);
		}

		//there has been a change in containers
		if (!tracker.state.containers.SequenceEqual(triggerDetector.containers))
		{
			tracker.isChanged = true;
			lastContainers.Clear();
			lastContainers.AddRange(tracker.state.containers);
			tracker.state.containers.Clear();
			tracker.state.containers.AddRange(triggerDetector.containers);
			tracker.state.changes.Add(new TrackedChange("containers", lastContainers.ToString(), tracker.state.containers.ToString()));
			//Debug.Log("change in containers from " + lastContainers.Count + " to " + tracker.state.containers.Count + ". occured in " + gameObject.name);

		}

		//there has been a change in contained
		if (triggerDetector.contained != tracker.state.contained)
		{
			tracker.isChanged = true;
			lastContained = tracker.state.contained;
			tracker.state.contained = triggerDetector.contained;
			tracker.state.changes.Add(new TrackedChange("contained", lastContained.ToString(), tracker.state.contained.ToString()));
			//Debug.Log("change in contained from " + lastContained + " to " + tracker.state.contained + ". occured in " + gameObject.name);
		}


	}

	public void ResetTracker()
	{
		//Debug.Log("TriggerContainmentTracker reset");
	}

}
