﻿using System;

public interface ITracker
{
    void StartTracker();

    void UpdateTracker();

	void ResetTracker();
}

