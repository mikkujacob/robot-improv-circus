﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class TemporalMemory
{
    public class Node
    {
        public Guid id { get; }
        public int temporalIndex { get; }
        public Node prev { get; set; }
        public Node next { get; set; }

        public Node(Guid id, int temporalIndex)
        {
            this.id = id;
            this.temporalIndex = temporalIndex;
            this.prev = null;
            this.next = null;
        }
    }

    internal Node head;
    internal Node last;

    public void InsertAtEnd(Guid id)
    {
        int temporalIndex = 0;
        if(last != null)
        {
            temporalIndex = last.temporalIndex + 1;
        }
        Insert(id, temporalIndex);
    }

    public void Insert(Guid id, int temporalIndex)
    {
        Node newNode = new Node(id, temporalIndex);
        newNode.next = null;
        if(head == null)
        {
            head = newNode;
            last = newNode;
            head.prev = null;
            return;
        }

        if(head.temporalIndex > temporalIndex)
        {
            newNode.prev = null;
            head.prev = newNode;
            newNode.next = head;
            head = newNode;
            return;
        }

        if(last.temporalIndex < temporalIndex)
        {
            newNode.prev = last;
            last.next = newNode;
            last = newNode;
            return;
        }

        Node temp = head.next;
        while(temp.temporalIndex < temporalIndex)
        {
            temp = temp.next;
        }

        temp.prev.next = newNode;
        newNode.prev = temp.prev;
        temp.prev = newNode;
        newNode.next = temp;
    }

    public Node GetNode(int temporalIndex)
    {
        if(head == null || last == null)
        {
            return null;
        }
        else if(temporalIndex < head.temporalIndex)
        {
            return head;
        }
        else if(temporalIndex > last.temporalIndex)
        {
            return last;
        }

        Node temp = head;
        while(temp.next != null && temp.temporalIndex < temporalIndex)
        {
            temp = temp.next;
        }
        return temp;
    }

    public int getTemporalIndex(Guid id)
    {
        if(head == null || last == null)
        {
            return -1;
        }

        Node temp = head;
        while(temp.next != null && temp.id != id)
        {
            temp = temp.next;
        }

        if(temp.id != id)
        {
            return -1;
        }

        return temp.temporalIndex;
    }

    public Node GetKthNodeNext(int temporalIndex, int k)
    {
        if(head == null || last == null)
        {
            return null;
        }

        Node temp = GetNode(temporalIndex);

        if(temp == null)
        {
            return temp;
        }

        int i = 0;
        while(temp.next != null && i < k)
        {
            temp = temp.next;
            i++;
        }
        return temp;
    }

    public Node GetKthNodePrev(int temporalIndex, int k)
    {
        if(head == null || last == null)
        {
            return null;
        }

        Node temp = GetNode(temporalIndex);

        if(temp == null)
        {
            return temp;
        }

        int i = 0;
        while(temp.prev != null && i < k)
        {
            temp = temp.prev;
            i++;
        }
        return temp;
    }

    public int GetFirstTemporalIndex()
    {
        if(head == null)
        {
            return -1;
        }

        return head.temporalIndex;
    }

    public int GetLastTemporalIndex()
    {
        if(last == null)
        {
            return -1;
        }

        return last.temporalIndex;
    }
}
