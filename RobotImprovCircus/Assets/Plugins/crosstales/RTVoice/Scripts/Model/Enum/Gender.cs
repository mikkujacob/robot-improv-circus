﻿namespace Crosstales.RTVoice.Model.Enum
{
    /// <summary>The genders for voices.</summary>
    public enum Gender
    {
        MALE,
        FEMALE,
        UNKNOWN
    }
}
// © 2018 crosstales LLC (https://www.crosstales.com)