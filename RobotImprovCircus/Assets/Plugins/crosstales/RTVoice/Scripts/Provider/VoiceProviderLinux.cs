﻿using UnityEngine;
using System.Collections;

namespace Crosstales.RTVoice.Provider
{
    /// <summary>
    /// Linux voice provider.
    /// Note: needs eSpeak to work: => http://espeak.sourceforge.net/
    /// </summary>
    public class VoiceProviderLinux : BaseVoiceProvider
    {

        #region Variables

        //private static readonly System.Collections.Generic.List<Model.Voice> cachedVoices = new System.Collections.Generic.List<Model.Voice>(60);

        private const int defaultRate = 160;
        private const int defaultVolume = 100;
        private const int defaultPitch = 50;

#if UNITY_STANDALONE || UNITY_EDITOR
        private static string[] voices = {
            " 5  af             M  afrikaans            other\\af",
            " 5  am             -  amharic-test         test\\am",
            " 5  an             M  aragonese            europe\\an",
            " 5  as             -  assamese-test        test\\as",
            " 5  az             -  azerbaijani-test     test\\az",
            " 5  bg             -  bulgarian            europe\\bg",
            " 5  bn             M  bengali-test         test\\bn",
            " 5  bs             M  bosnian              europe\\bs",
            " 5  ca             M  catalan              europe\\ca",
            " 5  cs             M  czech                europe\\cs",
            " 5  cy             M  welsh                europe\\cy",
            " 5  da             M  danish               europe\\da",
            " 5  de             M  german               de",
            " 5  el             M  greek                europe\\el",
            " 5  en             M  default              default",
            " 2  en-gb          M  english              en            (en-uk 2)(en 2)",
            " 5  en-sc          M  en-scottish          other\\en-sc   (en 4)",
            " 5  en-uk-north    M  english-north        other\\en-n    (en-uk 3)(en 5)",
            " 5  en-uk-rp       M  english_rp           other\\en-rp   (en-uk 4)(en 5)",
            " 5  en-uk-wmids    M  english_wmids        other\\en-wm   (en-uk 9)(en 9)",
            " 2  en-us          M  english-us           en-us         (en-r 5)(en 3)",
            " 5  en-wi          M  en-westindies        other\\en-wi   (en-uk 4)(en 10)",
            " 5  eo             M  esperanto            other\\eo",
            " 5  es             M  spanish              europe\\es",
            " 5  es-la          M  spanish-latin-am     es-la         (es-mx 6)(es 6)",
            " 5  et             -  estonian             europe\\et",
            " 5  eu             -  basque-test          test\\eu",
            " 5  fa             -  persian              asia\\fa",
            " 5  fa-pin         -  persian-pinglish     asia\\fa-pin",
            " 5  fi             M  finnish              europe\\fi",
            //" 5  fr-be          M  french-Belgium       europe\\fr-be  (fr 8)",
            " 5  fr-fr          M  french               fr            (fr 5)",
            " 5  ga             -  irish-gaeilge        europe\\ga",
            " 5  gd             -  scottish-gaelic-test test\\gd",
            " 5  grc            M  greek-ancient        other\\grc",
            " 5  gu             -  gujarati-test        test\\gu",
            " 5  hi             M  hindi                asia\\hi",
            " 5  hr             M  croatian             europe\\hr     (hbs 5)",
            " 5  hu             M  hungarian            europe\\hu",
            " 5  hy             M  armenian             asia\\hy",
            " 5  hy-west        M  armenian-west        asia\\hy-west  (hy 8)",
            " 5  id             M  indonesian           asia\\id",
            " 5  is             M  icelandic            europe\\is",
            " 5  it             M  italian              europe\\it",
            " 5  jbo            -  lojban               other\\jbo",
            " 5  ka             -  georgian             asia\\ka",
            " 5  kl             -  greenlandic          test\\kl",
            " 5  kn             -  kannada              asia\\kn",
            " 5  ko             M  korean-test          test\\ko",
            " 5  ku             M  kurdish              asia\\ku",
            " 5  la             M  latin                other\\la",
            " 5  lfn            M  lingua_franca_nova   other\\lfn",
            " 5  lt             M  lithuanian           europe\\lt",
            " 5  lv             M  latvian              europe\\lv",
            " 5  mk             M  macedonian           europe\\mk",
            " 5  ml             M  malayalam            asia\\ml",
            " 5  ms             M  malay                asia\\ms",
            " 5  nci            M  nahuatl-classical    test\\nci",
            " 5  ne             M  nepali               asia\\ne",
            " 5  nl             M  dutch                europe\\nl",
            " 5  no             M  norwegian            europe\\no     (nb 5)",
            " 5  or             -  oriya-test           test\\or",
            " 5  pa             -  punjabi              asia\\pa",
            " 5  pap            -  papiamento-test      test\\pap",
            " 5  pl             M  polish               europe\\pl",
            " 5  pt-br          M  brazil               pt            (pt 5)",
            " 5  pt-pt          M  portugal             europe\\pt-pt  (pt 6)",
            " 5  ro             M  romanian             europe\\ro",
            " 5  ru             M  russian              europe\\ru",
            " 5  si             -  sinhala-test         test\\si",
            " 5  sk             M  slovak               europe\\sk",
            " 5  sl             -  slovenian-test       test\\sl",
            " 5  sq             M  albanian             europe\\sq",
            " 5  sr             M  serbian              europe\\sr",
            " 5  sv             M  swedish              europe\\sv",
            " 5  sw             M  swahili-test         other\\sw",
            " 5  ta             M  tamil                asia\\ta",
            " 5  te             -  telugu-test          test\\te",
            " 5  tr             M  turkish              asia\\tr",
            " 5  ur             -  urdu-test            test\\ur",
            " 5  vi             M  vietnam              asia\\vi",
            " 5  vi-hue         M  vietnam_hue          asia\\vi-hue",
            " 5  vi-sgn         M  vietnam_sgn          asia\\vi-sgn",
            //" 5  zh             M  mandarin             asia\\zh",
            " 5  zh-yue         M  cantonese            asia\\zh-yue   (yue 5)(zhy 5)"
            };
#endif

        #endregion


        #region Constructor

        /// <summary>
        /// Constructor for VoiceProviderLinux.
        /// </summary>
        /// <param name="obj">Instance of the speaker</param>
        public VoiceProviderLinux(MonoBehaviour obj) : base(obj)
        {
            if (Util.Helper.isEditorMode)
            {
#if UNITY_EDITOR
                getVoicesInEditor();
#endif
            }
            else
            {
                speakerObj.StartCoroutine(getVoices());
            }
        }

        #endregion


        #region Implemented methods

        public override string AudioFileExtension
        {
            get
            {
                return ".wav";
            }
        }

        public override AudioType AudioFileType
        {
            get
            {
                return AudioType.WAV;
            }
        }

        public override string DefaultVoiceName
        {
            get
            {
                return "en";
            }
        }

        public override System.Collections.Generic.List<Model.Voice> Voices
        {
            get
            {
                return cachedVoices;
            }
        }

        public override bool isWorkingInEditor
        {
            get
            {
                return true;
            }
        }

        public override int MaxTextLength
        {
            get
            {
                return 32000;
            }
        }

        public override IEnumerator SpeakNative(Model.Wrapper wrapper)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (wrapper == null)
            {
                Debug.LogWarning("'wrapper' is null!");
            }
            else
            {
                if (string.IsNullOrEmpty(wrapper.Text))
                {
                    Debug.LogWarning("'wrapper.Text' is null or empty: " + wrapper);
                }
                else
                {
                    yield return null; //return to the main process (uid)

                    string voiceName = getVoiceName(wrapper);
                    int calculatedRate = calculateRate(wrapper.Rate);
                    int calculatedVolume = calculateVolume(wrapper.Volume);
                    int calculatedPitch = calculatePitch(wrapper.Pitch);

                    System.Diagnostics.Process speakProcess = new System.Diagnostics.Process();

                    string args = (string.IsNullOrEmpty(voiceName) ? string.Empty : ("-v \"" + voiceName.Replace('"', '\'') + '"')) +
                                  (calculatedRate != defaultRate ? (" -s " + calculatedRate + " ") : string.Empty) +
                                  (calculatedVolume != defaultVolume ? (" -a " + calculatedVolume + " ") : string.Empty) +
                                  (calculatedPitch != defaultPitch ? (" -p " + calculatedPitch + " ") : string.Empty) +
                                  " -m \"" +
                                  wrapper.Text.Replace('"', '\'') + '"';

                    if (Util.Config.DEBUG)
                        Debug.Log("Process arguments: " + args);

                    speakProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                    speakProcess.StartInfo.Arguments = args;

                    System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref speakProcess)) { Name = wrapper.Uid.ToString() };
                    worker.Start();

                    silence = false;

                    processes.Add(wrapper.Uid, speakProcess);
                    onSpeakStart(wrapper);

                    do
                    {
                        yield return null;

                    } while (worker.IsAlive || !speakProcess.HasExited);

                    if (speakProcess.ExitCode == 0 || speakProcess.ExitCode == -1)
                    { //0 = normal ended, -1 = killed
                        if (Util.Config.DEBUG)
                            Debug.Log("Text spoken: " + wrapper.Text);

                        onSpeakComplete(wrapper);
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = speakProcess.StandardError)
                        {
                            string errorMessage = "Could not speak the text: " + wrapper + System.Environment.NewLine + "Exit code: " + speakProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                            Debug.LogError(errorMessage);
                            onErrorInfo(wrapper, errorMessage);
                        }
                    }

                    processes.Remove(wrapper.Uid);

                    speakProcess.Dispose();
                }
            }
#else
            yield return null;
#endif
        }

        public override IEnumerator Speak(Model.Wrapper wrapper)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (wrapper == null)
            {
                Debug.LogWarning("'wrapper' is null!");
            }
            else
            {
                if (string.IsNullOrEmpty(wrapper.Text))
                {
                    Debug.LogWarning("'wrapper.Text' is null or empty: " + wrapper);
                    //yield return null;
                }
                else
                {
                    if (wrapper.Source == null)
                    {
                        Debug.LogWarning("'wrapper.Source' is null: " + wrapper);
                        //yield return null;
                    }
                    else
                    {
                        yield return null; //return to the main process (uid)

                        string voiceName = getVoiceName(wrapper);
                        int calculatedRate = calculateRate(wrapper.Rate);
                        int calculatedVolume = calculateVolume(wrapper.Volume);
                        int calculatedPitch = calculatePitch(wrapper.Pitch);
                        string outputFile = getOutputFile(wrapper.Uid);

                        System.Diagnostics.Process speakToFileProcess = new System.Diagnostics.Process();

                        string args = (string.IsNullOrEmpty(voiceName) ? string.Empty : ("-v \"" + voiceName.Replace('"', '\'') + '"')) +
                                      (calculatedRate != defaultRate ? (" -s " + calculatedRate + " ") : string.Empty) +
                                      (calculatedVolume != defaultVolume ? (" -a " + calculatedVolume + " ") : string.Empty) +
                                      (calculatedPitch != defaultPitch ? (" -p " + calculatedPitch + " ") : string.Empty) +
                                      " -w \"" + outputFile.Replace('"', '\'') + '"' +
                                      " -m \"" +
                                      wrapper.Text.Replace('"', '\'') + '"';

                        if (Util.Config.DEBUG)
                            Debug.Log("Process arguments: " + args);

                        speakToFileProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                        speakToFileProcess.StartInfo.Arguments = args;

                        System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref speakToFileProcess)) { Name = wrapper.Uid.ToString() };
                        worker.Start();

                        silence = false;
                        onSpeakAudioGenerationStart(wrapper);

                        do
                        {
                            yield return null;
                        } while (worker.IsAlive || !speakToFileProcess.HasExited);

                        if (speakToFileProcess.ExitCode == 0)
                        {
                            yield return playAudioFile(wrapper, Util.Constants.PREFIX_FILE + outputFile, outputFile);
                        }
                        else
                        {
                            using (System.IO.StreamReader sr = speakToFileProcess.StandardError)
                            {
                                string errorMessage = "Could not speak the text: " + wrapper + System.Environment.NewLine + "Exit code: " + speakToFileProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                                Debug.LogError(errorMessage);
                                onErrorInfo(wrapper, errorMessage);
                            }
                        }

                        speakToFileProcess.Dispose();
                    }
                }
            }
#else
            yield return null;
#endif
        }

        public override IEnumerator Generate(Model.Wrapper wrapper)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (wrapper == null)
            {
                Debug.LogWarning("'wrapper' is null!");
            }
            else
            {
                if (string.IsNullOrEmpty(wrapper.Text))
                {
                    Debug.LogWarning("'wrapper.Text' is null or empty: " + wrapper);
                    //yield return null;
                }
                else
                {
                    yield return null; //return to the main process (uid)

                    string voiceName = getVoiceName(wrapper);
                    int calculatedRate = calculateRate(wrapper.Rate);
                    int calculatedVolume = calculateVolume(wrapper.Volume);
                    int calculatedPitch = calculatePitch(wrapper.Pitch);
                    string outputFile = getOutputFile(wrapper.Uid);

                    System.Diagnostics.Process speakToFileProcess = new System.Diagnostics.Process();

                    string args = (string.IsNullOrEmpty(voiceName) ? string.Empty : ("-v \"" + voiceName.Replace('"', '\'') + '"')) +
                                  (calculatedRate != defaultRate ? (" -s " + calculatedRate + " ") : string.Empty) +
                                  (calculatedVolume != defaultVolume ? (" -a " + calculatedVolume + " ") : string.Empty) +
                                  (calculatedPitch != defaultPitch ? (" -p " + calculatedPitch + " ") : string.Empty) +
                                  " -w \"" + outputFile.Replace('"', '\'') + '"' +
                                  " -m \"" +
                                  wrapper.Text.Replace('"', '\'') + '"';

                    if (Util.Config.DEBUG)
                        Debug.Log("Process arguments: " + args);

                    speakToFileProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                    speakToFileProcess.StartInfo.Arguments = args;

                    System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref speakToFileProcess)) { Name = wrapper.Uid.ToString() };
                    worker.Start();

                    silence = false;
                    onSpeakAudioGenerationStart(wrapper);

                    do
                    {
                        yield return null;
                    } while (worker.IsAlive || !speakToFileProcess.HasExited);

                    if (speakToFileProcess.ExitCode == 0)
                    {
                        processAudioFile(wrapper, outputFile);
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = speakToFileProcess.StandardError)
                        {
                            string errorMessage = "Could not generate the text: " + wrapper + System.Environment.NewLine + "Exit code: " + speakToFileProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                            Debug.LogError(errorMessage);
                            onErrorInfo(wrapper, errorMessage);
                        }
                    }

                    speakToFileProcess.Dispose();
                }
            }
#else
            yield return null;
#endif
        }

        #endregion


        #region Private methods

        protected override string getVoiceName(Model.Wrapper wrapper)
        {
            if (wrapper == null || wrapper.Voice == null || string.IsNullOrEmpty(wrapper.Voice.Name))
            {
                if (Util.Config.DEBUG)
                    Debug.LogWarning("'wrapper.Voice' or 'wrapper.Voice.Name' is null! Using the OS 'default' voice.");

                return DefaultVoiceName;
            }
            else
            {
                if (Speaker.ESpeakMod == Model.Enum.ESpeakModifiers.none)
                {
                    if (wrapper.Voice.Gender == Model.Enum.Gender.FEMALE)
                    {
                        return wrapper.Voice.Name + Util.Constants.ESPEAK_FEMALE_MODIFIER;
                    }

                    return wrapper.Voice.Name;
                }

                return wrapper.Voice.Name + "+" + Speaker.ESpeakMod.ToString();
            }


        }

        private IEnumerator getVoices()
        {
            /*
            #if (UNITY_STANDALONE_WIN || UNITY_EDITOR)
                        //the voices are fixed for Windows

                        cachedVoices.Clear();

                        foreach (string voice in voices)
                        {
                            cachedVoices.Add(new Model.Voice(voice.Substring(22, 20).Trim(), voice.Substring(43).Trim(), Util.Helper.StringToGender(voice.Substring(19, 1)), "unknown", voice.Substring(4, 15).Trim()));
                        }

            #endif
            */
#if UNITY_STANDALONE || UNITY_EDITOR
            if (Util.Helper.isWindowsPlatform) //the voices are fixed for Windows
            {
                cachedVoices.Clear();

                foreach (string voice in voices)
                {
                    cachedVoices.Add(new Model.Voice(voice.Substring(22, 20).Trim(), voice.Substring(43).Trim(), Util.Helper.StringToGender(voice.Substring(19, 1)), "unknown", voice.Substring(4, 15).Trim()));
                }

                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                System.Diagnostics.Process voicesProcess = new System.Diagnostics.Process();

                voicesProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                voicesProcess.StartInfo.Arguments = "--voices";

                voicesProcess.Start();

                System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref voicesProcess, Util.Constants.DEFAULT_TTS_KILL_TIME));
                worker.Start();

                do
                {
                    yield return null;
                } while (worker.IsAlive || !voicesProcess.HasExited);

                if (voicesProcess.ExitCode == 0)
                {
                    cachedVoices.Clear();

                    using (System.IO.StreamReader streamReader = voicesProcess.StandardOutput)
                    {
                        string reply;
                        while (!streamReader.EndOfStream)
                        {
                            reply = streamReader.ReadLine();

                            if (!string.IsNullOrEmpty(reply))
                            {
                                cachedVoices.Add(new Model.Voice(reply.Substring(22, 20).Trim(), reply.Substring(43).Trim(), Util.Helper.StringToGender(reply.Substring(19, 1)), "unknown", reply.Substring(4, 15).Trim()));
                            }
                        }
                    }

                    if (Util.Constants.DEV_DEBUG)
                        Debug.Log("Voices read: " + cachedVoices.CTDump());

                    //onVoicesReady();
                }
                else
                {
                    using (System.IO.StreamReader sr = voicesProcess.StandardError)
                    {
                        string errorMessage = "Could not get any voices: " + voicesProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                        Debug.LogError(errorMessage);
                        onErrorInfo(null, errorMessage);
                    }
                }

                voicesProcess.Dispose();
            }

#else
            yield return null;
#endif

            onVoicesReady();
        }

        private static int calculateRate(float rate)
        {
            int result = Mathf.Clamp(rate != 1f ? (int)(defaultRate * rate) : defaultRate, 1, 3 * defaultRate);

            if (Util.Constants.DEV_DEBUG)
                Debug.Log("calculateRate: " + result + " - " + rate);

            return result;
        }

        private static int calculateVolume(float volume)
        {
            return Mathf.Clamp((int)(defaultVolume * volume), 0, 200);
        }

        private static int calculatePitch(float pitch)
        {
            return Mathf.Clamp((int)(defaultPitch * pitch), 0, 99);
        }

#if UNITY_STANDALONE || UNITY_EDITOR
        private void startProcess(ref System.Diagnostics.Process process, int timeout = 0)
        {
            try
            {
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8; //TODO always enabled?

                process.Start();

                if (timeout > 0)
                {
                    process.WaitForExit(timeout);
                }
                else
                {
                    process.WaitForExit(); //TODO good idea?
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogError("Could not start process! Is eSpeak installed?" + System.Environment.NewLine + ex);
            }
        }

#endif

        #endregion


        #region Editor-only methods

#if UNITY_EDITOR

        public override void GenerateInEditor(Model.Wrapper wrapper)
        {
            if (wrapper == null)
            {
                Debug.LogWarning("'wrapper' is null!");
            }
            else
            {
                if (string.IsNullOrEmpty(wrapper.Text))
                {
                    Debug.LogWarning("'wrapper.Text' is null or empty: " + wrapper);
                }
                else
                {
                    string voiceName = getVoiceName(wrapper);
                    int calculatedRate = calculateRate(wrapper.Rate);
                    int calculatedVolume = calculateVolume(wrapper.Volume);
                    int calculatedPitch = calculatePitch(wrapper.Pitch);
                    string outputFile = getOutputFile(wrapper.Uid);

                    System.Diagnostics.Process speakToFileProcess = new System.Diagnostics.Process();

                    string args = (string.IsNullOrEmpty(voiceName) ? string.Empty : ("-v \"" + voiceName.Replace('"', '\'') + '"')) +
                                  (calculatedRate != defaultRate ? (" -s " + calculatedRate + " ") : string.Empty) +
                                  (calculatedVolume != defaultVolume ? (" -a " + calculatedVolume + " ") : string.Empty) +
                                  (calculatedPitch != defaultPitch ? (" -p " + calculatedPitch + " ") : string.Empty) +
                                  " -w \"" + outputFile.Replace('"', '\'') + '"' +
                                  " -m \"" +
                                  wrapper.Text.Replace('"', '\'') + '"';

                    if (Util.Config.DEBUG)
                        Debug.Log("Process arguments: " + args);

                    speakToFileProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                    speakToFileProcess.StartInfo.Arguments = args;

                    System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref speakToFileProcess)) { Name = wrapper.Uid.ToString() };
                    worker.Start();

                    silence = false;
                    onSpeakAudioGenerationStart(wrapper);

                    do
                    {
                        System.Threading.Thread.Sleep(50);
                    } while (worker.IsAlive || !speakToFileProcess.HasExited);

                    if (speakToFileProcess.ExitCode == 0)
                    {
                        if (Util.Config.DEBUG)
                            Debug.Log("Text generated: " + wrapper.Text);

                        copyAudioFile(wrapper, outputFile);
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = speakToFileProcess.StandardError)
                        {
                            string errorMessage = "Could not generate the text: " + wrapper + System.Environment.NewLine + "Exit code: " + speakToFileProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                            Debug.LogError(errorMessage);
                            onErrorInfo(wrapper, errorMessage);
                        }
                    }

                    onSpeakAudioGenerationComplete(wrapper);

                    speakToFileProcess.Dispose();
                }
            }
        }

        public override void SpeakNativeInEditor(Model.Wrapper wrapper)
        {
            if (wrapper == null)
            {
                Debug.LogWarning("'wrapper' is null!");
            }
            else
            {
                if (string.IsNullOrEmpty(wrapper.Text))
                {
                    Debug.LogWarning("'wrapper.Text' is null or empty: " + wrapper);
                }
                else
                {
                    string voiceName = getVoiceName(wrapper);
                    int calculatedRate = calculateRate(wrapper.Rate);
                    int calculatedVolume = calculateVolume(wrapper.Volume);
                    int calculatedPitch = calculatePitch(wrapper.Pitch);

                    System.Diagnostics.Process speakProcess = new System.Diagnostics.Process();

                    string args = (string.IsNullOrEmpty(voiceName) ? string.Empty : ("-v \"" + voiceName.Replace('"', '\'') + '"')) +
                                  (calculatedRate != defaultRate ? (" -s " + calculatedRate + " ") : string.Empty) +
                                  (calculatedVolume != defaultVolume ? (" -a " + calculatedVolume + " ") : string.Empty) +
                                  (calculatedPitch != defaultPitch ? (" -p " + calculatedPitch + " ") : string.Empty) +
                                  " -m \"" +
                                  wrapper.Text.Replace('"', '\'') + '"';

                    if (Util.Config.DEBUG)
                        Debug.Log("Process arguments: " + args);

                    speakProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                    speakProcess.StartInfo.Arguments = args;

                    System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref speakProcess)) { Name = wrapper.Uid.ToString() };
                    worker.Start();

                    silence = false;
                    onSpeakStart(wrapper);

                    do
                    {
                        System.Threading.Thread.Sleep(50);

                        if (silence)
                        {
                            speakProcess.Kill();
                        }
                    } while (worker.IsAlive || !speakProcess.HasExited);

                    if (speakProcess.ExitCode == 0 || speakProcess.ExitCode == -1)
                    { //0 = normal ended, -1 = killed
                        if (Util.Config.DEBUG)
                            Debug.Log("Text spoken: " + wrapper.Text);

                        onSpeakComplete(wrapper);
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = speakProcess.StandardError)
                        {
                            string errorMessage = "Could not speak the text: " + wrapper + System.Environment.NewLine + "Exit code: " + speakProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                            Debug.LogError(errorMessage);
                            onErrorInfo(wrapper, errorMessage);
                        }
                    }

                    speakProcess.Dispose();
                }
            }
        }

#endif
        private void getVoicesInEditor()
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (Util.Helper.isWindowsPlatform) //the voices are fixed for Windows
            {
                cachedVoices.Clear();

                foreach (string voice in voices)
                {
                    cachedVoices.Add(new Model.Voice(voice.Substring(22, 20).Trim(), voice.Substring(43).Trim(), Util.Helper.StringToGender(voice.Substring(19, 1)), "unknown", voice.Substring(4, 15).Trim()));
                }
            }
            else
            {
                System.Diagnostics.Process voicesProcess = new System.Diagnostics.Process();

                voicesProcess.StartInfo.FileName = Util.Config.TTS_LINUX;
                voicesProcess.StartInfo.Arguments = "--voices";

                try
                {
                    long time = System.DateTime.Now.Ticks;

                    System.Threading.Thread worker = new System.Threading.Thread(() => startProcess(ref voicesProcess, Util.Constants.DEFAULT_TTS_KILL_TIME));
                    worker.Start();

                    do
                    {
                        System.Threading.Thread.Sleep(50);
                    } while (worker.IsAlive || !voicesProcess.HasExited);

                    if (Util.Constants.DEV_DEBUG)
                        Debug.Log("Finished after: " + ((System.DateTime.Now.Ticks - time)/10000000));

                    if (voicesProcess.ExitCode == 0)
                    {
                        cachedVoices.Clear();

                        using (System.IO.StreamReader streamReader = voicesProcess.StandardOutput)
                        {
                            string reply;
                            while (!streamReader.EndOfStream)
                            {
                                reply = streamReader.ReadLine();

                                if (!string.IsNullOrEmpty(reply))
                                {
                                    cachedVoices.Add(new Model.Voice(reply.Substring(22, 20).Trim(), reply.Substring(43).Trim(), Util.Helper.StringToGender(reply.Substring(19, 1)), "unknown", reply.Substring(4, 15).Trim()));
                                }
                            }
                        }

                        if (Util.Constants.DEV_DEBUG)
                            Debug.Log("Voices read: " + cachedVoices.CTDump());
                    }
                    else
                    {
                        using (System.IO.StreamReader sr = voicesProcess.StandardError)
                        {
                            string errorMessage = "Could not get any voices: " + voicesProcess.ExitCode + System.Environment.NewLine + sr.ReadToEnd();
                            Debug.LogError(errorMessage);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    string errorMessage = "Could not get any voices!" + System.Environment.NewLine + ex;
                    Debug.LogError(errorMessage);
                }

                voicesProcess.Dispose();
            }
#endif
            onVoicesReady();
        }

        #endregion
    }
}
// © 2018 crosstales LLC (https://www.crosstales.com)